CC = g++ 
CFLAGS = -g -Wall -ferror-limit=2
#CFLAGS += -pthread -fopenmp -ggdb3 -O3 -fPIC -DPIC
EXEC = dragon
LIB = libdragon.a
AR = ar
ARFLAGS = rcs

#CFITSIO_DIR = /home/daniele/libs/cfitsio

INCDIR += -I./include/
#INCDIR += -I./include/readInputFile/
INCDIR += -Ilibs/pugixml/include/
INCDIR += -Ilibs/NE2001++/include/
INCDIR += -Ilibs/CRPropa3/include/

INCDIR += -I$(CFITSIO_DIR)/include
LIBDIR += -L$(CFITSIO_DIR)/lib -lcfitsio

INCDIR += -I$(GSL_DIR)/include
#INCDIR += `root-config --cflags`
LIBDIR += -L$(GSL_DIR)/lib -lgsl -lgslcblas
#LIBDIR += `root-config --libs`	
#HDRS += include/readInputFile.h 

OBJS += src/Galaxy/TGalaxy.o 
OBJS += src/Galaxy/ISRF.o 
OBJS += src/Galaxy/sourceProfile.o 
OBJS += src/Galaxy/darkMatterProfile.o
OBJS += src/Galaxy/Xco.o 
OBJS += src/Galaxy/gasDensityH2.o 
OBJS += src/Galaxy/gasDensityHI.o 
OBJS += src/Galaxy/gasDensityHII.o 
OBJS += src/Galaxy/TGasFerriere07.o
OBJS += src/Galaxy/TGasPohl08.o
OBJS += src/Galaxy/TGalaxyGrid.o
OBJS += src/Galaxy/magneticFields.o 
OBJS += src/Galaxy/pattern.o

OBJS += src/TPositionGrid.o 
OBJS += src/TEnergyAxes.o 
OBJS += src/TDragon.o 
OBJS += src/logger.o 
OBJS += src/utilities.o 
OBJS += src/cfitsioWrapper.o 
OBJS += src/pugiWrapper.o

OBJS += src/ParticleChain/particleChain.o 

OBJS += src/readInputFile/TGalaxyParams.o 
OBJS += src/readInputFile/TGasHIIParams.o 
OBJS += src/readInputFile/TAxisParams.o 
OBJS += src/readInputFile/TMagneticFieldParams.o 
OBJS += src/readInputFile/TOutputModeParams.o 
OBJS += src/readInputFile/TInput.o 
OBJS += src/readInputFile/TGasH2Params.o 
OBJS += src/readInputFile/TGasHIParams.o
OBJS += src/readInputFile/TIsrfParams.o 
OBJS += src/readInputFile/TObserverParams.o 
OBJS += src/readInputFile/TXcoParams.o 
OBJS += src/readInputFile/TSourceProfileParams.o 
OBJS += src/readInputFile/TDarkProfileParams.o
OBJS += src/readInputFile/TPatternParams.o

OBJS += libs/pugixml/src/pugixml.o
OBJS += libs/NE2001++/src/NE2001.o

.cpp.o:  
	$(CC) $(CFLAGS) $(INCDIR) -c $< -o $@

all: $(EXEC) $(LIB)

$(EXEC): $(OBJS) src/main.o
	$(CC) $(CFLAGS) $(INCDIR)  -o $@ $^ $(LIBDIR)

$(LIB): $(OBJS)
	$(AR) $(ARFLAGS) $@ $(OBJS)

clean:
	@rm -vf src/*.o
	@rm -vf src/Galaxy/*.o
	@rm -vf src/ParticleChain/*.o
	@rm -vf src/readInputFile/*.o
	@rm -vf libs/pugixml/src/*.o
	@rm -vf libs/NE2001++/src/*.o
	@rm -vf $(EXEC)
	@rm -vf $(LIB)
