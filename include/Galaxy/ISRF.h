#ifndef INCLUDE_DRAGON_GALAXY_ISRF_H_
#define INCLUDE_DRAGON_GALAXY_ISRF_H_

#include <cmath>
#include <iostream>
#include <vector>

#include "cgs.h"
#include "cfitsioWrapper.h"
#include "enums.h"
#include "logger.h"
#include "referenced.h"
#include "TGrid4D.h"

namespace DRAGON {

class TRadiationFieldGalpropTable : public Referenced {

	size_t rSize, zSize, frequencySize, nAxes, nComponents;
	double CRVAL1, CRVAL2, CRVAL3, CDELT1, CDELT2, CDELT3;
	std::string tableFilename;
	std::vector<double> frequency; // nu_array
	std::vector<double> isrfTable; // isrf_in

	void loadTable();

	void buildFrequencyAxis();

public:

	TRadiationFieldGalpropTable() : rSize(0), zSize(0), frequencySize(0), nComponents(0) {}

	TRadiationFieldGalpropTable(const std::string& isrfTableFilename_) : tableFilename(isrfTableFilename_) {
		loadTable();
		buildFrequencyAxis();
	}

	virtual ~TRadiationFieldGalpropTable() {
		frequency.clear();
		isrfTable.clear();
	}

	double getIsrfTableValue(int ir, int iz, int inu, int icomp) {
		return (isrfTable.at(((icomp * frequencySize + inu) * zSize + iz) * rSize + ir));
	}

	size_t getFrequencySize() const {
		return (frequencySize);
	}

	size_t getNComponents() const {
		return (nComponents);
	}

	size_t getRSize() const {
		return (rSize);
	}

	size_t getZSize() const {
		return (zSize);
	}

	double getCRVAL1() const {
		return (CRVAL1);
	}

	double getCRVAL2() const {
		return (CRVAL2);
	}

	double getCRVAL3() const {
		return (CRVAL3);
	}

	double getCDELT1() const {
		return (CDELT1);
	}

	double getCDELT2() const {
		return (CDELT2);
	}

	double getCDELT3() const {
		return (CDELT3);
	}

	const std::vector<double>& getFrequency() const {
		return (frequency);
	}

	const std::vector<double>& getIsrfTable() const {
		return (isrfTable);
	}
};


class TRadiationField : public Referenced {

protected:

	ref_ptr<ScalarGrid4D> field;
	std::vector<double> xGalaxy, yGalaxy, zGalaxy, frequency;
	size_t xSize, ySize, zSize, frequencySize;

public:

	TRadiationField() : xSize(0), ySize(0), zSize(0), frequencySize(0) {}

	TRadiationField(const std::vector<double>& x_, const std::vector<double>& y_, const std::vector<double>& z_) : xGalaxy(x_), yGalaxy(y_), zGalaxy(z_), xSize(0), ySize(0), zSize(0), frequencySize(0) {
		field = new ScalarGrid4D(xSize,ySize,zSize,frequencySize);
	}

	virtual ~TRadiationField() {
		field->clearGrid(); xGalaxy.clear(); yGalaxy.clear(); zGalaxy.clear(); frequency.clear();
	}

	//TISRF(TGrid* /**< Geometry description */, string /**< File containing the ISRF*/, TGeometry*, Input*);
	//virtual ~TIsrf() { ISRField.clear(); e_loss_Compton.clear(); nu_array.clear(); }

	//inline vector<double>& GetISRF() { return ISRField; } /**< Return the ISRF field distribution. */
	//inline double  GetISRF(int ir /**< radial position */, int iz /**< vertical position */, int inu /**< frequency position */) { return ISRField[index(inu,ir,iz)]; }
	//inline double  GetISRF(int ix /**< radial position */, int iy, int iz /**< vertical position */, int inu /**< frequency position */) { return ISRField[index(inu,ix,iy,iz)]; }
	//inline vector<double>& GetNuArray() { return nu_array; } /**< Get array of frequencies. */
	//inline double GetDnu() const { return Dnu; } /**< Get (logarithmic) spacing in frequency space. */

	//vector<double> ISRField; /**< ISRF density. */
	//vector<double> e_loss_Compton; /**< Energy loss for Compton scattering. */
	//vector<double> nu_array; /**< Array of frequency of ISRF. */
	//double Dnu; /**< Logarithmic spacing of frequency. */
	//int dimx; /**< radial dimension of simulation box. */
	//int dimy; /**< radial dimension of simulation box. */
	//int dimz; /**< vertical dimension of simulation box. */
	//int dimr_isrf; /**< radial dimension of ISRF representation in file. */
	//int dimz_isrf; /**< vertical dimension of ISRF representation in file. */
	//int dimnu;  /**< frequency dimension of ISRF representation in file. */
	//int ncomp; /**< Components accounted for in ISRF. */

	//inline int isrf_index(int ir, int iz, int inu, int icomp) { return ((icomp*dimnu +inu)*dimz_isrf + iz)*dimr_isrf + ir; }
	/**
	 * @fn inline int index(int ir, int iz, int inu, int icomp)
	 * @brief Convert matrix to linear representation.
	 * @return ((icomp*dimnu +inu)*dimz_isrf + iz)*dimr_isrf + ir
	 */

	//inline int index(int inu, int ir, int iz) { return (inu*dimx+ir)*dimz+iz; }
	/**
	 * @fn inline int index(int inu, int ir, int iz)
	 * @brief Convert matrix to linear representation.
	 * @return (inu*dimr+ir)*dimz+iz
	 */

	//inline int index(int inu, int ix, int iy, int iz) { return ((inu*dimx+ix)*dimy+iy)*dimz+iz; }
	/**
	 * @fn inline int index(int inu, int ir, int iz)
	 * @brief Convert matrix to linear representation.
	 * @return (inu*dimr+ir)*dimz+iz
	 */
};

/*class TRadiationFieldConstant : public TRadiationField {
	double constantValue;
public:
	TRadiationFieldConstant(const std::vector<double>& x_, const std::vector<double>& y_, const std::vector<double>& z_, const double& constantValue_) : TRadiationField(x_,y_,z_), constantValue(constantValue_) {}
	double densityFunction(const double& x_, const double& y_, const double& z_) { return (constantValue); }
};*/

class TRadiationFieldGalpropModel : public TRadiationField {

	ref_ptr<TRadiationFieldGalpropTable> GalpropTable;

	void buildGrid();

public:

	TRadiationFieldGalpropModel(const std::vector<double>& x_, const std::vector<double>& y_, const std::vector<double>& z_, const std::string& galpropIsrfTableFilename_) : TRadiationField(x_,y_,z_) {
		xSize = x_.size();
		ySize = y_.size();
		zSize = z_.size();
		GalpropTable = new TRadiationFieldGalpropTable(galpropIsrfTableFilename_);
		frequency = GalpropTable->getFrequency();
		frequencySize = GalpropTable->getFrequencySize();
		field = new ScalarGrid4D(xSize,ySize,zSize,frequencySize);
		buildGrid();
	}
};

class TRadiationFieldIsrfDelahaye10 : public TRadiationField {

	void buildGrid();

public:

	TRadiationFieldIsrfDelahaye10(const std::vector<double>& x_, const std::vector<double>& y_, const std::vector<double>& z_) : TRadiationField(x_,y_,z_) {
		xSize = x_.size();
		ySize = y_.size();
		zSize = z_.size();
		for (double f = 1e10; f < 1e16; f *= 1.2 )
			frequency.push_back(f);
		frequencySize = frequency.size();
		field = new ScalarGrid4D(xSize,ySize,zSize,frequencySize);
		buildGrid();
	}

};

} // namespace

#endif /* INCLUDE_DRAGON_GALAXY_ISRF_H_ */
