#ifndef INCLUDE_GALAXY_H_
#define INCLUDE_GALAXY_H_

#include <map>
#include <vector>

#include "exceptions.h"
#include "referenced.h"
#include "TPositionGrid.h"

#include "readInputFile/TGalaxyParams.h"

#include "Galaxy/TGalaxyGrid.h"
#include "Galaxy/gasDensityHI.h"
#include "Galaxy/gasDensityH2.h"
#include "Galaxy/gasDensityHII.h"
#include "Galaxy/ISRF.h"
#include "Galaxy/pattern.h"
#include "Galaxy/magneticFields.h"
#include "Galaxy/sourceProfile.h"
#include "Galaxy/darkMatterProfile.h"
#include "Galaxy/Xco.h"

namespace DRAGON {

class TGalaxy : public Referenced {

public:

	TGalaxy(const TGalaxyParams& GalaxyNode, const ref_ptr<TPositionGrid>& position);
	~TGalaxy();

	void buildGasH2(const TGasH2Params&);
	void buildGasHI(const TGasHIParams&);
	void buildGasHII(const TGasHIIParams&);
	void buildGasXco(const TXcoParams&);

	void buildSourceProfile(const TSourceProfileParams&);
	void buildDarkProfile(const TDarkProfileParams&);

	void buildMagneticField(const TMagneticFieldParams&);
	void buildISRF(const TIsrfParams&);

	void buildPattern(const TPatternParams&);
	//void applyPattern(ref_ptr<Grid3D>&, ref_ptr<TPattern>);

	void dumpToFile(const std::string& initFilename);

	//TGrid* GetCoordinates() { return _fCoordinates; }
	//TGeometry* GetGeometry() { return _fGeometry; }
	//TSource* GetSource() { return _fSource; }
	//TSource* GetSourceExtra() { return _fSourceExtra; }
	//TSource* GetDMSource() { return _fDMSource; }
	//vector<TGas*>& GetGas() { return _fGas; }
	//TGas* GetTotalGas() { return _fTotalGas; }

	//map<int, double> GetGasAbundances() { return _fGasAbundances; }
	//TDiffusionCoefficient* GetDiffCoeff() { return _fDperp; }
	//TReaccelerationCoefficient* GetReaccCoeff() { return _fDpp; }
	//TDiffusionCoefficient* GetDiffCoeffEl() { return _fDperpEl; }
	//TReaccelerationCoefficient* GetReaccCoeffEl() { return _fDppEl; }
	//TConvectionVelocity* GetVC() { return _fVC; }
	//TISRF* GetISRF() { return _fISRF; }
	//TBField* GetBField() { return _fB; }
	//Input* GetInput() { return inputStructure; }
	//void Delete();

protected:

	TGalaxyParams GalaxyNode;
	ref_ptr<TPositionGrid> position;

	ref_ptr<TGalaxyGrid> H2Density;
	ref_ptr<TGalaxyGrid> HIDensity;
	ref_ptr<TGalaxyGrid> HIIDensity;
	ref_ptr<TGalaxyGrid> Xco;
	ref_ptr<TGalaxyGrid> SourceProfile;
	ref_ptr<TGalaxyGrid> DarkProfile;

	//ref_ptr<TRadiationField> isrf;
	//ref_ptr<TPattern> pattern;
	//ref_ptr<TMagneticField> magneticField;
	//ref_ptr<TGrid3D<double> > convectionVelocity;
	//ref_ptr<TSNR> snrDistribution;
};

}

#endif /* INCLUDE_GALAXY_H_ */
