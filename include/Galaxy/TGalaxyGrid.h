#ifndef INCLUDE_GALAXY_TGALAXYGRID_H_
#define INCLUDE_GALAXY_TGALAXYGRID_H_

#include <algorithm>
#include <fstream>
#include <string>

#include "../TGrid3D.h"
#include "referenced.h"
#include "TPositionGrid.h"
#include "TVector3.h"

namespace DRAGON {

class TGalaxyGrid: public Referenced {
protected:
	ref_ptr<ScalarGrid3D> grid;
	ref_ptr<TPositionGrid> position;
	size_t xSize, ySize, zSize;

public:
	TGalaxyGrid();
	virtual ~TGalaxyGrid(){ if (grid) grid->clearGrid(); }

	void buildGrid(ref_ptr<TPositionGrid> position);
	void setPosition(ref_ptr<TPositionGrid> position);
	void calculateGrid(const double& smoothingRadius = -1);
	void scaleGrid(const double& norm);

	double getSmoothedDistribution(const TVector3d& pos, const double& smoothingRadius);
	virtual double distribution(const TVector3d& pos) = 0;

	double getTotal() const;
	double getTotal2() const;
	double getMean() const;
	//double getInterpolatedValue(const TVector3d& pos) const;
	double getVolumeIntegralCyl();
	double getVolumeIntegralCar();

	void dumpToFile(const std::string& filename) const;
	bool loadFromFITS(const std::string& filename);
	bool loadFromTXT(const std::string& filename);
};

} /* namespace */

#endif /* INCLUDE_GALAXY_TGALAXYGRID_H_ */
