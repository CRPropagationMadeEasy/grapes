#ifndef INCLUDE_GALAXY_TGASFERRIERE07_H_
#define INCLUDE_GALAXY_TGASFERRIERE07_H_

#include "cgs.h"
#include "TGasModel3D.h"
#include "TVector3.h"
#include "utilities.h"


#define pow2(A) (A * A)
#define sech(A) (1.0 / std::cosh(A))
#define sech2(A) (1.0 / std::cosh(A) / std::cosh(A))

namespace DRAGON {

struct BulgeParams {
	double X;
	double L;
	double H;
	double HI;
	double alpha;
	double beta;
	double theta;
};

struct CmzParams {
	double xc;
	double yc;
	double thetac;
	double Xc;
	double Lc;
	double Hc2;
	double HcI;
	double L3;
	double H3;
	double L2;
	double H2;
	double L1;
	double H1;
	double Y;
	double Z;
};

class TGasFerriere07 : public TGasModel3D {
protected:
	BulgeParams bulgeParams;
	CmzParams cmzParams;

	double sinalpha;
	double sinbeta;
	double sintheta;
	double sinthetac;
	double cosalpha;
	double cosbeta;
	double costheta;
	double costhetac;

public:
	TGasFerriere07();
	virtual ~TGasFerriere07() {}

	double density(const TVector3d &pos);

	virtual double cmz(const TVector3d &pos) { return (0); }
	virtual	double bulge(const TVector3d &pos) { return (0); }
	virtual	double disk(const TVector3d &pos) { return (0); }
};

class TH2Ferriere07 : public TGasFerriere07 {
	double XcoCorrectionFactor;

public:
	TH2Ferriere07() : TGasFerriere07(), XcoCorrectionFactor(1.0/0.5) {}
	double cmz(const TVector3d &pos);
	double bulge(const TVector3d &pos);
	double disk(const TVector3d &pos);
};

class THIFerriere07 : public TGasFerriere07 {
public:
	THIFerriere07() : TGasFerriere07() {}
	double cmz(const TVector3d &pos);
	double bulge(const TVector3d &pos);
	double disk(const TVector3d &pos);
};

class THIIFerriere07 : public TGasFerriere07 {
public:
	THIIFerriere07() : TGasFerriere07() {}
	double cmz(const TVector3d &pos);
	double bulge(const TVector3d &pos);
	double disk(const TVector3d &pos);
};

} /* namespace */

#endif /* INCLUDE_GALAXY_TGASFERRIERE07_H_ */
