#ifndef INCLUDE_GALAXY_TGASMODEL3D_H_
#define INCLUDE_GALAXY_TGASMODEL3D_H_

#include <cmath>

#include "referenced.h"
#include "TVector3.h"

namespace DRAGON {

class TGasModel3D : public Referenced  {
protected:
	virtual double density(const TVector3d& pos) = 0;

public:
	TGasModel3D() { }
	virtual ~TGasModel3D() {}

	double getDensity3D(const TVector3d& pos) {
		return (density(pos));
	}

	double getDensity2D(const TVector3d& pos) {
		double mean = 0;
		size_t counter = 0;
		for (double angle = 0; angle < 2.0 * M_PI; angle += M_PI / 10.0) {
			TVector3d rotated = pos.getRotated2D(angle);
			mean += density(rotated);
			counter++;
		}
		return (mean / (double)counter);
	}
};

} /* namespace */

#endif /* INCLUDE_GALAXY_TGASMODEL3D_H_ */

