#ifndef INCLUDE_GALAXY_TGASPOHL08_H_
#define INCLUDE_GALAXY_TGASPOHL08_H_

#include "../TGrid3D.h"
#include "cfitsioWrapper.h"
#include "cgs.h"
#include "referenced.h"
#include "TGasModel3D.h"
#include "TVector3.h"
#include "utilities.h"


namespace DRAGON {

class TH2Pohl08 : public TGasModel3D {
protected:
	size_t Nx;
	size_t Ny;
	size_t Nz;
	TVector3d origin;
	TVector3d spacing;
	ref_ptr<ScalarGrid3D> map;
	std::string fitsFilename;
	double XcoFactor;

public:
	TH2Pohl08(const std::string& fitsFilename);
	virtual ~TH2Pohl08() {}

	void loadMap();
	double density(const TVector3d &pos);
};

} /* namespace */

#endif /* INCLUDE_GALAXY_TGASPOHL08_H_ */
