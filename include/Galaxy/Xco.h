#ifndef INCLUDE_GALAXyXCO_H_
#define INCLUDE_GALAXyXCO_H_

#include "TGalaxyGrid.h"

namespace DRAGON {

class TXcoConstant : public TGalaxyGrid {
	double constantValue;
public:
	TXcoConstant(const double& constantValue_) : TGalaxyGrid(), constantValue(constantValue_) {}
	double distribution(const TVector3d& pos) { return (constantValue); }
};

class TXcoArimoto96 : public TGalaxyGrid {
public:
	TXcoArimoto96() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TXcoStrong04 : public TGalaxyGrid {
public:
	TXcoStrong04() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TXcoAckermann10 : public TGalaxyGrid {
public:
	TXcoAckermann10() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TXcoEvoli12 : public TGalaxyGrid {
	double XcoInner;
	double XcoOuter;
	double rBorder;
public:
	TXcoEvoli12(const double& XcoInner_, const double& XcoOuter_, const double& rBorder_) : TGalaxyGrid(),
			XcoInner(XcoInner_), XcoOuter(XcoOuter_), rBorder(rBorder_) {}
	double distribution(const TVector3d& pos);
};

class TXcoMyModel : public TGalaxyGrid {
public:
	TXcoMyModel() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

} // namespace

#endif /* INCLUDE_GALAXyXCO_H_ */
