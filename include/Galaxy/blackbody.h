#ifndef INCLUDE_GALAXY_BLACKBODY_H_
#define INCLUDE_GALAXY_BLACKBODY_H_

#include <cmath>

#include "../cgs.h"

#define pow3(A) (A * A * A)

namespace DRAGON {

class TBlackBody {
	double Temperature;
	double NormalizationToCMB;

public:
	TBlackBody() : Temperature(-1), NormalizationToCMB(-1) {}
	TBlackBody(const double& T, const double& norm) : Temperature(T), NormalizationToCMB(norm) {}
	~TBlackBody(){}

	double getNormalizationToCmb() const {
		return (NormalizationToCMB);
	}

	void setNormalizationToCmb(double normalizationToCmb) {
		NormalizationToCMB = normalizationToCmb;
	}

	double getTemperature() const {
		return (Temperature);
	}

	void setTemperature(double temperature) {
		Temperature = (temperature);
	}

	double getSpectrum(double frequency) { // in eV/(cm3 Hz)
		double value = NormalizationToCMB; // * (8.0 * M_PI * hPlanck) /
				//pow3(cLight) * pow3(frequency) *
				//( 1.0 / ( exp(hPlanck * frequency / kBoltzmann / Temperature) - 1.0) );
		return (value);
	}


};

}

#endif /* INCLUDE_GALAXY_BLACKBODY_H_ */
