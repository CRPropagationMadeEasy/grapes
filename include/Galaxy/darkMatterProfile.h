#ifndef SRC_GALAXY_DARKMATTERPROFILE_H_
#define SRC_GALAXY_DARKMATTERPROFILE_H_

#include <cmath>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>

#include "Galaxy/TGalaxyGrid.h"

namespace DRAGON {

class TDarkProfileConstant : public TGalaxyGrid {
	double constantValue;
public:
	TDarkProfileConstant(const double& constantValue_) : TGalaxyGrid(), constantValue(constantValue_) {}
	double distribution(const TVector3d& pos);
};

class TDarkProfileMyModel : public TGalaxyGrid {
public:
	TDarkProfileMyModel() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TDarkProfileGNFW : public TGalaxyGrid {
	double alpha;
	double beta;
	double gamma;
	double rhos;
	double rs;
public:
	TDarkProfileGNFW(double alpha, double beta, double gamma);
	void findScaleRadius();
	void computeProfileParams(double mwDarkMass, double mwDarkRmax, double mwDarkLocalDensity);
	static double minimizeFunction(double x, void* params) { return x; };

	double profile(const double& x);
	double distribution(const TVector3d& pos);
};

class TDarkProfileCored : public TGalaxyGrid {
	double beta;
	double gamma;
	double rhos;
	double rs;
public:
	TDarkProfileCored(double beta, double gamma);
	double distribution(const TVector3d& pos);
};

class TDarkProfileEinasto65 : public TGalaxyGrid {
	double alpha;
	double rhos;
	double rs;
public:
	TDarkProfileEinasto65(double alpha);
	double distribution(const TVector3d& pos);
};

class TDarkProfileStadel09 : public TGalaxyGrid {
	double alpha;
	double rhos;
	double rs;
public:
	TDarkProfileStadel09(double alpha);
	double distribution(const TVector3d& pos);
};

} /* namespace */

#endif /* SRC_GALAXY_DARKMATTERPROFILE_H_ */
