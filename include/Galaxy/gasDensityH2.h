#ifndef INCLUDE_GALAXY_GASDENSITYH2_H_
#define INCLUDE_GALAXY_GASDENSITYH2_H_

#include "TGalaxyGrid.h"
#include "TGasFerriere07.h"
#include "TGasModel3D.h"
#include "TGasPohl08.h"

namespace DRAGON {

class TH2DensityConstant : public TGalaxyGrid {
	double constantValue;
public:
	TH2DensityConstant(const double& constantValue_);
	double distribution(const TVector3d& pos);
};

class TH2DensityBronfman88 : public TGalaxyGrid {
public:
	TH2DensityBronfman88() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TH2DensityNakanishi06 : public TGalaxyGrid {
public:
	TH2DensityNakanishi06() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TH2DensityFerriere07 : public TGalaxyGrid {
	ref_ptr<TH2Ferriere07> gasModel;
public:
	TH2DensityFerriere07();
	~TH2DensityFerriere07() {}
	double distribution(const TVector3d& pos);
};

class TH2DensityPohl08 : public TGalaxyGrid {
	ref_ptr<TH2Pohl08> gasModel;
public:
	TH2DensityPohl08(const std::string& filename);
	~TH2DensityPohl08() {}
	double distribution(const TVector3d& pos);
};

class TH2DensityMyModel : public TGalaxyGrid {
public:
	TH2DensityMyModel() : TGalaxyGrid() {}
 	double distribution(const TVector3d& pos);
};

} // namespace

#endif /* INCLUDE_GALAXY_GASDENSITYH2_H_ */
