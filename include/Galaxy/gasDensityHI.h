#ifndef INCLUDE_GALAXY_GASDENSITYHI_H_
#define INCLUDE_GALAXY_GASDENSITYHI_H_

#include "TGalaxyGrid.h"
#include "TGasFerriere07.h"
#include "TGasModel3D.h"

namespace DRAGON {

class THIDensityConstant : public TGalaxyGrid {
	double constantValue;
public:
	THIDensityConstant(const double& constantValue_) : TGalaxyGrid(), constantValue(constantValue_) {}
	double distribution(const TVector3d& pos);
};

class THIDensityFerriere07 : public TGalaxyGrid {
	ref_ptr<THIFerriere07> gasModel;
public:
	THIDensityFerriere07();
	double distribution(const TVector3d& pos);
};

class THIDensityNakanishi03 : public TGalaxyGrid {
	double h0;
	double n0;
public:
	THIDensityNakanishi03();
	double distribution(const TVector3d& pos);
};

class THIDensityMoskalenko02 : public TGalaxyGrid {
	std::vector<double> Rkpc;
	std::vector<double> NHI;
	double nGB, nDL;
	double A1, z1, A2, z2, B, zh;
public:
	THIDensityMoskalenko02();
	double calcfZ(const double& rKpc, const double& zKpc);
	double calcfR(const double& rKpc, const double& zKpc);
	double distribution(const TVector3d& pos);
};

class THIDensityMyModel : public TGalaxyGrid {
public:
	THIDensityMyModel() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

} // namespace

#endif /* INCLUDE_GALAXY_GASDENSITYHI_H_ */
