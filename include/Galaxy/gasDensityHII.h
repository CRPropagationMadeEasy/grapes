#ifndef INCLUDE_GALAXY_GASDENSITYHII_H_
#define INCLUDE_GALAXY_GASDENSITYHII_H_

#include "TGalaxyGrid.h"
#include "NE2001.h"

namespace DRAGON {

class THIIDensityConstant : public TGalaxyGrid {
	double constantValue;
public:
	THIIDensityConstant(const double& constantValue_) : TGalaxyGrid(), constantValue(constantValue_) {}
	double distribution(const TVector3d& pos);
};

class THIIDensityMyModel : public TGalaxyGrid {
public:
	THIIDensityMyModel() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class THIIDensityCordes91 : public TGalaxyGrid {
	double fne1;
	double H1;
	double A1;
	double fne2;
	double H2;
	double A2;
	double R2;

public:
	THIIDensityCordes91();
	double distribution(const TVector3d& pos);
};

class THIIDensityNE2001 : public TGalaxyGrid {
	NE2001 model;
public:
	THIIDensityNE2001() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class THIIDensityFerriere07 : public TGalaxyGrid {
public:
	THIIDensityFerriere07() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

} // namespace

#endif /* INCLUDE_GALAXY_GASDENSITYHII_H_ */
