#ifndef INCLUDE_MAGNETIC_FIELDS_H_
#define INCLUDE_MAGNETIC_FIELDS_H_

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "../TGrid3D.h"
#include "cgs.h"
#include "enums.h"
#include "exceptions.h"
#include "referenced.h"
#include "TPositionGrid.h"
#include "TVector3.h"
#include "utilities.h"

#include "readInputFile/TMagneticFieldParams.h"

namespace DRAGON {

class TMagneticField : public Referenced {

protected:
	ref_ptr<ScalarGrid3D> randomMagneticField; /// in muG
	ref_ptr<VectorGrid> regularMagneticField; // in muG
	std::vector<double> xGalaxy, yGalaxy, zGalaxy;
	size_t xSize, ySize, zSize;

public:
	TMagneticField() : xSize(0), ySize(0), zSize(0) { }
	virtual ~TMagneticField() { randomMagneticField->clearGrid(); regularMagneticField->clearGrid(); xGalaxy.clear(); yGalaxy.clear(); zGalaxy.clear(); }

	void buildGrid();
	void calculateRandomField();
	void calculateRegularField();

	virtual double randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_) = 0;
	virtual TVector3d regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_) = 0;

	void dumpToFile(const std::string& outputFilename);

	//TMagneticField(const std::vector<double>& x_, const std::vector<double>& y_, const std::vector<double>& z_) : xGalaxy(x_), yGalaxy(y_), zGalaxy(z_), xSize(x_.size()), ySize(y_.size()), zSize(z_.size()) {
	//	magneticField = new Grid3D(xSize,ySize,zSize);
	//}
	//	magneticFieldRegular.clear();
	//	magneticFieldRegularVersors.clear();

	//double getMagneticEnergyDensity(const int& i); // eV/cm^3
	//double getMagneticEnergyDensity(const int& ix, const int& iy, const int& iz);

	//inline std::vector<double>& getMagneticField(){ return (magneticField); }
	//inline std::map<int, std::vector<double> >& getMagneticFieldRegular() { return (magneticFieldRegular); }
	//inline std::map<int, std::vector<double> >& getMagneticFieldRegularVersors() { return (magneticFieldRegularVersors); }

	/*std::vector<double> getMagneticFieldRegular(const int& counter)
    		{
		std::vector<double> b(3, 0);
		b[0] = magneticFieldRegular[0][counter];
		b[1] = magneticFieldRegular[1][counter];
		b[2] = magneticFieldRegular[2][counter];
		return (b);
    		}

	inline std::vector<double> getMagneticFieldRegular(const int& ix,const int& iy,const int& iz)
    		{
		return (getMagneticFieldRegular( index(ix,iy,iz) ));
    		}

	std::vector<double> getMagneticFieldRegularVersors(const int& counter)
    		{
		std::vector<double> b(3, 0);
		b[0] = magneticFieldRegularVersors[0][counter];
		b[1] = magneticFieldRegularVersors[1][counter];
		b[2] = magneticFieldRegularVersors[2][counter];
		return (b);
    		}

	inline std::vector<double> getMagneticFieldRegularVersors(const int& ix,const int& iy,const int& iz)
    		{
		return (getMagneticFieldRegularVersors( index(ix,iy,iz) ));
    		}
	 */

	//virtual std::vector<double> getMagneticField(const double& x,const double& y,const double& z) =0;
	//std::vector<double> getMagneticFieldRegularVersors(const double& x,const double& y,const double& z);

	/*std::vector<double> x;
     std::vector<double> y;
     std::vector<double> z;*/

	/*int xSize;
     int ySize;
     int zSize;*/
	//inline int index(const int& ir,const int& iz) { return (ir*zGalaxy.Size+iz); }
	//inline int index(const int& ix,const int& iy,const int& iz) { return ((ix*yGalaxy.Size+iy)*zGalaxy.Size+iz); }
};

class TExponentialMagneticField : public TMagneticField {

protected:
	double B0;
	double r0;
	double rc;
	double zc;

public:
	TExponentialMagneticField(const double& B0_, const double& r0_, const double& rc_, const double zc_) : TMagneticField(), B0(B0_), r0(r0_), rc(rc_), zc(zc_) { }
	double randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_){ return (0);};
	TVector3d regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
};


class TSun2007MagneticField : public TMagneticField {

protected:
	const double B0_disk;
	const double B0z_disk;
	const double Bc_disk;
	const double r0_disk;
	const double rc_disk;
	const double z0_disk;
	const double p; //pitch angle
	const double r1, r2, r3;
	const double B0_halo;
	const double r0_halo;
	const double z0_halo;
	const double z1_halo_low;
	const double z1_halo_high;
	const double Brandom;
	const double rsun;  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
	const double zsun;  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)

public:
	TSun2007MagneticField() : TMagneticField(),
		B0_disk(2.0), //muG
		B0z_disk(0.25), //muG
		Bc_disk(2.), //muG
		r0_disk(10.0), //kpc
		rc_disk(5.0), //kpc
		z0_disk(1.0), //kpc
		p(-12.0), //degrees
		r1(7.5), r2(6), r3(5), //kpc
		B0_halo(10.), //muG
		r0_halo(4.0), //kpc
		z0_halo(1.5), //kpc
		z1_halo_low(0.2), //kpc
		z1_halo_high(0.4), //kpc
		Brandom(3.), //muG
		rsun(8.3),  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
		zsun(0.0)  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
	{ }
	double randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
	TVector3d regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
	const double D1(const double& r_, const double& theta_, const double& z_);
	const double D2(const double& r_, const double& theta_);
};


class TPshirkov11ASSMagneticField : public TMagneticField {

protected:
	const double B0_disk;
	const double B0z_disk;
	const double rc_disk;
	const double z0;
	const double d;
	const double p; //pitch angle
	const double B0_halo;
	const double r0_halo;
	const double z0_halo;
	const double z1_halo_low;
	const double z1_halo_high;
	const double Brandom;
	const double rsun;  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
	const double zsun;  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)

public:
	TPshirkov11ASSMagneticField() : TMagneticField(),
		B0_disk(2.0), //muG
		B0z_disk(0.25), //muG
		rc_disk(5.0), //kpc
		z0(1.0), //kpc
		d(-0.6), //kpc
		p(-5.0), //degrees
		B0_halo(4.), //muG
		r0_halo(8.0), //kpc
		z0_halo(1.3), //kpc
		z1_halo_low(0.25), //kpc
		z1_halo_high(0.4), //kpc
		Brandom(3.0), //muG
		rsun(8.3),  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
		zsun(0.0)  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
	{ }
	double randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
	TVector3d regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
	const double D1(const double& r_, const double& theta_, const double& z_);
	const double D2(const double& r_, const double& theta_);
};


class TPshirkov11BSSMagneticField : public TMagneticField {

protected:
	const double B0_disk;
	const double B0z_disk;
	const double rc_disk;
	const double z0;
	const double d;
	const double p; //pitch angle
	const double B0_halo;
	const double r0_halo;
	const double z0_halo;
	const double z1_halo_low;
	const double z1_halo_high;
	const double Brandom;
	const double rsun;  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
	const double zsun;  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)

public:
	TPshirkov11BSSMagneticField() : TMagneticField(),
		B0_disk(2.0), //muG
		B0z_disk(0.25), //muG
		rc_disk(5.0), //kpc
		z0(1.0), //kpc
		d(-0.6), //kpc
		p(-6.0), //degrees
		B0_halo(4.), //muG
		r0_halo(8.0), //kpc
		z0_halo(1.3), //kpc
		z1_halo_low(0.25), //kpc
		z1_halo_high(0.4), //kpc
		Brandom(3.0), //muG
		rsun(8.3),  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
		zsun(0.0)  // THIS HAS TO BE READ FROM INPUT (input must be a singleton!)
	{ }
	double randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
	TVector3d regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_);
	const double D1(const double& r_, const double& theta_, const double& z_);
	const double D2(const double& r_, const double& theta_);
};


//class TUniformMagneticField : public TMagneticField {
//
//public:
//	TUniformMagneticField() { }
//	TUniformMagneticField(const double& magneticFieldConstantValue,TGalaxyGrids* galGrid);//, TGeometry*);
//	virtual std::vector<double> getMagneticField(const double& x,const double& y,const double& z)
//    		{
//		return (std::vector<double>(3,0));
//    		}
//};
//
//class TPshirkov11MagneticField : public TMagneticField {
//
//public :
//
//	TPshirkov11MagneticField(TGalaxyGrids* galGrid);
//	//TPshirkov11MagneticField(const PshirkovMagneticFieldsParams& pMagneticField_, TGalaxyGrids*);
//	virtual std::vector<double> getMagneticField(const double& x, const double& y, const double& z);
//
//protected :
//
//	double B0;
//	double RC;
//	double Z0;
//	double R0;
//	double B0H;
//	double R0H;
//	double Z0H;
//	double B0turb;
//	double rscaleTurb;
//	double zscaleTurb;
//	double robs;
//};
//
//class TJanssonFarrar12MagneticField : public TMagneticField {
//
//public :
//	/*TJanssonFarrar12MagneticField() :
//     bring(0.1),
//     hdisk(0.40),
//     wdisk(0.27),
//     Bn(1.4),
//     Bs(-1.1),
//     rn(9.22),
//     rs(16.7),
//     wh(0.2),
//     z0(5.3),
//     BX(4.6),
//     Theta0X(49.0*M_PI/180.0),
//     rcX(4.8),
//     rX(2.9),
//     p(11.5*M_PI/180.0)
//     {
//     bj = std::vector<double>(8,0);
//     bj[0] = 0.1;
//     bj[1] = 3.0;
//     bj[2] = -0.9;
//     bj[3] = -0.8;
//     bj[4] = -2.0;
//     bj[5] = -4.2;
//     bj[6] = 0.0;
//     bj[7] = 0.0;
//
//     fj = std::vector<double>(8,0);
//     fj[0] = 0.130;
//     fj[1] = 0.165;
//     fj[2] = 0.094;
//     fj[3] = 0.122;
//     fj[4] = 0.13;
//     fj[5] = 0.118;
//     fj[6] = 0.084;
//     fj[7] = 0.156;
//
//     for (int i = 0; i < 7; i++) bj[7] -= (fj[i]*bj[i]/fj[7]);
//
//     rj = std::vector<double>(8,0);
//     rj[0] = 5.1;
//     rj[1] = 6.3;
//     rj[2] = 7.1;
//     rj[3] = 8.3;
//     rj[4] = 9.8;
//     rj[5] = 11.4;
//     rj[6] = 12.7;
//     rj[7] = 15.5;
//     }*/
//
//	TJanssonFarrar12MagneticField(const double&, TGalaxyGrids*);//, TGeometry*);
//	virtual std::vector<double> getMagneticField(const double& x,const double& y,const double& z);
//
//protected :
//
//	std::vector<double> getMagneticFieldInCylindricalCoords(const double& r,const double& phi,const double& z);
//	std::vector<double> getMagneticFieldAveraged(const double& r,const double& z);
//	std::vector<double> bj;
//	std::vector<double> fj;
//	std::vector<double> rj;
//
//	double bring;
//	double hdisk;
//	double wdisk;
//	double Bn;
//	double Bs;
//	double rn;
//	double rs;
//	double wh;
//	double z0;
//	double BX;
//	double Theta0X;
//	double rcX;
//	double rX;
//	double p;
//};
//
///*class ToyModelField : public TBField {
//
//   public :
//   ToyModelField() { }
//   ToyModelField(double Bx_, double By_, double Bz_, double B0turb_, TGrid* Coord, TGeometry*);
//   virtual std::vector<double> GetField(double x, double y, double z);
//
//   protected :
//   double Bx, By, Bz;
//   double B0turb;
//   };*/
//

} /* namespace */

#endif
