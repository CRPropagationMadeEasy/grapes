#ifndef INCLUDE_PATTERN_H_
#define INCLUDE_PATTERN_H_

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "../TGrid3D.h"
#include "cgs.h"
#include "enums.h"
#include "exceptions.h"
#include "referenced.h"
#include "TPositionGrid.h"
#include "TVector3.h"

#include "readInputFile/TPatternParams.h"

namespace DRAGON {

class TPattern : public Referenced {

private:
	ref_ptr<ScalarGrid3D> patternFunction;
	std::vector<double> xGalaxy, yGalaxy, zGalaxy;
	size_t xSize, ySize, zSize;
	void buildPattern(const TPatternParams&);
	void calculatePattern(const TPatternParams&);

protected:
	virtual double computePatternFunction(const double& x_, const double& y_, const double& z_, const TPatternParams&) = 0;

public:
	TPattern() : xSize(0), ySize(0), zSize(0) { }
	virtual ~TPattern() { patternFunction->clearGrid(); xGalaxy.clear(); yGalaxy.clear(); zGalaxy.clear(); }

	inline ref_ptr<ScalarGrid3D> getPatternFunction() { return patternFunction; }

	void dumpToFile(const std::string& outputFilename);
};


class spiralPattern : public TPattern {

public:
	spiralPattern() : TPattern() { }
	double computePatternFunction(const double& x_, const double& y_, const double& z_, const TPatternParams&);

};


} /* namespace */

#endif
