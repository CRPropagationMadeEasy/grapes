#ifndef INCLUDE_GALAXySOURCEPROFILE_H_
#define INCLUDE_GALAXySOURCEPROFILE_H_

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "cgs.h"
#include "enums.h"
#include "exceptions.h"
#include "referenced.h"
#include "TPositionGrid.h"
#include "TGalaxyGrid.h"

#include "readInputFile/TSourceProfileParams.h"

namespace DRAGON {

class TSourceProfileConstant : public TGalaxyGrid {
	double constantValue;
public:
	TSourceProfileConstant(const double& constantValue) : TGalaxyGrid(), constantValue(constantValue) {}
	double distribution(const TVector3d& pos);
};

class TSourceProfileMyModel : public TGalaxyGrid {
public:
	TSourceProfileMyModel() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TSourceProfileGaussianModel : public TGalaxyGrid {
	GaussianParams pGaussian;
	TVector3d GaussianCentre;
public:
	TSourceProfileGaussianModel(GaussianParams params);
	double distribution(const TVector3d& pos);
};

class TSourceProfileFerriere01 : public TGalaxyGrid {
public:
	TSourceProfileFerriere01() : TGalaxyGrid() {}
	double distribution(const TVector3d& pos);
};

class TSourceProfileYusifov04 : public TGalaxyGrid {
	ProfileParams pYusifov04;
public:
	TSourceProfileYusifov04(ProfileParams params) : TGalaxyGrid(), pYusifov04(params) {}
	double distribution(const TVector3d& pos);
};

} /* namespace */

#endif /* INCLUDE_GALAXySOURCEPROFILE_H_ */
