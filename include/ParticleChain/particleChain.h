#ifndef INCLUDE_PARTICLE_CHAIN_H_
#define INCLUDE_PARTICLE_CHAIN_H_

#include <map>
#include <vector>
#include <fstream>
#include <sstream>

#include "../cgs.h"
#include "../readInputFile/TInput.h"

#include "logger.h"
#include "enums.h"
#include "utilities.h"

namespace DRAGON {

class TSpectrumAttributes {
public:
	TSpectrumAttributes(const double& ab, const double& rigCut, const std::vector<double>& sl, const std::vector<double>& rigBr) :
		abundance(ab), rigidityCutoff(rigCut), slopes(sl), rigidityBreaks(rigBr) {}
	TSpectrumAttributes() {}
	virtual ~TSpectrumAttributes() {}
	double abundance;
	double rigidityCutoff; //GeV
	std::vector<double> slopes;
	std::vector<double> rigidityBreaks; //GeV
};

class TDmSpectrumAttributes {
public:
	TDmSpectrumAttributes(const double& M, const double& cs, const double& tau, const std::string& filename) :
		mass(M), annihilationCrossSection(cs), decayLifetime(tau), spectrumFilename(filename) {}
	TDmSpectrumAttributes() {}
	virtual ~TDmSpectrumAttributes() {}
	double mass;
	double annihilationCrossSection; // PUT THE UNIT
	double decayLifetime;  // PUT THE UNIT
	std::string spectrumFilename;
};

class TParticleAttributes {
public:
	TParticleAttributes(const int& Z_, const unsigned int& A_, const double& decayLifetime_, const decayTypes& decayMode_, const std::string& name_, const TSpectrumAttributes& FirstPop_, const TSpectrumAttributes& SecondPop_, const TDmSpectrumAttributes& DarkMatterSpectrum_) :
	Z(Z_), A(A_), decayLifetime(decayLifetime_), decayMode(decayMode_), name(name_), FirstPop(FirstPop_), SecondPop(SecondPop_), DarkMatterSpectrum(DarkMatterSpectrum_) {}
	TParticleAttributes() {}
	virtual ~TParticleAttributes() {}

	int Z;
	unsigned int A;
	double decayLifetime;
	decayTypes decayMode;
	std::string name;
	TSpectrumAttributes FirstPop;
	TSpectrumAttributes SecondPop;
	TDmSpectrumAttributes DarkMatterSpectrum;
};

class TParticleChain : public Referenced {

public:
	TParticleChain(ref_ptr<TInput> in);

	//inline std::vector<int>& getList() { return (particleIds); }

	~TParticleChain() { particleChain.clear(); }

private:
	std::map<int, TParticleAttributes> particleChain;

	//std::vector<int> particleIds; /**< uid Array of unique IDs = 1000*Z+A .*/
	//std::vector<int> nucleiShortLived; /**< Array of short lived nuclei. */
	//std::map<int, double> nucleiLifetime; /**< Lifetimes of nuclei (stored according to their uid). */
	//std::map<int, double> nucleiNakedLifetime; /**< Lifetimes of nuclei (stored according to their uid). */
	//std::map<int, decayTypes> nucleiBetaDecay; /**< Whether nuclei decay beta+ or beta- . */

	void readParticleListFromFile();
	void addParticle(const std::string& line);
	bool doPropagate(int particleId);
	void sortParticleList();
	void updateAttribute();
};

} /* namespace */

#endif /* INCLUDE_PARTICLE_CHAIN_H_ */
