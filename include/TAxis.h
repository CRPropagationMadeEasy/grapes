#ifndef INCLUDE_TAXIS_H_
#define INCLUDE_TAXIS_H_

#include <algorithm>
#include <cmath>
#include <fstream>
#include <limits>
#include <string>
#include <vector>

#include "cgs.h"
#include "exceptions.h"
#include "referenced.h"

namespace DRAGON {

template<typename T>
class TAxis {
	T max, min;
	size_t size;
	std::vector<T> axis;
	std::vector<T> steps;

public:
	TAxis() : max(0), min(0), size(0) {}

	TAxis(const std::vector<T>& axis) {
		max = axis.max();
		min = axis.min();
		size = axis.size();
		this->axis = axis;
		this->steps = steps;
	}

	virtual ~TAxis(){ size = 0; axis.clear(); }

	T getMax() const { return (max); }

	T getMin() const { return (min); }

	size_t getSize() const { return (size); }

	T getValue(size_t ix) {
		return (axis[ix]);
	}

	std::vector<T>& getAxis() {
		return (axis);
	}

	std::vector<T>& getSteps() {
		return (steps);
	}

	void buildLinearAxis(T min, T max, size_t size) {
		this->max = max;
		this->min = min;
		this->size = size;
		double step = (size > 1) ? (max - min) / (T)(size - 1) : 0;
		for (size_t i = 0; i < size; ++i) {
			axis.push_back((T)i * step + min);
			steps.push_back(step);
		}
	}

	void buildGeometricalAxis(T min, T max, size_t size) {
		this->max = max;
		this->min = min;
		this->size = size;
		double step = (size > 1) ? std::pow(max / min, 1.0 / (T)(size - 1)) : 0;
		for (size_t i = 0; i < size; ++i) {
			axis.push_back(min * std::pow(step, (T)i));
			steps.push_back(step);
		}
	}

	void loadAxisFromFile(const std::string& filename) {
		std::ifstream fin(filename.c_str());
		if (!fin) {
			throw TDragonExceptions("buildAxis : "+filename+" not found");
		}
		// skip header lines
		while (fin.peek() == '#')
			fin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		// read until eof
		while (!fin.eof()) {
			double value;
			fin >> value;
			if (!fin.eof()) axis.push_back(value * kpc);
		}
		fin.close();
	}

	void buildNegativeAxis() {
		std::vector<double> reverseAxis;
		for (size_t i = axis.size() - 1; i > 0; --i)
			reverseAxis.push_back(-axis.at(i));

		axis.insert(axis.begin(), reverseAxis.begin(), reverseAxis.end());
	}

	void sortAxis() {
		std::sort (axis.begin(), axis.end());
		if (axis.front() < -1e-10 or axis.front() > 1e-10)
			throw TDragonExceptions("The not-equidistant grid has to be only positive and including 0");
	}

	void buildStepsAxis() {
		for (size_t i = 0; i < axis.size(); ++i) {
			if (i < axis.size() -1)
				steps.push_back(axis[i+1]-axis[i]);
			else
				steps.push_back(0);
		}
	}

	void buildAxis(const std::string& filename, bool doNegativeAxis) {
		loadAxisFromFile(filename);
		sortAxis();
		if (doNegativeAxis)
			buildNegativeAxis();
		buildStepsAxis();
		max = axis.back();
		min = axis.front();
		size = axis.size();
	}
};

} /* namespace */

#endif /* INCLUDE_TAXIS_H_ */
