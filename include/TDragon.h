#ifndef INCLUDE_TDRAGON_H_
#define INCLUDE_TDRAGON_H_

#include <iostream>
#include <string>

#include "Galaxy/TGalaxy.h"
#include "referenced.h"
#include "pugixml.hpp"
#include "readInputFile/TInput.h"
#include "TEnergyAxes.h"
#include "TPositionGrid.h"

using namespace pugi;

namespace DRAGON {

class TDragon {
private:
	ref_ptr<TInput> in;
	ref_ptr<TPositionGrid> position;
	ref_ptr<TEnergyAxes> energyAxes;
	ref_ptr<TGalaxy> galaxy;
	std::string xml_filename;
	std::string root_filename;

public:
	TDragon();
	void loadInputFile(const std::string &filename);
	void run();
	void buildPositionGrid(const TAxisParams& pAxis);
	void buildEnergyAxes(const TAxisParams& pAxis);
	void buildGalaxy(const TGalaxyParams& pGalaxy);
};

} // namespace

#endif /* INCLUDE_TDRAGON_H_ */
