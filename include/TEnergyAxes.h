#ifndef INCLUDE_GALAXY_GALAXYENERGYAXES_H_
#define INCLUDE_GALAXY_GALAXYENERGYAXES_H_

#include <cmath>
#include <string>
#include <vector>

#include "readInputFile/TAxisParams.h"
#include "TAxis.h"

namespace DRAGON {

class TEnergyAxes : public Referenced {
public:
	TEnergyAxes() {}
	TEnergyAxes(const TAxisParams& pGrid);
	~TEnergyAxes() {}

	std::vector<double>& getKEnergyAxis();
	double getMin();
	double getMax();

protected:
	TAxis<double> kineticEnergy;
};

} /* namespace */

#endif /* INCLUDE_GALAXY_GALAXYENERGYAXES_H_ */
