#ifndef _GRIDS_H
#define _GRIDS_H

#include <vector>

#include "exceptions.h"
#include "referenced.h"
#include "TVector3.h"
#include "utilities.h"

namespace DRAGON {

template<typename T>
class TGrid3D : public Referenced {
	std::vector<T> grid;
	size_t Nx, Ny, Nz;

public:
	TGrid3D(size_t Nx, size_t Ny, size_t Nz) {
		setGridSize(Nx, Ny, Nz);
	}

	void setGridSize(size_t Nx, size_t Ny, size_t Nz) {
		this->Nx = Nx;
		this->Ny = Ny;
		this->Nz = Nz;
		grid.resize(Nx * Ny * Nz);
	}

	/** Accessor / Mutator */
	T &get(size_t ix, size_t iy, size_t iz) {
		return (grid[ix * Ny * Nz + iy * Nz + iz]);
	}

	T &get(const size_t& i) {
		return (grid[i]);
	}

	/** Accessor */
	const T &get(size_t ix, size_t iy, size_t iz) const {
		return (grid[ix * Ny * Nz + iy * Nz + iz]);
	}

	const T &get(const size_t& i) const {
		return (grid[i]);
	}

	T getValue(size_t ix, size_t iy, size_t iz) {
		return (grid[ix * Ny * Nz + iy * Nz + iz]);
	}

	/** Return a reference to the grid values */
	std::vector<T> &getGrid() {
		return (grid);
	}

	void clearGrid() {
		grid.clear();
		return ;
	}

	size_t getNx() const {
		return (Nx);
	}

	void setNx(size_t nx) {
		Nx = nx;
	}

	size_t getNy() const {
		return (Ny);
	}

	void setNy(size_t ny) {
		Ny = ny;
	}

	size_t getNz() const {
		return (Nz);
	}

	void setNz(size_t nz) {
		Nz = nz;
	}

	size_t getNElements() const {
		return (Nx * Ny * Nz);
	}
};

typedef TGrid3D<double> ScalarGrid3D;
typedef TGrid3D<TVector3<double> > VectorGrid;

} // namespace

#endif
