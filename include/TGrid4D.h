#ifndef INCLUDE_TGRID4D_H_
#define INCLUDE_TGRID4D_H_

namespace DRAGON {

template<typename T>
class TGrid4D: public Referenced {
	std::vector<T> grid;
	size_t Nx, Ny, Nz;
	size_t Nk;

public:
	TGrid4D(size_t Nx, size_t Ny, size_t Nz, size_t Nk) {
		setGridSize(Nx, Ny, Nz, Nk);
	}

	void setGridSize(size_t Nx, size_t Ny, size_t Nz, size_t Nk) {
		this->Nx = Nx;
		this->Ny = Ny;
		this->Nz = Nz;
		this->Nk = Nk;
		grid.resize(Nx * Ny * Nz * Nk);
	}

	/** Accessor / Mutator */
	T &get(size_t ix, size_t iy, size_t iz, size_t ik) {
		return (grid[ix * Ny * Nz * Nk + iy * Nz * Nk + iz * Nk + ik]);
	}

	/** Accessor */
	const T &get(size_t ix, size_t iy, size_t iz, size_t ik) const {
		return (grid[ix * Ny * Nz * Nk + iy * Nz * Nk + iz * Nk + ik]);
	}

	T getValue(size_t ix, size_t iy, size_t iz, size_t ik) {
		return (grid[ix * Ny * Nz * Nk + iy * Nz * Nk + iz * Nk + ik]);
	}

	/** Return a reference to the grid values */
	std::vector<T> &getGrid() {
		return (grid);
	}

	const T &get(const size_t& i) const {
		return (grid[i]);
	}

	void clearGrid() {
		grid.clear();
		return ;
	}

	size_t getNx() const {
		return (Nx);
	}

	void setNx(size_t nx) {
		Nx = nx;
	}

	size_t getNy() const {
		return (Ny);
	}

	void setNy(size_t ny) {
		Ny = ny;
	}

	size_t getNz() const {
		return (Nz);
	}

	void setNz(size_t nz) {
		Nz = nz;
	}

	size_t getNk() const {
		return (Nk);
	}

	void setNk(size_t nk) {
		Nk = nk;
	}

	size_t getNElements() const {
		return (Nx * Ny * Nz * Nk);
	}
};

typedef TGrid4D<double> ScalarGrid4D;

} /* namespace */

#endif /* INCLUDE_TGRID4D_H_ */
