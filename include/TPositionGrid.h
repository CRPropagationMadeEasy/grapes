#ifndef INCLUDE_GALAXY_GALAXYSPATIALAXES_H_
#define INCLUDE_GALAXY_GALAXYSPATIALAXES_H_

#include <cmath>
#include <string>
#include <vector>

#include "TAxis.h"
#include "TVector3.h"

#include "readInputFile/TAxisParams.h"
#include "TGrid3D.h"

namespace DRAGON {

class TPositionGrid : public Referenced {
protected:
	TAxisParams pAxis;
	ref_ptr<VectorGrid> grid;
	size_t Nx, Ny, Nz;

public:
	TPositionGrid(const TAxisParams& pAxis_);

	gridTypes getGridType() const;
	size_t getNx() const;
	size_t getNy() const;
	size_t getNz() const;
	size_t getNElements() const;
	double getXMin() const;
	double getYMax() const;
	double getZMin() const;
	double getXMax() const;
	double getYMin() const;
	double getZMax() const;
	void setGrid(ref_ptr<VectorGrid> grid);
	ref_ptr<VectorGrid> getGrid();
	TVector3d getPosition(const size_t& ix, const size_t& iy, const size_t& iz) const;
	TVector3d getPosition(const size_t& i) const;
	TVector3d getFirstPosition() const;
	TVector3d getLastPosition() const;
};

} /* namespace */

#endif /* INCLUDE_GALAXY_GALAXYSPATIALAXES_H_ */
