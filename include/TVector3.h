#ifndef INCLUDE_TVECTOR3_H_
#define INCLUDE_TVECTOR3_H_

#include <iostream>
#include <cmath>
#include <vector>
#include <limits>

namespace DRAGON {

template<typename T>
class TVector3 {
public:
	T x, y, z;

	TVector3() :
		x(0), y(0), z(0) {
	}

	// Provides implicit conversion
	template<typename U>
	TVector3(const TVector3<U> &v) :
	x(v.x), y(v.y), z(v.z) {
	}

	explicit TVector3(const double *v) : x(v[0]), y(v[1]), z(v[2]) {
	}

	explicit TVector3(const float *v) : x(v[0]), y(v[1]), z(v[2]) {
	}

	explicit TVector3(const T &X, const T &Y, const T &Z) : x(X), y(Y), z(Z) {
	}

	explicit TVector3(T t) : x(t), y(t), z(t) {
	}

	void setX(const T X) {
		x = X;
	}

	void setY(const T Y) {
		y = Y;
	}

	void setZ(const T Z) {
		z = Z;
	}

	void setXYZ(const T X, const T Y, const T Z) {
		x = X;
		y = Y;
		z = Z;
	}

	T getX() const {
		return (x);
	}

	T getY() const {
		return (y);
	}

	T getZ() const {
		return (z);
	}

	//void setR(const T r) {
	//	*this *= r / getR();
	//}

	void setRThetaZ(const T r, const T theta, const T z) {
		this->x = r * sin(theta);
		this->y = r * cos(theta);
		this->z = z;
	}

	void setValuesCylindrical(const T r, const T theta, const T z, const T thetaCartesian) {
		this->x = r * cos(thetaCartesian) - theta * sin(thetaCartesian);
		this->y = r * sin(thetaCartesian) + theta * cos(thetaCartesian);
		this->z = z;
	}

	void addValuesCylindrical(const T r, const T theta, const T z, const T thetaCartesian) {
		this->x += (r * cos(thetaCartesian) - theta * sin(thetaCartesian));
		this->y += (r * sin(thetaCartesian) + theta * cos(thetaCartesian));
		this->z += z;
	}

	// magnitude (2-norm) of the vector
	T getModule() const {
		return (std::sqrt(x * x + y * y + z * z));
	}

	T getR() const {
		return (std::sqrt(x * x + y * y));
	}

	// square of magnitude of the vector
	T getR2() const {
		return (x * x + y * y);
	}

	// return the azimuth angle
	T getTheta() const {
		T eps = std::numeric_limits < T > ::min();
		if ((fabs(x) < eps) and (fabs(y) < eps))
			return (0.0);
		else
			return (std::atan2(y, x));
	}

	// return the unit-vector e_r
	TVector3<T> getUnitVector() const {
		return (*this / getModule());
	}

	// return the unit-vector e_theta
	TVector3<T> getUnitVectorTheta() const {
		return (TVector3<T>(-sin(getTheta()), cos(getTheta()), 0));
	}

	// return the angle [0, pi] between the vectors
	T getAngleTo(const TVector3<T> &v) const {
		T cosdistance = dot(v) / v.getR() / getR();
		// In some directions cosdistance is > 1 on some compilers
		// This ensures that the correct result is returned
		if (cosdistance >= 1.)
			return (0);
		else if (cosdistance <= -1.)
			return (M_PI);
		else
			return (acos(cosdistance));
	}

	// return true if the angle between the vectors is smaller than a threshold
	bool isParallelTo(const TVector3<T> &v, T maxAngle) const {
		return (getAngleTo(v) < maxAngle);
	}

	// linear distance to a given vector
	T getDistanceTo(const TVector3<T> &point) const {
		TVector3<T> d = *this - point;
		return (d.getR());
	}

	// return the component parallel to a second vector
	// 0 if the second vector has 0 magnitude
	TVector3<T> getParallelTo(const TVector3<T> &v) const {
		T vmag = v.getR();
		if (vmag == std::numeric_limits < T > ::min())
			return (TVector3<T>(0.));
		return (v * dot(v) / vmag);
	}

	// return the component perpendicular to a second vector
	// 0 if the second vector has 0 magnitude
	TVector3<T> getPerpendicularTo(const TVector3<T> &v) const {
		if (v.getR() == std::numeric_limits < T > ::min())
			return (TVector3<T>(0.));
		return ((*this) - getParallelTo(v));
	}

	// rotate the vector around a given axis by a given a angle
	//TVector3<T> getRotated(const TVector3<T> &axis, T angle) const {
	//	TVector3<T> u = axis / axis.getR();
	//	T c = cos(angle);
	//	T s = sin(angle);
	//	TVector3<T> Rx(c + u.x * u.x * (1 - c), u.x * u.y * (1 - c) - u.z * s,
	//			u.x * u.z * (1 - c) + u.y * s);
	//	TVector3<T> Ry(u.y * u.x * (1 - c) + u.z * s, c + u.y * u.y * (1 - c),
	//			u.y * u.z * (1 - c) - u.x * s);
	//	TVector3<T> Rz(u.z * u.x * (1 - c) - u.y * s,
	//			u.z * u.y * (1 - c) + u.x * s, c + u.z * u.z * (1 - c));
	//	return (TVector3<T>(dot(Rx), dot(Ry), dot(Rz)));
	//}

	TVector3<T> getRotated2D(T angle) const {
		T r = getR();
		T X = r * std::sin(angle);
		T Y	= r * std::cos(angle);
		return (TVector3<T>(X, Y, z));
	}

	// return vector with values limited to the range [lower, upper]
	TVector3<T> clip(T lower, T upper) const {
		TVector3<T> out;
		out.x = std::max(lower, std::min(x, upper));
		out.y = std::max(lower, std::min(y, upper));
		out.z = std::max(lower, std::min(z, upper));
		return (out);
	}

	// return vector with absolute values
	TVector3<T> abs() const {
		return (TVector3<T>(std::abs(x), std::abs(y), std::abs(z)));
	}

	// return vector with floored values
	TVector3<T> floor() const {
		return (TVector3<T>(std::floor(x), std::floor(y), std::floor(z)));
	}

	// return vector with ceiled values
	TVector3<T> ceil() const {
		return (TVector3<T>(std::ceil(x), std::ceil(y), std::ceil(z)));
	}

	// minimum element
	T min() const {
		return (std::min(x, std::min(y, z)));
	}

	// maximum element
	T max() const {
		return (std::max(x, std::max(y, z)));
	}

	// dot product
	T dot(const TVector3<T> &v) const {
		return (x * v.x + y * v.y + z * v.z);
	}

	// cross product
	TVector3<T> cross(const TVector3<T> &v) const {
		return (TVector3<T>(y * v.z - v.y * z, z * v.x - v.z * x,
				x * v.y - v.x * y));
	}

	// returns true if all elements of the first vector are smaller than those in the second vector
	bool operator <(const TVector3<T> &v) const {
		if (x > v.x)
			return (false);
		else if (x < v.x)
			return (true);
		if (y > v.y)
			return (false);
		else if (y < v.y)
			return (true);
		if (z >= v.z)
			return (false);
		else
			return (true);
	}

	// returns true if all elements of the two vectors are equal
	bool operator ==(const TVector3<T> &v) const {
		if (x != v.x)
			return (false);
		if (y != v.y)
			return (false);
		if (z != v.z)
			return (false);
		return (true);
	}

	TVector3<T> operator +(const TVector3<T> &v) const {
		return (TVector3(x + v.x, y + v.y, z + v.z));
	}

	TVector3<T> operator +(const T &f) const {
		return (TVector3(x + f, y + f, z + f));
	}

	TVector3<T> operator -(const TVector3<T> &v) const {
		return (TVector3(x - v.x, y - v.y, z - v.z));
	}

	TVector3<T> operator -(const T &f) const {
		return (TVector3(x - f, y - f, z - f));
	}

	// element-wise multiplication
	TVector3<T> operator *(const TVector3<T> &v) const {
		return (TVector3(x * v.x, y * v.y, z * v.z));
	}

	TVector3<T> operator *(const T &v) const {
		return (TVector3(x * v, y * v, z * v));
	}

	// element-wise division
	TVector3<T> operator /(const TVector3<T> &v) const {
		return (TVector3(x / v.x, y / v.y, z / v.z));
	}

	TVector3<T> operator /(const T &f) const {
		return (TVector3(x / f, y / f, z / f));
	}

	// element-wise modulo operation
	TVector3<T> operator %(const TVector3<T> &v) const {
		return (TVector3(fmod(x, v.x), fmod(y, v.y), fmod(z, v.z)));
	}

	TVector3<T> operator %(const T &f) const {
		return (TVector3(fmod(x, f), fmod(y, f), fmod(z, f)));
	}

	TVector3<T> &operator -=(const TVector3<T> &v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return (*this);
	}

	TVector3<T> &operator -=(const T &f) {
		x -= f;
		y -= f;
		z -= f;
		return (*this);
	}

	TVector3<T> &operator +=(const TVector3<T> &v) {
		x += v.x;
		y += v.y;
		z += v.z;
		return (*this);
	}

	TVector3<T> &operator +=(const T &f) {
		x += f;
		y += f;
		z += f;
		return (*this);
	}

	// element-wise multiplication
	TVector3<T> &operator *=(const TVector3<T> &v) {
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return (*this);
	}

	TVector3<T> &operator *=(const T &f) {
		x *= f;
		y *= f;
		z *= f;
		return (*this);
	}

	// element-wise division
	TVector3<T> &operator /=(const TVector3<T> &v) {
		x /= v.x;
		y /= v.y;
		z /= v.z;
		return (*this);
	}

	TVector3<T> &operator /=(const T &f) {
		x /= f;
		y /= f;
		z /= f;
		return (*this);
	}

	// element-wise modulo operation
	TVector3<T> &operator %=(const TVector3<T> &v) {
		x = fmod(x, v.x);
		y = fmod(y, v.y);
		z = fmod(z, v.z);
		return (*this);
	}

	TVector3<T> &operator %=(const T &f) {
		x = fmod(x, f);
		y = fmod(y, f);
		z = fmod(z, f);
		return (*this);
	}

	TVector3<T> &operator =(const TVector3<T> &v) {
		x = v.x;
		y = v.y;
		z = v.z;
		return (*this);
	}

	TVector3<T> &operator =(const T &f) {
		x = f;
		y = f;
		z = f;
		return (*this);
	}
};

#ifndef SWIG
template<typename T>
inline std::ostream &operator <<(std::ostream &out, const TVector3<T> &v) {
	out << v.x << " " << v.y << " " << v.z;
	return (out);
}

template<typename T>
inline std::istream &operator >>(std::istream &in, TVector3<T> &v) {
	in >> v.x >> v.y >> v.z;
	return (in);
}
#endif

template<typename T>
inline TVector3<T> operator *(T f, const TVector3<T> &v) {
	return (TVector3<T>(v.x * f, v.y * f, v.z * f));
}

typedef TVector3<double> TVector3d;
//typedef TVector3<float> TVector3f;

} /* namespace */

#endif /* INCLUDE_TVECTOR3_H_ */
