#ifndef INCLUDE_DRAGON_CFITSIOWRAPPER_H_
#define INCLUDE_DRAGON_CFITSIOWRAPPER_H_

#include <string>
#include <vector>

#include "fitsio.h"

namespace DRAGON {

class TReadCfitsioFile {
	std::string fitsFilename;
	fitsfile * fptr;

public:

	TReadCfitsioFile(const std::string& fitsFilename_) : fitsFilename(fitsFilename_) {
		int status = 0;
		fptr = NULL;
		if (fits_open_file(&fptr, fitsFilename.c_str(), READONLY, &status)) fits_report_error(stderr, status);
	}

	~TReadCfitsioFile() {
		int status = 0;
		if (fits_close_file(fptr, &status)) fits_report_error(stderr, status);
	}

	int readIntKeyword(const std::string& keywordName);
	double readDoubleKeyword(const std::string& keywordName);
	std::vector<double> readDoubleImage(long nelements);
};

} // namespace

#endif /* INCLUDE_DRAGON_CFITSIOWRAPPER_H_ */
