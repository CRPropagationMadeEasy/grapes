#ifndef INCLUDE_ENUMS_H_
#define INCLUDE_ENUMS_H_

#include <string>

enum decayTypes { STABLE, BM, EC, BB, IT, BP, ECBM, ECBP };
enum gridTypes { g2D, g3D };
enum dxxTypes { dxxConstant , dxxMyModel };
enum dppTypes { dppBerezinskii90, dppMyModel };

enum gasH2Types { H2Constant, H2MyModel, H2Bronfman88, H2Nakanishi06, H2Ferriere07, H2Pohl08 };
enum gasHITypes { HIConstant, HIMyModel, HIMoskalenko02, HINakanishi03, HIFerriere07 };
enum gasHIITypes { HIIConstant, HIIMyModel, HIICordes91, HIINE2001, HIIFerriere07 };
enum gasXcoTypes { XcoConstant, XcoMyModel, XcoArimoto96, XcoStrong04, XcoAckermann10, XcoEvoli12 };
enum sourceProfileTypes { sourceProfileConstant, sourceProfileMyModel, sourceProfileGaussianModel, sourceProfileLorimer06, sourceProfileYusifov04, sourceProfileCase98, sourceProfileFerriere01 };
enum darkProfileTypes { darkProfileConstant, darkProfileMyModel, darkProfileGNFW, darkProfileCored, darkProfileEinasto65, darkProfileStadel09 };
enum isrfTypes { IsrfConstant, IsrfMyModel, IsrfGalpropTable, IsrfDelahaye10 };
enum patternTypes { PatternConstant, PatternSpiralArms };

enum magneticFieldTypes { MagFieldConstant, MagFieldMyModel, MagFieldExponential, MagFieldSun07, MagFieldPshirkov11ASS, MagFieldPshirkov11BSS, MagFieldFarrar12 };

#endif /* INCLUDE_ENUMS_H_ */
