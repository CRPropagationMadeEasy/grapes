#ifndef _DRAGON_EXCEPTIONS_H
#define _DRAGON_EXCEPTIONS_H

#include <exception>
#include <iostream>
#include <string>
#include <stdexcept>

namespace DRAGON {

class TDragonExceptions: public std::runtime_error {
private:
	std::string reason;

public:
	TDragonExceptions() : runtime_error("Generic error in DRAGON"), reason() { }
	TDragonExceptions(const std::string& str) : std::runtime_error(str), reason(str) {}
	virtual ~TDragonExceptions() throw() {};
};

} // namespace

#endif
