#ifndef __LOGGER_H
#define __LOGGER_H

#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "TVector3.h"

namespace DRAGON {

enum LogLevel { NONE, ERROR, INFO, DEBUG };

class Logger {

public:

	// Returns a reference to the singleton Logger object
	static Logger& cout(LogLevel);

	static void setLogLevelScreen(const LogLevel& v){ LogLevelScreen = v; }

	static void setLogLevelFile(const LogLevel& v){ LogLevelFile = v; }

	// Logs a single message at the given log level
	void log(const std::string& inMessage);

	// Logs a vector of messages at the given log level
	void log(const std::vector<std::string>& inMessages);

	friend Logger& operator << (Logger &out, int value)
	{
		std::ostringstream s;
		s << value;
		out << s.str();
		return (out);
	};

	friend Logger& operator << (Logger &out, unsigned int value)
	{
		std::ostringstream s;
		s << value;
		out << s.str();
		return (out);
	};

	friend Logger& operator << (Logger &out, unsigned long value)
	{
		std::ostringstream s;
		s << value;
		out << s.str();
		return (out);
	};

	friend Logger& operator << (Logger &out, const double& value)
	{
		std::ostringstream s;
		s << std::scientific << std::setprecision(3) << value;
		out << s.str();
		return (out);
	};

	friend Logger& operator << (Logger &out, const std::vector<double>& value)
	{
		std::ostringstream s;
		if (!value.empty())
		{
			if (value.size() > 1)
				s << std::scientific << std::setprecision(3) << value[0] << " " << value[1] << " ... " << value[value.size()-1] << " , " << value.size();
			else
				s << std::scientific << std::setprecision(3) << value[0] << " , " << value.size();
			out << s.str();
		}
		else
			out << "NULL";
		return (out);
	};

	friend Logger& operator << (Logger &out, std::string value)
	{
		if (LogLevelCout <= LogLevelFile) LogStream << value;
		if (LogLevelCout <= LogLevelScreen) std::cout << value;
		return (out);
	};

	friend Logger& operator << (Logger &out, TVector3d value)
	{
		if (LogLevelCout <= LogLevelFile) LogStream << value.x << " " << value.y << " " << value.z;
		if (LogLevelCout <= LogLevelScreen) std::cout << value.x << " " << value.y << " " << value.z;
		return (out);
	};

protected:

	static Logger Instance;
	static const char* const LogFileName;
	static LogLevel LogLevelCout;
	static LogLevel LogLevelScreen;
	static LogLevel LogLevelFile;
	static std::ofstream LogStream;

private:

	Logger();
	~Logger();

};

} // namespace

#endif
