#ifndef INCLUDE_MATRIX_H_
#define INCLUDE_MATRIX_H_

#include <vector>

namespace DRAGON {
class TMatrix3{
public:

	TMatrix3(){ nx=ny=nz=1; }
	TMatrix3(unsigned int& nx, const unsigned int& ny, const unsigned int& nz)
	{
		this->nx=nx;
		this->ny=ny;
		this->nz=nz;

		m.resize(nx*ny*nz);
	}

	~TMatrix3()
	{
		m.clear();
	}

protected:

	unsigned int nx;
	unsigned int ny;
	unsigned int nz;

	std::vector<double> m;
};

} /* namespace */

#endif /* INCLUDE_MATRIX_H_ */
