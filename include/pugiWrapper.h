#ifndef INCLUDE_PUGIWRAPPER_H_
#define INCLUDE_PUGIWRAPPER_H_

#include <string>

#include "enums.h"
#include "exceptions.h"
#include "pugixml.hpp"

namespace pugi {

bool childExist(xml_node parent, std::string childName);

bool childAttributeExist(xml_node parent, std::string childName);

int childIntValue(xml_node parent, std::string childName, bool throwIfEmpty = true);

double childDoubleValue(xml_node parent, std::string childName, double units, bool throwIfEmpty = true);

double childDoubleAttribute(xml_node child, std::string attribute, double units, bool throwIfEmpty = true);

std::string childStringValue(xml_node parent, std::string childName, bool throwIfEmpty = true);

std::string childStringAttribute(xml_node child, std::string attribute, bool throwIfEmpty = true);

pugi::xml_node childNode(xml_node parent, std::string childName, bool throwIfEmpty = true);

} /* namespace */

#endif /* INCLUDE_PUGIWRAPPER_H_ */
