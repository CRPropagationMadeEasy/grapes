#ifndef INCLUDE_READINPUTFILE_TAXISPARAMS_H_
#define INCLUDE_READINPUTFILE_TAXISPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"

using namespace pugi;

namespace DRAGON {

class TAxisParams {
public:
	TAxisParams() :
		Type(g2D), doEquidistantInX(true), doEquidistantInY(true), doEquidistantInZ(true),
		xSize(1), xMin(0), xMax(0),
		ySize(1), yMin(0), yMax(0),
		zSize(1), zMin(0), zMax(0),
		EkSize(1), EkMin(0), EkMax(0) {}
	~TAxisParams(){};
	void setParams(xml_node &node);
	void print();

	void readSpatialDimensions(xml_node &node);
	void readXParams(xml_node &node);
	void readYParams(xml_node &node);
	void readZParams(xml_node &node);
	void readEnergyParams(xml_node &node);

	gridTypes Type;

	bool doEquidistantInX;
	bool doEquidistantInY;
	bool doEquidistantInZ;

	size_t xSize;
	double xMin;
	double xMax;

	size_t ySize;
	double yMin;
	double yMax;

	size_t zSize;
	double zMin;
	double zMax;

	size_t EkSize;
	double EkMin;
	double EkMax;

	std::string xValuesFilename;
	std::string yValuesFilename;
	std::string zValuesFilename;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TAXISPARAMS_H_ */
