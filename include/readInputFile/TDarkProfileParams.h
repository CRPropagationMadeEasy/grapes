#ifndef INCLUDE_READINPUTFILE_TDARKPROFILEPARAMS_H_
#define INCLUDE_READINPUTFILE_TDARKPROFILEPARAMS_H_

#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TDarkProfileParams {
	void setDarkProfileParamsFromXML(xml_node &node);

public:
	TDarkProfileParams() : Type(darkProfileConstant), ConstantValue(0), alpha(0), beta(0), gamma(0), Mass(0), Rmax(0), LocalDensity(0) {}
	~TDarkProfileParams() {};
	void setParams(xml_node &node);
	void print();
	darkProfileTypes Type;
	std::string stringType;
	double ConstantValue;
	double alpha;
	double beta;
	double gamma;
	double Mass;
	double Rmax;
	double LocalDensity;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TDARKPROFILEPARAMS_H_ */
