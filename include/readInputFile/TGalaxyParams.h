#ifndef INCLUDE_READINPUTFILE_TGALAXYPARAMS_H_
#define INCLUDE_READINPUTFILE_TGALAXYPARAMS_H_

#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"

#include "readInputFile/TGasH2Params.h"
#include "readInputFile/TGasHIParams.h"
#include "readInputFile/TGasHIIParams.h"
#include "readInputFile/TXcoParams.h"
#include "readInputFile/TIsrfParams.h"
#include "readInputFile/TMagneticFieldParams.h"
#include "readInputFile/TSourceProfileParams.h"
#include "readInputFile/TDarkProfileParams.h"
#include "readInputFile/TPatternParams.h"

using namespace pugi;

namespace DRAGON {

class TGalaxyParams {
public:
	TGalaxyParams() : smoothGasRadius(-1), doDumpToFile(false) {}
	~TGalaxyParams(){};
	void setParams(xml_node &node);
	void print();

	TGasH2Params pH2;
	TGasHIParams pHI;
	TGasHIIParams pHII;
	TXcoParams pXco;
	TMagneticFieldParams pMagneticField;
	TIsrfParams pIsrf;
	TSourceProfileParams pSourceProfile;
	TDarkProfileParams pDarkProfile;
	TPatternParams pPattern;

	double smoothGasRadius;
	bool doDumpToFile;
};


} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TGALAXYPARAMS_H_ */
