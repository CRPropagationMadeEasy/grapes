#ifndef INCLUDE_READINPUTFILE_TGASH2PARAMS_H_
#define INCLUDE_READINPUTFILE_TGASH2PARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TGasH2Params {
public:
	TGasH2Params() : type(H2Constant), constantValue(0), smoothGasRadius(0) {}
	~TGasH2Params() {};
	void setParams(xml_node &node, const double& smoothGasRadius_);
	void print();
	gasH2Types type;
	std::string stringType;
	double constantValue;
	double smoothGasRadius;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TGASH2PARAMS_H_ */
