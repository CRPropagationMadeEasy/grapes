#ifndef INCLUDE_READINPUTFILE_TGASHIIPARAMS_H_
#define INCLUDE_READINPUTFILE_TGASHIIPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TGasHIIParams {
public:
	TGasHIIParams() : type(HIIConstant), constantValue(0), smoothGasRadius(0) {}
	~TGasHIIParams() {};
	void setParams(xml_node &node, const double& smoothGasRadius_);
	void print();
	gasHIITypes type;
	std::string stringType;
	double constantValue;
	double smoothGasRadius;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TGASHIIPARAMS_H_ */
