#ifndef SRC_READINPUTFILE_TGASHIPARAMS_H_
#define SRC_READINPUTFILE_TGASHIPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TGasHIParams {
public:
	TGasHIParams() : type(HIConstant), constantValue(0), smoothGasRadius(0) {}
	~TGasHIParams() {};
	void setParams(xml_node &node, const double& smoothGasRadius_);
	void print();
	gasHITypes type;
	std::string stringType;
	double constantValue;
	double smoothGasRadius;
};

} /* namespace */

#endif /* SRC_READINPUTFILE_TGASHIPARAMS_H_ */
