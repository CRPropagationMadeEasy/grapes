#ifndef __READ_INPUT_FILE_H
#define __READ_INPUT_FILE_H

#include <string>

#include "../cgs.h"
#include "enums.h"
#include "exceptions.h"
#include "logger.h"
#include "referenced.h"
#include "pugiWrapper.h"
#include "utilities.h"
#include "readInputFile/TObserverParams.h"
#include "readInputFile/TGalaxyParams.h"
#include "readInputFile/TOutputModeParams.h"

#include "pugixml.hpp"
#include "TAxisParams.h"

using namespace pugi;

namespace DRAGON {

class TInput: public Referenced {

public:

	TInput(xml_node root);
	~TInput() {};

	TOutputModeParams OutputNode;
	TObserverParams ObserverNode;
	TAxisParams AxisNode;
	TGalaxyParams GalaxyNode;

private:

	void printParams();
	//std::string inputFilename;
	//std::string outputFilenameInit;
	//std::string sourceParamsFilename;
	//void setOutputFilenameInit();
	//void setSourceParamFilename();
};

/*class TSpatialDiffusionParams {
public:
	TSpatialDiffusionParams() : type(dxxConstant),D0inCGS(1e28),delta(0.5),refRigidityInGV(3) {}
	~TSpatialDiffusionParams(){}
	void print();

	dxxTypes type;
	double D0inCGS;
	double delta;
	double refRigidityInGV;
};*/

/*class TMomentumDiffusionParams {
public:
	TMomentumDiffusionParams() : type(dppBerezinskii90),vAinKMS(10.0) {}
	~TMomentumDiffusionParams(){}
	void print();

	dppTypes type;
	double vAinKMS;
};*/

/*class TSourceParams {
public:
	TSourceParams(){}
	~TSourceParams(){}
	void print();
};*/

/*class TParticleChainParams {
public:
	TParticleChainParams() : Zmax(2), Zmin(1), doPropAntiprotons(false), doPropLeptons(false), doPropLeptonsExtraComponent(false){}
	~TParticleChainParams(){}
	void print();

	int Zmax;
	int Zmin;
	bool doPropAntiprotons;
	bool doPropLeptons;
	bool doPropLeptonsExtraComponent;
};*/

/*class TNormalizations {
public:
	TNormalizations() : ProtonNormEnergy(100),ElectronNormEnergy(33),ExtraComponentNormEnergy(300),
	ProtonNormValue(5.0e-2),ElectronNormValue(0.004),ExtraComponentNormValue(1.e-06) {}
	~TNormalizations(){}
	void print();

	double ProtonNormEnergy;
	double ElectronNormEnergy;
	double ExtraComponentNormEnergy;
	double ProtonNormValue;
	double ElectronNormValue;
	double ExtraComponentNormValue;
};*/

/*class TInjectionSpectra {
public:
	TInjectionSpectra() : doInjectionIndexAllNuclei(false) {}
	~TInjectionSpectra(){}
	void print();

	bool doInjectionIndexAllNuclei;
};*/

} // namespace

#endif
