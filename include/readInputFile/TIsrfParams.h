#ifndef INCLUDE_READINPUTFILE_TISRFPARAMS_H_
#define INCLUDE_READINPUTFILE_TISRFPARAMS_H_

#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"
#include "Galaxy/blackbody.h"

using namespace pugi;

namespace DRAGON {

class TIsrfParams {

	void setDefaultTemperatures();
	void setIRTemperatureFromXML(xml_node &node);
	void setOpticalTemperatureFromXML(xml_node &node);
	void setUVITemperatureFromXML(xml_node &node);
	void setUVIITemperatureFromXML(xml_node &node);
	void setUVIIITemperatureFromXML(xml_node &node);

public:

	TIsrfParams() : Type(IsrfConstant),ConstantValue(1.) {}
	~TIsrfParams() {};
	void setParams(xml_node &node);
	void print();

	isrfTypes Type;
	std::string stringType;
	double ConstantValue;
	std::string tableFilename;
	TBlackBody CMB;
	TBlackBody IR;
	TBlackBody Optical;
	TBlackBody UVI;
	TBlackBody UVII;
	TBlackBody UVIII;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TISRFPARAMS_H_ */
