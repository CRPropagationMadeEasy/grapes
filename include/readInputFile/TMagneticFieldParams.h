#ifndef INCLUDE_READINPUTFILE_TMAGNETICFIELDPARAMS_H_
#define INCLUDE_READINPUTFILE_TMAGNETICFIELDPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TMagneticFieldParams {
public:
	TMagneticFieldParams() : Type(MagFieldConstant), ConstantValue(1.), B0(3), r0(8.5), rc(4), zc(1) {}
	~TMagneticFieldParams() {};
	void setParams(xml_node &node);
	void print();
	magneticFieldTypes Type;
	std::string stringType;
	double ConstantValue;
	double B0;
	double r0;
	double rc;
	double zc;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TMAGNETICFIELDPARAMS_H_ */
