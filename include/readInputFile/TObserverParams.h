#ifndef INCLUDE_READINPUTFILE_TOBSERVERPARAMS_H_
#define INCLUDE_READINPUTFILE_TOBSERVERPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"

using namespace pugi;

namespace DRAGON {

class TObserverParams {
public:
	TObserverParams() : x(8.3 * kpc), y(0.0), z(0.0) {}
	~TObserverParams(){};
	void setParams(xml_node &node);
	void print();

	double x;
	double y;
	double z;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TOBSERVERPARAMS_H_ */
