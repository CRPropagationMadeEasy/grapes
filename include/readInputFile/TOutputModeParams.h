#ifndef INCLUDE_READINPUTFILE_TOUTPUTMODEPARAMS_H_
#define INCLUDE_READINPUTFILE_TOUTPUTMODEPARAMS_H_

#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"

using namespace pugi;

namespace DRAGON {

class TOutputModeParams {

public:
	TOutputModeParams() : doStoreSpectraAtObs(false), doStoreSpectraAllGalaxy(false),
	doOutputInASCII(false), doOutputInFITS(false), logLevelScreen(INFO), logLevelFile(DEBUG) {
		Logger::setLogLevelScreen(DEBUG);
		Logger::setLogLevelFile(DEBUG);
	}
	~TOutputModeParams(){};
	void setParams(xml_node &node);
	void print();

	bool doStoreSpectraAtObs;
	bool doStoreSpectraAllGalaxy;
	bool doOutputInASCII;
	bool doOutputInFITS;
	LogLevel logLevelScreen;
	LogLevel logLevelFile;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TOUTPUTMODEPARAMS_H_ */
