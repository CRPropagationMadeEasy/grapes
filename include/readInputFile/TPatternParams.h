#ifndef INCLUDE_READINPUTFILE_TPATTERNPARAMS_H_
#define INCLUDE_READINPUTFILE_TPATTERNPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TPatternParams {

	void setDefaultArms();
	void setArmsFromXML(xml_node &node);

public:

	TPatternParams(): Type(PatternConstant), constantValue(1.) {}
	~TPatternParams() {};
	void setParams(xml_node &node);
	void print();

	patternTypes Type;
	std::string stringType;

	double constantValue;
	unsigned int numArms;
	double armWidth; //kpc
	std::vector<double> arms_K;
	std::vector<double> arms_r0;
	std::vector<double> arms_theta0;

};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TISRFPARAMS_H_ */
