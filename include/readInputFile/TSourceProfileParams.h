#ifndef INCLUDE_READINPUTFILE_TSOURCEPROFILEPARAMS_H_
#define INCLUDE_READINPUTFILE_TSOURCEPROFILEPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

struct ProfileParams {
	double a;
	double b;
	double R1;
	double z0;
};

struct GaussianParams {
	double xC;
	double yC;
	double zC;
	double sigma2;
};

class TSourceProfileParams {

	void setProfileParamsFromXML(xml_node &node);
	void setGaussianProfileParamsFromXML(xml_node &node);
	void setProfileParamsForYusifov04();
	void setProfileParamsForLorimer06();
	void setProfileParamsForCase98();

public:

	TSourceProfileParams() : Type(sourceProfileConstant), ConstantValue(1.), sourceRate(-1.) {
		pYusifov04.a = -1; pYusifov04.b = -1; pYusifov04.R1 = -1; pYusifov04.z0 = -1;
		pGaussian.xC = -1; pGaussian.yC = -1; pGaussian.zC = -1; pGaussian.sigma2 = -1;
	}
	~TSourceProfileParams() {};
	void setParams(xml_node &node);
	void print();
	sourceProfileTypes Type;
	std::string stringType;
	double ConstantValue;
	double sourceRate;
	ProfileParams pYusifov04;
	GaussianParams pGaussian;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TSOURCEPROFILEPARAMS_H_ */
