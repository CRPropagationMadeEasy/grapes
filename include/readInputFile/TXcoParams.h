#ifndef INCLUDE_READINPUTFILE_TXCOPARAMS_H_
#define INCLUDE_READINPUTFILE_TXCOPARAMS_H_

#include "cgs.h"
#include "enums.h"
#include "logger.h"
#include "pugixml.hpp"
#include "pugiWrapper.h"
#include "utilities.h"

using namespace pugi;

namespace DRAGON {

class TXcoParams {
public:
	TXcoParams() : Type(XcoConstant), ConstantValue(1.), InnerValue(1.), OuterValue(1.), Border(2.) {}
	~TXcoParams() {};
	void setParams(xml_node &node);
	void print();
	gasXcoTypes Type;
	std::string stringType;
	double ConstantValue;
	double InnerValue;
	double OuterValue;
	double Border;
};

} /* namespace */

#endif /* INCLUDE_READINPUTFILE_TXCOPARAMS_H_ */
