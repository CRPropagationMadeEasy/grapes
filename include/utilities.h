#ifndef INCLUDE_UTILITIES_H_
#define INCLUDE_UTILITIES_H_

#include <cmath>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <vector>

#include "enums.h"
#include "exceptions.h"

namespace DRAGON {

bool fileExist(const std::string& filename);

bool dirExist(const std::string& dirname);

void idToAZ(const int& uid, int& A, int& Z);

bool idComparator(const int& i, const int& j);

decayTypes strToDecayMode(const std::string& str);

inline double deg2rad(const double& angleInDegree) { return (angleInDegree * M_PI / 180.); }

template<typename T>
inline std::string str(T const& x) {
	std::ostringstream o;
	o.imbue(std::locale("C"));
	o << x;
	if (!o) {
#ifdef DEBUG
		std::cerr << "bad_conversion: str(" << typeid(x).name() << ")";
#endif
		throw TDragonExceptions(std::string("str(") + typeid(x).name() + ")");
	}
	return (o.str());
}

template<class T>
T base_name(T const & path, T const & delims = "/\\")
{
	return (path.substr(path.find_last_of(delims) + 1));
}

template<class T>
T remove_extension(T const & filename)
{
	typename T::size_type const p(filename.find_last_of('.'));
	return (p > 0 && p != T::npos ? filename.substr(0, p) : filename);
}

} /* namespace */

#endif /* INCLUDE_UTILITIES_H_ */
