cmake_minimum_required(VERSION 2.6)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

add_library(NE2001++ STATIC
            src/NE2001.cpp
)

SET_TARGET_PROPERTIES(NE2001++ PROPERTIES COMPILE_FLAGS -DWITH_NE2001++)

