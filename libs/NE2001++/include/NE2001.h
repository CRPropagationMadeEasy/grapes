#ifndef LIBS_NE2001___INCLUDE_NE2001_H_
#define LIBS_NE2001___INCLUDE_NE2001_H_

namespace DRAGON {

class NE2001 {

//	parameters of large-scale components (inner+outer+arm components):
	double rsun;
	double n1h1;
	double h1;
	double A1;
	double F1;
	double n2;
	double h2;
	double A2;
	double F2;
	double na;
	double ha;
	double wa;
	double Aa;
	double Fa;
	double narm[6];
	double warm[6];
	double harm[6];
	double farm[6];

	void get_parameters();

public:

	NE2001();
	virtual ~NE2001();

	double ne_outer(double x_, double y_, double z_);
	double ne_inner(double x_, double y_, double z_);
	double ne_gc(double x_, double y_, double z_);
	double ne_arms_log_mod(double x_, double y_, double z_);

};

} /* namespace DRAGON */

#endif /* LIBS_NE2001___INCLUDE_NE2001_H_ */
