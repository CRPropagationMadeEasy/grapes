#include "NE2001.h"

#include <cmath>

inline double sech2(const double x_) { return ((fabs(x_) < 20.0) ? 2.0 / pow(exp(x_) + exp(-x_), 2.0) : 0.0); }

namespace DRAGON {

NE2001::NE2001() {
	get_parameters();
}

NE2001::~NE2001() {
	// TODO Auto-generated destructor stub
}

void NE2001::get_parameters() {

	rsun = 8.5;

	//	gal01.inp = input parameters for large-scale components of NE2001 30 June '02
	//	1 1 1 1 1 1 1 wg1 wg2 wga wggc wglism wgcN, wgvN
	n1h1 = 0.033;
	h1 = 0.97;
	A1 = 17.5;
	F1 = 0.18;
	n2 = 0.08;
	h2 = 0.15;
	A2 = 3.8;
	F2 = 120;
	na = 0.028;
	ha = 0.23;
	wa = 0.65;
	Aa = 10.5;
	Fa = 5;

	// factors for controlling individual spiral arms:
	//	narm: 	multiplies electron density (in addition to the`fac' quantities)
	narm[0] = 0.5;
	narm[1] = 1.2;
	narm[2] = 1.3;
	narm[3] = 1.0;
	narm[4] = 0.25;
	//	warm:	arm width factors that multiply nominal arm width
	warm[0] = 1.0;
	warm[1] = 1.5;
	warm[2] = 1.0;
	warm[3] = 0.8;
	warm[4] = 1.0;
	//	harm:	arm scale height factors
	harm[0] = 1.0;
	harm[1] = 0.8;
	harm[2] = 1.3;
	harm[3] = 1.5;
	harm[4] = 1.0;
	//  farm:	factors that multiply n_e^2 when calculating SM
	farm[0] = 1.1;
	farm[1] = 0.3;
	farm[2] = 0.4;
	farm[3] = 1.5;
	farm[4] = 0.3;

	return ;
}

double NE2001::ne_outer(double x_, double y_, double z_) {
	//	REAL FUNCTION NE_OUTER(x,y,z, F_outer)
	//  Thick disk component:

	const double pihalf = M_PI/2.0;
	const double rad = 57.2957795130823;

	const double rr = std::sqrt(x_ * x_ + y_ * y_);
	const double suncos = std::cos(pihalf * rsun / A1);
	const double g1 = (rr > A1) ? 0.0 : std::cos(pihalf * rr / A1) / suncos;
	const double ne1 = (n1h1 / h1) * g1 * sech2(z_ / h1);
	//F_outer = F1

	return (ne1);
}

double NE2001::ne_inner(double x_, double y_, double z_) {
	//  REAL FUNCTION NE_INNER(x,y,z, F_inner)
	//  Thin disk (inner Galaxy) component:
	//  (referred to as 'Galactic center component' in circa TC93 density.f)

	double g2 = 0.0;

	const double rr = std::sqrt(x_ * x_ + y_ * y_);
	const double rrarg = pow((rr - A2) / 1.8, 2.);
	if (rrarg < 10.0)
		g2 = std::exp(-rrarg);
	const double ne2 = n2 * g2 * sech2(z_ / h2);
	//F_inner = F2

	return (ne2);
}

double NE2001::ne_gc(double x_, double y_, double z_) {
	//  REAL FUNCTION NE_GC(x, y, z, F_gc)
	//  Determines the contribution of the Galactic center to the free
	//  electron density of the interstellar medium at Galactic location
	//  (x,y,z).  Combine with `fluctuation' parameter to obtain the
	//  scattering measure.
	//  NOTE: This is for the hyperstrong scattering region in the
	//  Galactic center.  It is distinct from the inner Galaxy
	//  (component 2) of the TC93 model.
	//
	//  Origin of coordinate system is at Galactic center; the Sun is at
	//  (x,y,z) = (0,R0,0), x is in l=90 direction
	//
	//  Based on Section 4.3 of Lazio & Cordes (1998, ApJ, 505, 715)
	//
	// Input:
	// REAL X - location in Galaxy [kpc]
	// REAL Y - location in Galaxy [kpc]
	// REAL Z - location in Galaxy [kpc]
	// REAL NEGC0 - nominal central density
	// PARAMETERS:
	// REAL RGC - radial scale length of Galactic center density enhancement
	// REAL HGC - z scale height of Galactic center density enhancement
	// Output:
	// REAL NE_GC - Galactic center free electron density contribution [cm^-3]

	double ne_gc = 0.;
	double F_gc = 0.;

	// open(11, file='ne_gc.inp', status='old')
	const double xgc = -0.01;
	const double ygc = 0.;
	const double zgc = -0.020;
	const double rgc = 0.145;
	const double hgc = 0.026;
	const double negc0 = 10.0;
	const double Fgc0 = 0.6e5;

	const double rr = std::sqrt(std::pow(x_ - xgc, 2.) + std::pow(y_ - ygc, 2.));
	if (rr > rgc)
		return (ne_gc);

	const double zz = std::abs(z_ - zgc);
	if (zz > hgc)
		return (ne_gc);

	const double arg = std::pow(rr / rgc, 2.) + std::pow(zz / hgc, 2.);
	if (arg <= 1.) {
		ne_gc = negc0;
		F_gc = Fgc0;
	}

	return (ne_gc);
}

double NE2001::ne_arms_log_mod(double x_, double y_, double z_) {
	// REAL FUNCTION NE_ARMS_LOG_MOD(x,y,z, whicharm, Farms)
	//  Spiral arms are defined as logarithmic spirals using the
	//  parameterization in Wainscoat et al. 1992, ApJS, 83, 111-146.
	//  But arms are modified selectively at various places to distort them
	//  as needed (08 Aug 2000).
	//  Note that arm numbering follows that of TC93 for the four large arms
	//  (after remapping).
	//  The local spiral arm is number 5.
	//  06 Apr 02:   removed TC type modifications of arms 2,3 (fac calculations)
	//  and replaced with new versions.  Data for these are hard wired.

	//implicit none
	//real x, y, z
	//integer whicharm
	//real Farms

	//integer whicharm_spiralmodel

	//   real n1h1,h1,A1,F1,n2,h2,A2,F2,na,ha,wa,Aa,Fa
	//   common/galparams/n1h1,h1,A1,F1,n2,h2,A2,F2,
	//.                na,ha,wa,Aa,Fa

	//c see get_parameters for definitions of narm, warm, harm.

	//  integer narmsmax
	//  parameter (narmsmax=5)
	//real narm, warm, harm, farm
	//common/armfactors/
	// .     harm(narmsmax),narm(narmsmax),warm(narmsmax),farm(narmsmax)


	//  real rsun
	//  common /mw/ rsun

	const double rad = 57.2957795130823;

	//integer ks
	//   data ks/3/

	//integer NN
	//data NN/7/

	const int NNmax = 20;
	const int narms = 5;

	//real aarm(narms), rmin(narms), thmin(narms), extent(narms)

	int armmap[5] = {1, 3, 4, 2, 5}; // order to TC93 order, which is from GC outwards toward Sun.
	int NNj[narms] = { 20, 20, 20, 20, 20};

	double th1[NNmax][narms];
	double r1[NNmax][narms];

	//real arm
	//integer kmax, narmpoints, ncoord
	//   parameter(narmpoints=500, ncoord=2)
	//dimension arm(narms,narmpoints,ncoord),kmax(narms)

	//real nea
	//   integer j, k, n, jj

	//logical first
	//data first /.true./

	/*real rr
	real dth, th, r
	real smin, sminmin
	real sq, sq1, sq2, sqmin
	integer kk, kmi, kma, kl
	real emm, ebb, exx, eyy,  test
	real ga, fac
	real thxy
	real arg
	real th3a, th3b, fac3min, test3
	real th2a, th2b, fac2min, test2
	c function:
	real sech2
	save*/

	const double rr = std::sqrt(x_ * x_ + y_ * y_);

	//if (first) then ! Reconstruct spiral arm axes

	// read arm parameters: open(11, file='ne_arms_log_mod.inp', status='old')
	double aarm[narms] = {4.25, 4.25, 4.89, 4.89, 4.57};
	double rmin[narms] = {3.48, 3.48, 4.90, 3.76, 8.10};
	double thmin[narms] = {0., 3.141, 2.525, 4.24, 5.847};
	double extent[narms] = {6., 6., 6., 6., 0.55};

	for (int j = 0; j < narms; ++j) {// fill sampling array
		for (int n = 0; n < NNj[j]; ++n) {
			th1[n][j] = thmin[j] + (n - 1) * extent[j] / (NNj[j] - 1.); // rad
			r1[n][j] = rmin[j] * std::exp((th1[n][j] - thmin[j]) / aarm[j]);
			th1[n][j] = th1[n][j] * rad; // deg
			// begin sculpting spiral arm 2 == TC arm 3
			if (armmap[j] == 3) {
				if (th1[n][j] > 370.0 && th1[n][j] <= 410.0) {
					r1[n][j] = r1[n][j] *
							(1.0 + 0.04 * std::cos((th1[n][j] - 390.0) * 180.0 / (40.0 * rad)));
				}
				if (th1[n][j] > 315.0 && th1[n][j] <= 370.0) {
					r1[n][j] = r1[n][j] *
							(1.0 - 0.07 * std::cos((th1[n][j] - 345.0) * 180.0 / (55.0 * rad)));
				}
				if (th1[n][j] > 180.0 && th1[n][j] <= 315.0) {
					r1[n][j] = r1[n][j] *
							(1.0 + 0.16 * std::cos((th1[n][j] - 260.0) * 180.0 / (135.0 * rad)));
				}
			}
			// begin sculpting spiral arm 4 == TC arm 2
			if (armmap[j] == 2) {
				if (th1[n][j] > 290.0 && th1[n][j] <= 395.0) {
					r1[n][j] = r1[n][j] *
							(1.0 - 0.11 * std::cos((th1[n][j] - 350.0) * 180.0 / (105.0 * rad)));
				}
			}
			// end arm sculpting
		}
	}

	/*open(11,file='log_arms.out', status='unknown')
    write(11,*) 'arm  n   xa     ya'

   do 21 j=1,narms
      dth=5.0/r1(1,j)
      th=th1(1,j)-0.999*dth
      call cspline(th1(1,j),r1(1,j),-NNj(j),th,r)
c	      write(6,*) 'doing arm ', j, ' with ', NNj(j), ' points',
c    .               dth
c	      write(6,*) (th1(k,j), r1(k,j), k=1,NNj(j))
      do 10 k=1,narmpoints-1
	 th=th+dth
	 if(th.gt.th1(NNj(j),j)) go to 20
	 call cspline(th1(1,j),r1(1,j),NNj(j),th,r)
	 arm(j,k,1)=-r*sin(th/rad)
	 arm(j,k,2)= r*cos(th/rad)
             write(11,"(1x,i2,1x,i3,1x,2(f7.3,1x))")
 .              j,k,arm(j,k,1),arm(j,k,2)
10	      continue
20	   continue
   kmax(j)=k
21        continue
   close(11)

first = .false.
endif*/

	/*
c Get spiral arm component:  30 do loop finds a coarse minimum distance
c from line of sight to arm; 40 do loop finds a fine minimum distance
c from line of sight to arm; line 35 ensures that arm limits are not
c exceeded; linear interpolation beginning at line 41 finds the
c minimum distance from line of sight to arm on a finer scale than gridding
c of arms allows (TJL)

nea=0.0
    ga = 0.
whicharm = 0
    whicharm_spiralmodel = 0
    sminmin = 1.e10
thxy = atan2(-x, y) * rad		! measured ccw from +y axis
					! (different from tc93 theta)
if(thxy.lt.0.) thxy=thxy+360.
if(abs(z/ha).lt.10.) then
       do 50 j=1,narms
          jj = armmap(j)
          sqmin=1.e10
          do 30 k=1+ks,kmax(j)-ks,2*ks+1
             sq=(x-arm(j,k,1))**2 + (y-arm(j,k,2))**2
             if(sq.lt.sqmin) then
                sqmin=sq
                kk=k
             endif
30           continue
35           kmi = max(kk-2*ks, 1)
          kma = min(kk+2*ks, kmax(j))
          do 40 k=kmi,kma
             sq=(x-arm(j,k,1))**2 + (y-arm(j,k,2))**2
             if(sq.lt.sqmin) then
                sqmin=sq
                kk=k
             endif
40           continue
41           if (kk.gt.1.and.kk.lt.kmax(j)) then
             sq1 = (x - arm(j,kk-1,1))**2 + (y - arm(j,kk-1,2))**2
             sq2 = (x - arm(j,kk+1,1))**2 + (y - arm(j,kk+1,2))**2
             if (sq1.lt.sq2) then
                kl = kk - 1
             else
                kl = kk + 1
             endif
             emm = (arm(j,kk,2) - arm(j,kl,2))
 $                /(arm(j,kk,1) - arm(j,kl,1))
             ebb = arm(j,kk,2) - emm*arm(j,kk,1)
             exx = (x + emm*y - emm*ebb)/(1.0 + emm**(2))
             test = (exx - arm(j,kk,1))/(arm(j,kl,1) - arm(j,kk,1))
             if (test.lt.0.0.or.test.gt.1.0) exx = arm(j,kk,1)
             eyy = emm*exx + ebb
          else
             exx = arm(j,kk,1)
             eyy = arm(j,kk,2)
          endif
          sqmin = (x - exx)**(2) + (y - eyy)**(2)
      smin=sqrt(sqmin)		! Distance of (x,y,z) from arm axis
c           write(23,"(4(f5.2,1x),i2,1x,3(f8.3,1x))")
c    .        x,y,z,rr,j,exx,eyy,smin
    if(smin.lt.3*wa) then		! If (x,y,z) is close to this
      ga=exp(-(smin/(warm(jj)*wa))**2)	! arm, get the arm weighting factor
      if(smin .lt. sminmin) then
	whicharm_spiralmodel = j
	sminmin = smin
      endif
      if(rr.gt.Aa) then 	! Galactocentric radial dependence of arms
	ga=ga*sech2((rr-Aa)/2.0)
c		write(6,*) 'd99a: rr,Aa,sech2() = ',
c                 rr, Aa, sech2((rr-Aa)/2.0)
      endif


c arm3 reweighting:
      th3a=320.
      th3b=390.
      th3b=370.
      th3a=290.
      th3b=363.
      th3b=363.
      fac3min=0.0
      test3 = thxy-th3a
      if(test3 .lt.0.) test3 = test3+360.
      if(jj.eq.3
 .            .and. 0. .le. test3
 .            .and. test3 .lt. (th3b-th3a))
 .        then
        arg=6.2831853*(thxy-th3a)/(th3b-th3a)
c		fac = (3.0 + cos(arg))/4.0
	fac = (1.+fac3min + (1.-fac3min)*cos(arg))/2.
	fac = fac**4.0
c		write(90,*) x, y, thxy, th3a, th3b, test3, fac
	ga=ga*fac
      endif


c arm2 reweighting:
c    first: as in tc93 (note different definition of theta)
      th2a=35.
      th2b=55.
      test2 = thxy-th2a
      fac = 1.

      if(jj.eq.2
 .            .and. 0. .le. test2
 .            .and. test2 .lt. (th2b-th2a))
 .        then
         fac=1.+ test2/(th2b-th2a)
	 fac = 1.		!**** note turned off
             ga=ga*fac
      endif

      if (jj.eq.2 .and. test2 .gt. (th2b-th2a)) then
        fac = 2.
	fac = 1.		!**** note turned off
            ga=ga*fac
      endif
c    second:  weaken the arm in a short range:
      th2a=340.
      th2b=370.
c note fix does nothing if fac2min = 1.0
      fac2min=0.1
      test2 = thxy-th2a
      if(test2 .lt.0.) test2 = test2+360.

      if(jj.eq.2
 .            .and. 0. .le. test2
 .            .and. test2 .lt. (th2b-th2a))
 .        then
        arg=6.2831853*(thxy-th2a)/(th2b-th2a)
	fac = (1.+fac2min + (1.-fac2min)*cos(arg))/2.
c		fac = fac**3.5
c		write(90,*) x, y, thxy, th2a, th2b, test2, fac
	ga=ga*fac
      endif

      nea=nea +
 .            narm(jj)*na*ga*sech2(z/(harm(jj)*ha))   ! Add this arm contribution
    endif
50	  continue
endif

    ne_arms_log_mod = nea
    Farms = 0
if(whicharm_spiralmodel .eq. 0) then
  whicharm = 0
else
  whicharm = armmap(whicharm_spiralmodel)	! remap arm number
  Farms = Fa * farm(whicharm)
endif
return
end
	 */
	return (0);
}

/*
c%%%%%%%%%%%%%%%%%%%%%%%%%  cspline.f  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c
	subroutine cspline(x,y,nn,xout,yout)
	integer nnmax
	parameter(nnmax=20)
	real x(nnmax),y(nnmax),y2(nnmax),u(nnmax)
	save

	if(nn .gt. nnmax) then
	  write(6,*)
     .    ' too many points to spline. Change parameter statement'
	  write(6,*)
     .    ' in cspline'
	endif

	n=abs(nn)
	if(nn.lt.0) then
	  y2(1)=0.
	  u(1)=0.
	  do 10 i=2,n-1
	    sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
	    p=sig*y2(i-1)+2.
	    y2(i)=(sig-1.)/p
	    u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     +        /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
10        continue
	  qn=0.
	  un=0.
	  y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
	  do 20 k=n-1,1,-1
20	  y2(k)=y2(k)*y2(k+1)+u(k)
	endif

	klo=1
	khi=n
30	if (khi-klo.gt.1) then
	  k=(khi+klo)/2
	  if(x(k).gt.xout)then
	    khi=k
	  else
	    klo=k
	  endif
	goto 30
	endif
	h=x(khi)-x(klo)
	if (h.eq.0.) pause 'bad x input.'
	a=(x(khi)-xout)/h
	b=(xout-x(klo))/h
	yout=a*y(klo)+b*y(khi)+
     +    ((a**3-a)*y2(klo)+(b**3-b)*y2(khi))*(h**2)/6.
	return
	end

 */

} /* namespace DRAGON */
