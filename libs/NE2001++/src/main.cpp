#include <cmath>
#include <iostream>

#include "include/NE2001.h"

int main()
{
	DRAGON::NE2001 ne;

	const double x = 8.5, y = 0.1, z = 0.1;

	std::cout<<ne.ne_outer(x,y,z)<<"\t"<<ne.ne_inner(x,y,z)<<"\t"<<ne.ne_gc(x,y,z)<<"\n";

	return (0);
}
