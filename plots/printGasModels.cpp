#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "Galaxy/gasDensity.h"

void printHIRadialProfiles(std::string outputFilename, double y, double z)
{
	DRAGON::THIDensityWolfire03 W03;
	DRAGON::THIDensityNakanishi03 N03;
	DRAGON::THIDensityMoskalenko02 M02;
	DRAGON::THIDensityMyModel MM;

	std::ofstream fout(outputFilename.c_str());
	fout<<"# x[kpc] Wolfire03[cm^-3] Nakanishi03[cm^-3] Moskalenko02[cm^-3] MyModel[cm^-3]"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (double x = 0.0; x < 30.0; x += 1.0) {
		fout<<x<<"\t";
		fout<<W03.densityFunction(x,y,z)<<"\t";
		fout<<N03.densityFunction(x,y,z)<<"\t";
		fout<<M02.densityFunction(x,y,z)<<"\t";
		fout<<MM.densityFunction(x,y,z)<<"\n";
	}
	fout.close();
	return ;
}

void printHIHeightProfiles(std::string outputFilename, double x, double y)
{
	DRAGON::THIDensityWolfire03 W03;
	DRAGON::THIDensityNakanishi03 N03;
	DRAGON::THIDensityMoskalenko02 M02;
	DRAGON::THIDensityMyModel MM;

	std::ofstream fout(outputFilename.c_str());
	fout<<"# z[kpc] Wolfire03[cm^-3] Nakanishi03[cm^-3] Moskalenko02[cm^-3] MyModel[cm^-3]"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (double z = -3.0; z <= 3.0; z += 0.1) {
		fout<<z<<"\t";
		fout<<W03.densityFunction(x,y,z)<<"\t";
		fout<<N03.densityFunction(x,y,z)<<"\t";
		fout<<M02.densityFunction(x,y,z)<<"\t";
		fout<<MM.densityFunction(x,y,z)<<"\n";
	}
	fout.close();
	return ;
}

void printH2RadialProfiles(std::string outputFilename, double y, double z)
{
	DRAGON::TH2DensityBronfman88 B88;
	DRAGON::TH2DensityNakanishi06 N06;
	DRAGON::TH2DensityEvoli07 E07;
	DRAGON::TH2DensityMyModel MM;

	std::ofstream fout(outputFilename.c_str());
	fout<<"# x[kpc] Bronfman88[cm^-3] Nakanishi06[cm^-3] Evoli07[cm^-3] MyModel[cm^-3]"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (double x = 0.0; x < 30.0; x += 1.0) {
		fout<<x<<"\t";
		fout<<B88.densityFunction(x,y,z)<<"\t";
		fout<<N06.densityFunction(x,y,z)<<"\t";
		fout<<E07.densityFunction(x,y,z)<<"\t";
		fout<<MM.densityFunction(x,y,z)<<"\n";
	}
	fout.close();
	return ;
}

void printH2HeightProfiles(std::string outputFilename, double x, double y)
{
	DRAGON::TH2DensityBronfman88 B88;
	DRAGON::TH2DensityNakanishi06 N06;
	DRAGON::TH2DensityEvoli07 E07;
	DRAGON::TH2DensityMyModel MM;

	std::ofstream fout(outputFilename.c_str());
	fout<<"# z[kpc] Bronfman88[cm^-3] Nakanishi06[cm^-3] Evoli07[cm^-3] MyModel[cm^-3]"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (double z = -3.0; z <= 3.0; z += 0.1) {
		fout<<z<<"\t";
		fout<<B88.densityFunction(x,y,z)<<"\t";
		fout<<N06.densityFunction(x,y,z)<<"\t";
		fout<<E07.densityFunction(x,y,z)<<"\t";
		fout<<MM.densityFunction(x,y,z)<<"\n";
	}
	fout.close();
	return ;
}

void printHIIRadialProfiles(std::string outputFilename, double y, double z)
{
	DRAGON::THIIDensityCordes91 C91;
	DRAGON::THIIDensityNE2001 N01;
	DRAGON::THIIDensityMyModel MM;

	std::ofstream fout(outputFilename.c_str());
	fout<<"# x[kpc] Cordes91[cm^-3] NE2001[cm^-3] MyModel[cm^-3]"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (double x = 0.0; x < 30.0; x += 1.0) {
		fout<<x<<"\t";
		fout<<C91.densityFunction(x,y,z)<<"\t";
		fout<<N01.densityFunction(x,y,z)<<"\t";
		fout<<MM.densityFunction(x,y,z)<<"\n";
	}
	fout.close();
	return ;
}

void printHIIHeightProfiles(std::string outputFilename, double x, double y)
{
	DRAGON::THIIDensityCordes91 C91;
	DRAGON::THIIDensityNE2001 N01;
	DRAGON::THIIDensityMyModel MM;

	std::ofstream fout(outputFilename.c_str());
	fout<<"# z[kpc] Cordes91[cm^-3] NE2001[cm^-3] MyModel[cm^-3]"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (double z = -3.0; z <= 3.0; z += 0.1) {
		fout<<z<<"\t";
		fout<<C91.densityFunction(x,y,z)<<"\t";
		fout<<N01.densityFunction(x,y,z)<<"\t";
		fout<<MM.densityFunction(x,y,z)<<"\n";
	}
	fout.close();
	return ;
}


int main()
{
	printHIRadialProfiles("output/HI_radialProfiles.txt", 0, 0);
	printH2RadialProfiles("output/H2_radialProfiles.txt", 0, 0);
	printHIIRadialProfiles("output/HII_radialProfiles.txt", 0, 0);
	printHIHeightProfiles("output/HI_heightProfiles.txt", 8.5, 0);
	printH2HeightProfiles("output/H2_heightProfiles.txt", 8.5, 0);
	printHIIHeightProfiles("output/HII_heightProfiles.txt", 8.5, 0);

	return (0);
}
