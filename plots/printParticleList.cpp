#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Particle {
  int A;
  int Z;
  double tauHalfInSec;
  double tauHalfNakedInSec;
  double relativeAbundance;
  double injectionSlope;
  double cutoffEnergy;
  string decayMode;
  string name;

public:
  Particle(int A_, int Z_, double tauHalfInSec_, double tauHalfNakedInSec_, double relativeAbundance_, double injectionSlope_, double cutoffEnergy_, string decayMode_, string name_)
  {
    A = A_;
    Z = Z_;
    tauHalfInSec = tauHalfInSec_;
    tauHalfNakedInSec = tauHalfNakedInSec_;
    relativeAbundance = relativeAbundance_;
    injectionSlope = injectionSlope_;
    cutoffEnergy = cutoffEnergy_;
    decayMode = decayMode_;
    name = name_;
  }
  ~Particle() {};
  
  void printIt()
  {
    cout<<A<<"\t"<<Z<<"\t"<<tauHalfInSec<<"\t"<<tauHalfNakedInSec<<"\n";
    return ;
  }
};

class ParticleList {
  vector<Particle> list;

public:
  ParticleList() {}
  ~ParticleList() { list.clear(); };
  
  void pushBack(int A_, int Z_, double tauHalfInSec_, double tauHalfNakedInSec_, double relativeAbundance_, double injectionSlope_, double cutoffEnergy_, string decayMode_, string name_)
  {
    Particle p(A_,Z_,tauHalfInSec_,tauHalfNakedInSec_,relativeAbundance_,injectionSlope_,cutoffEnergy_,decayMode_,name_);
    list.push_back(p);
  }
  
  void printIt()
  {
    for (vector<Particle>::iterator it = list.begin(); it != list.end(); it++)
    it->printIt();
  }
};

int main()
{
  ParticleList l;
  
  l.pushBack(1 ,  1,  -1.0000E+00,  -1.0000E+00,    "STABLE",   "H");
  l.pushBack(2 ,  1,  -1.0000E+00,  -1.0000E+00,    "STABLE",	"H
  l.pushBack(3 ,  1,   3.8880E+08,   3.8880E+08,	"B-",	    "H
  l.pushBack(3 ,  2,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"He
  l.pushBack(4 ,  2,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"He
  l.pushBack(6 ,  3,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Li
  l.pushBack(7 ,  3,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Li
  l.pushBack(7 ,  4,   4.5982E+06,   4.5982E+06,	"EC",	    "Be
  l.pushBack(9 ,  4,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Be
  l.pushBack(10,  4,   4.7650E+13,   4.7650E+13,	"B-",       "Be
  l.pushBack(10,  5,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"B
  l.pushBack(11,  5,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"B
  l.pushBack(12,  6,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"C
  l.pushBack(13,  6,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"C
  l.pushBack(14,  6,   1.7990E+11,   1.7990E+11,	"B-",	    "C
  l.pushBack(14,  7,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"N
  l.pushBack(15,  7,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"N
  l.pushBack(16,  8,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"O
  l.pushBack(17,  8,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"O
  l.pushBack(18,  8,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"O
  l.pushBack(19,  9,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"F
  l.pushBack(20, 10,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ne
  l.pushBack(21, 10,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ne
  l.pushBack(22, 10,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ne
  l.pushBack(22, 11,   8.2130E+07,   1.5162E+11,	"EC",	    "Na
  l.pushBack(23, 11,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Na
  l.pushBack(24, 12,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Mg
  l.pushBack(25, 12,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Mg
  l.pushBack(26, 12,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Mg
  l.pushBack(26, 13,   1.2872E+14,   2.8746E+13,	"ECB+",	    "Al
  l.pushBack(27, 13,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Al
  l.pushBack(28, 14,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Si
  l.pushBack(29, 14,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Si
  l.pushBack(30, 14,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Si
  l.pushBack(32, 14,   4.8280E+09,   4.8280E+09,	"B-",	    "Si
  l.pushBack(31, 15,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"P
  l.pushBack(32, 15,   1.2322E+06,   1.2322E+06,	"B-",	    "P
  l.pushBack(33, 15,   2.1894E+06,   2.1894E+06,	"B-",	    "P
  l.pushBack(32, 16,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"S
  l.pushBack(33, 16,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"S
  l.pushBack(34, 16,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"S
  l.pushBack(35, 16,   7.5609E+06,   7.5609E+06,	"B-",	    "S
  l.pushBack(36, 16,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"S
  l.pushBack(35, 17,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cl
  l.pushBack(36, 17,   9.4990E+12,   9.4990E+12,	"B-",	    "Cl
  l.pushBack(37, 17,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cl
  l.pushBack(36, 18,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ar
  l.pushBack(37, 18,   3.0197E+06,   3.0197E+06,	"EC",	    "Ar
  l.pushBack(38, 18,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ar
  l.pushBack(39, 18,   8.4890E+09,   8.4890E+09,	"B-",	    "Ar
  l.pushBack(40, 18,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ar
  l.pushBack(42, 18,   1.0382E+09,   1.0382E+09,	"B-",	    "Ar
  l.pushBack(39, 19,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"K
  l.pushBack(40, 19,   3.9380E+16,   3.9380E+16,	"B-",	    "K
  l.pushBack(41, 19,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"K
  l.pushBack(41, 20,   3.2190E+12,   3.2190E+12,	"EC",	    "Ca
  l.pushBack(42, 20,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ca
  l.pushBack(43, 20,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ca
  l.pushBack(44, 20,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ca
  l.pushBack(45, 20,   1.4050E+07,   1.4050E+07,	"B-",	    "Ca
  l.pushBack(47, 20,   3.9191E+05,   3.9191E+05,	"B-",	    "Ca
  l.pushBack(48, 20,   5.9960E+26,   5.9960E+26,	"BB",	    "Ca
  l.pushBack(44, 21,   2.1100E+05,   2.1100E+05,	"B-",	    "Sc
  l.pushBack(45, 21,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Sc
  l.pushBack(46, 21,   7.2395E+06,   7.2395E+06,	"B-",	    "Sc
  l.pushBack(47, 21,   2.8937E+05,   2.8937E+05,	"B-",	    "Sc
  l.pushBack(48, 21,   1.5721E+05,   1.5721E+05,	"B-",	    "Sc
  l.pushBack(44, 22,   1.8930E+09,   1.8930E+09,	"BC",	    "Ti
  l.pushBack(46, 22,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ti
  l.pushBack(47, 22,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ti
  l.pushBack(48, 22,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ti
  l.pushBack(49, 22,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ti
  l.pushBack(50, 22,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ti
  l.pushBack(48, 23,   1.3801E+06,   1.3801E+06,	"EC",	    "V
  l.pushBack(49, 23,   2.8510E+07,   2.8510E+07,	"EC",	    "V
  l.pushBack(50, 23,   4.4180E+24,   4.4180E+24,	"EC",	    "V
  l.pushBack(51, 23,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"V
  l.pushBack(51, 24,   2.3935E+06,   2.3935E+06,	"EC",	    "Cr
  l.pushBack(52, 24,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cr
  l.pushBack(53, 24,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cr
  l.pushBack(54, 24,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cr
  l.pushBack(52, 25,   4.8306E+05,   4.8306E+05,	"EC",	    "Mn
  l.pushBack(53, 25,   1.1800E+14,   1.1800E+14,	"EC",	    "Mn
  l.pushBack(54, 25,   2.6982E+07,   1.9881E+13,	"ECB-",	    "Mn
  l.pushBack(55, 25,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Mn
  l.pushBack(54, 26,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Fe
  l.pushBack(55, 26,   8.6152E+07,   8.6152E+07,	"EC",	    "Fe
  l.pushBack(56, 26,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Fe
  l.pushBack(57, 26,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Fe
  l.pushBack(58, 26,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Fe
  l.pushBack(59, 26,   3.8444E+06,   3.8444E+06,	"B-",	    "Fe
  l.pushBack(60, 26,   4.7340E+13,   4.7340E+13,	"B-",	    "Fe
  l.pushBack(56, 27,   6.6729E+06,   6.6729E+06,	"EC",	    "Co
  l.pushBack(57, 27,   2.3480E+07,   2.3480E+07,	"EC",	    "Co
  l.pushBack(58, 27,   6.1223E+06,   6.1223E+06,	"EC",	    "Co
  l.pushBack(59, 27,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Co
  l.pushBack(60, 27,   1.6630E+08,   1.6630E+08,	"B-",	    "Co
  l.pushBack(56, 28,   3.1558E+06,   1.2623E+12,	"ECB+",	    "Ni
  l.pushBack(57, 28,   1.2816E+05,   1.2816E+05,	"EC",	    "Ni
  l.pushBack(58, 28,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ni
  l.pushBack(59, 28,   2.3980E+12,   2.3980E+12,	"EC",	    "Ni
  l.pushBack(60, 28,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ni
  l.pushBack(61, 28,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ni
  l.pushBack(62, 28,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ni
  l.pushBack(63, 28,   3.1590E+09,   3.1590E+09,	"B-",	    "Ni
  l.pushBack(64, 28,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Ni
  l.pushBack(66, 28,   1.9656E+05,   1.9656E+05,	"B-",	    "Ni
  l.pushBack(63, 29,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cu
  l.pushBack(65, 29,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Cu
  l.pushBack(67, 29,   2.2259E+05,   2.2259E+05,	"B-",	    "Cu
  l.pushBack(64, 30,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Zn
  l.pushBack(65, 30,   2.1050E+07,   2.1050E+07,	"EC",	    "Zn
  l.pushBack(66, 30,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Zn
  l.pushBack(67, 30,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Zn
  l.pushBack(68, 30,  -1.0000E+00,  -1.0000E+00,	"STABLE",	"Zn
  
  
  l.printIt();

  return 0;
}