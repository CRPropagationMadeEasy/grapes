#include "Galaxy/ISRF.h"

#include "fitsio.h"

namespace DRAGON {

void TRadiationFieldGalpropTable::buildFrequencyAxis()
{
	frequency.assign(frequencySize, 0.0);

	//for (size_t i = 0; i < frequencySize; ++i) // microns -> cm; nu = c/lambda where nu is in Hz
		//frequency[frequencySize-1-i] = cLight/(pow(10.,CRVAL3+(double)i*CDELT3)*1.0e-4);

	Logger::cout(DEBUG)<<"... ISRF frequency "<<frequency<<"\n";

	return ;
}

void TRadiationFieldGalpropTable::loadTable()
{
	Logger::cout(DEBUG)<<"... loading ISRF Table\n";

	TReadCfitsioFile cfitsioFile(tableFilename);

	nAxes = cfitsioFile.readIntKeyword("NAXIS");
	rSize = cfitsioFile.readIntKeyword("NAXIS1");
	zSize = cfitsioFile.readIntKeyword("NAXIS2");
	frequencySize = cfitsioFile.readIntKeyword("NAXIS3");
	nComponents = cfitsioFile.readIntKeyword("NAXIS4");

	CRVAL1 = cfitsioFile.readDoubleKeyword("CRVAL1");
	CRVAL2 = cfitsioFile.readDoubleKeyword("CRVAL2");
	CRVAL3 = cfitsioFile.readDoubleKeyword("CRVAL3");
	CDELT1 = cfitsioFile.readDoubleKeyword("CDELT1");
	CDELT2 = cfitsioFile.readDoubleKeyword("CDELT2");
	CDELT3 = cfitsioFile.readDoubleKeyword("CDELT3");

	long nelements = rSize * zSize * frequencySize * nComponents;

	isrfTable = cfitsioFile.readDoubleImage(nelements);

	Logger::cout(DEBUG)<<"... ISRF loaded in a "<<isrfTable.size()<<" elements array\n";

	return ;
}

void TRadiationFieldGalpropModel::buildGrid()
{
	for (size_t iComponent = 0; iComponent < GalpropTable->getNComponents(); ++iComponent)
	{
		for (size_t ix = 0; ix < xSize; ++ix)
			for (size_t iy = 0; iy < ySize; ++iy)
			{
				double r = sqrt(xGalaxy[ix]*xGalaxy[ix] + yGalaxy[iy]*yGalaxy[iy]);
				for (size_t iz = 0; iz < zSize; ++iz)
				{
					double z = zGalaxy[iz];
					size_t irr = (size_t)((r - GalpropTable->getCRVAL1()) / GalpropTable->getCDELT1() + 0.5);
					size_t izz = (size_t)((fabs(z) - GalpropTable->getCRVAL2()) / GalpropTable->getCDELT2() + 0.5);
					if (irr > GalpropTable->getRSize() - 2)
						irr = GalpropTable->getRSize() - 2;
					if (izz > GalpropTable->getZSize() - 2)
						izz = GalpropTable->getRSize() - 2;
					double rr = GalpropTable->getCRVAL1() + irr * GalpropTable->getCDELT1();
					double zz = GalpropTable->getCRVAL2() + izz * GalpropTable->getCDELT2();

					for (size_t iFrequency = 0; iFrequency < GalpropTable->getFrequencySize(); ++iFrequency)
					{
						double v1 = GalpropTable->getIsrfTableValue(irr, izz, iFrequency, iComponent); //isrf_in[isrf_index(irr  ,izz  ,inu,i)];
						double v2 = GalpropTable->getIsrfTableValue(irr + 1, izz, iFrequency, iComponent);
						double v3 = GalpropTable->getIsrfTableValue(irr, izz + 1, iFrequency, iComponent);
						double v4 = GalpropTable->getIsrfTableValue(irr + 1, izz + 1, iFrequency, iComponent);
						double v5 = v1 + (v2 - v1) * (r - rr) / GalpropTable->getCDELT1();
						double v6 = v3 + (v4 - v3) * (r - rr) / GalpropTable->getCDELT1();
						double value = v5 + (v6 - v5) * (fabs(z) - zz) / GalpropTable->getCDELT2();

						// reverse scale from wavelength to frequency //ISRField[index(dimnu-1-inu, ix, iy, iz)] += value;
						field->get(ix, iy, iz, GalpropTable->getFrequencySize() - 1 - iFrequency) += (value > 0.) ? value : 0.0;

					}  //  inu
				}  //  iz
			}  //  iy
	} // icomponents

	return ;
}

void TRadiationFieldIsrfDelahaye10::buildGrid()
{
	int nComponentsIsrf = 6;
	std::vector<double> t0isrf;
	std::vector<double> factor;

	t0isrf.assign(nComponentsIsrf,0.);
	factor.assign(nComponentsIsrf,0.);

	factor[0] = 1.;
	t0isrf[0] = 2.726;

	factor[1] = 4.5e-5;
	t0isrf[1] = 33.07;

	factor[2] = 1.2e-9;
	t0isrf[2] = 313.32;

	factor[3] = 7.03e-13;
	t0isrf[3] = 3249.3;

	factor[4] = 3.39e-14;
	t0isrf[4] = 6150.4;

	factor[5] = 8.67e-17;
	t0isrf[5] = 23209.0;

	//		double hPlanck = 4.135638e-15;  // eV/Hz
	//		double kB      = 8.6173324e-5;  // eV/K
	//		// C is in cm/s from constants.h
	//
	//		vector<double> ISRF_vector;
	//		ISRF_vector.assign(dimnu,0.);
	//
	//		cout << "*** ISRF vector from Delahaye et al. 2010 *** " << endl;
	//		for(int inu = 0; inu < dimnu; inu++) {
	//			// ISRF is in eV/(cm3 Hz)
	//			for (int i_isrf = 0; i_isrf < n_isrf; i_isrf++) {
	//				double u_nu = factor[i_isrf] * (8.* M_PI * hPlanck)/(C*C*C) * pow(nu_array[inu],3.) * ( 1./ ( exp(hPlanck*nu_array[inu]/(kB*t0isrf[i_isrf])) - 1. ) );
	//				ISRF_vector[inu] += nu_array[inu] * u_nu;
	//			}
	//			cout << ISRF_vector[inu] << " ";
	//		}
	//		cout << endl;
	//
	//
	//		for(int ix = 0; ix < dimx; ix++) {
	//			for(int iy = 0; iy < dimy; iy++) {
	//				for(int iz = 0; iz < dimz; iz++) {
	//					for(int inu = 0; inu < dimnu; inu++) {
	//						ISRField[index(inu, ix, iy, iz)] = ISRF_vector[inu];
	//					}
	//				}
	//			}
	//		}
	//
	//	}
	//
	//	// microns -> cm; nu=c/lambda
	//	//for(int inu = 0; inu < dimnu; inu++) nu_array[dimnu-1-inu]=C/(pow(10.,1.*CRVAL3+inu*CDELT3)*1.0e-4); --> already did before!
	//
	//	delete[] isrf_in;
	return ;
}

} // namespace
