#include "Galaxy/TGalaxy.h"

namespace DRAGON {

TGalaxy::TGalaxy(const TGalaxyParams& GalaxyNode, const ref_ptr<TPositionGrid>& position) {
	this->GalaxyNode = GalaxyNode;
	this->position = position;

	buildGasH2(GalaxyNode.pH2);
	buildGasHI(GalaxyNode.pHI);
	buildGasHII(GalaxyNode.pHII);
	buildGasXco(GalaxyNode.pXco);
	buildSourceProfile(GalaxyNode.pSourceProfile);

	//buildDarkProfile(GalaxyNode.pDarkProfile);
	//buildMagneticField(GalaxyNode.pMagneticField);
	//buildISRF(GalaxyNode.pIsrf);
	//buildPattern(GalaxyNode.pPattern);

	//Logger::cout(INFO)<<"Applying Pattern...\n";
	// Apply pattern to Source profile -- just an example
	//ref_ptr<Grid3D> SourceProfileGrid = SourceProfile->getSourceProfileGrid();
	//applyPattern(SourceProfileGrid, pattern);
	//Logger::cout(INFO)<<"... Pattern applied\n";
	//SourceProfile->resetSourceProfileGrid(SourceProfileGrid); //this has to be checked

	/*if (feedback>1) cout<<"Preparing the geometry."<<endl;
	if (inputStructure->SA_type == "None") _fGeometry = new TUniformGeometry(_fCoordinates, in_);
	else _fGeometry = new TSpiralGeometry(_fCoordinates, in_);
	if (feedback>0) cout<<"... geometry done!"<<endl;*/
}

TGalaxy::~TGalaxy() {}

void TGalaxy::buildGasXco(const TXcoParams& pXco)
{
	switch (pXco.Type) {
	case XcoConstant :
		Xco = new TXcoConstant(pXco.ConstantValue);
		break;
	case XcoMyModel :
		Xco = new TXcoMyModel();
		break;
	case XcoArimoto96 :
		Xco = new TXcoArimoto96();
		break;
	case XcoStrong04 :
		Xco = new TXcoStrong04();
		break;
	case XcoAckermann10 :
		Xco = new TXcoAckermann10();
		break;
	case XcoEvoli12 :
		Xco = new TXcoEvoli12(pXco.InnerValue,pXco.OuterValue,pXco.Border);
		break;
	default :
		throw TDragonExceptions("Unknown Xco model in TGalaxy");
	}
	Xco->buildGrid(position);
	Xco->calculateGrid(-1);
	Logger::cout(DEBUG)<<"... mean XCO : "<<Xco->getMean()<<"\n";
}

void TGalaxy::buildGasH2(const TGasH2Params& pH2)
{
	switch (pH2.type) {
	case H2Constant :
		H2Density = new TH2DensityConstant(pH2.constantValue);
		break;
	case H2MyModel :
		H2Density = new TH2DensityMyModel();
		break;
	case H2Bronfman88 :
		H2Density = new TH2DensityBronfman88();
		break;
	case H2Nakanishi06 :
		H2Density = new TH2DensityNakanishi06();
		break;
	case H2Ferriere07 :
		H2Density = new TH2DensityFerriere07();
		break;
	case H2Pohl08 :
		H2Density = new TH2DensityPohl08("data/MolecularGas3D_Pohl07.fits.gz");
		break;

	default:
		throw TDragonExceptions("Unknown H2 model in TGalaxy");
	}
	H2Density->buildGrid(position);
	H2Density->calculateGrid(pH2.smoothGasRadius);
	Logger::cout(DEBUG)<<"... mean H2 gas : "<<H2Density->getMean()<<" cm-3\n";
}

void TGalaxy::buildGasHI(const TGasHIParams& pHI)
{
	switch (pHI.type) {
	case HIConstant :
		HIDensity = new THIDensityConstant(pHI.constantValue);
		break;
	case HIMyModel :
		HIDensity = new THIDensityMyModel();
		break;
	case HIFerriere07 :
		HIDensity = new THIDensityFerriere07();
		break;
	case HIMoskalenko02 :
		HIDensity = new THIDensityMoskalenko02();
		break;
	case HINakanishi03 :
		HIDensity = new THIDensityNakanishi03();
		break;
	default:
		throw TDragonExceptions("Unknown HI model in TGalaxy");
	}
	HIDensity->buildGrid(position);
	HIDensity->calculateGrid(pHI.smoothGasRadius);
	Logger::cout(DEBUG)<<"... mean HI gas : "<<HIDensity->getMean()<<" cm-3\n";
}

void TGalaxy::buildGasHII(const TGasHIIParams& pHII)
{
	switch (pHII.type) {
	case HIIConstant :
		HIIDensity = new THIIDensityConstant(pHII.constantValue);
		break;
	case HIIMyModel :
		HIIDensity = new THIIDensityMyModel();
		break;
	case HIICordes91 :
		HIIDensity = new THIIDensityCordes91();
		break;
	case HIINE2001 :
		HIIDensity = new THIIDensityNE2001();
		break;
	case HIIFerriere07 :
		HIIDensity = new THIIDensityFerriere07();
		break;
	default:
		throw TDragonExceptions("Unknown HII model in TGalaxy");
	}
	HIIDensity->buildGrid(position);
	HIIDensity->calculateGrid(pHII.smoothGasRadius);
	Logger::cout(DEBUG)<<"... mean HII gas : "<<HIIDensity->getMean()<<" cm-3\n";
}

void TGalaxy::buildSourceProfile(const TSourceProfileParams& pSource)
{
	switch (pSource.Type) {
	case sourceProfileConstant :
		SourceProfile = new TSourceProfileConstant(pSource.ConstantValue);
		break;
	case sourceProfileMyModel :
		SourceProfile = new TSourceProfileMyModel();
		break;
	case sourceProfileGaussianModel :
		SourceProfile = new TSourceProfileGaussianModel(pSource.pGaussian);
		break;
	case sourceProfileLorimer06 :
	case sourceProfileYusifov04 :
	case sourceProfileCase98 :
		SourceProfile = new TSourceProfileYusifov04(pSource.pYusifov04);
		break;
	case sourceProfileFerriere01 :
		SourceProfile = new TSourceProfileFerriere01();
		break;
	default :
		throw TDragonExceptions("Unknown SourceModel in TGalaxy");
	}
	SourceProfile->buildGrid(position);
	SourceProfile->calculateGrid();
	SourceProfile->scaleGrid(pSource.sourceRate);
	Logger::cout(DEBUG)<<"... mean source profile : "<<SourceProfile->getMean()<<" units\n";
}

void TGalaxy::buildDarkProfile(const TDarkProfileParams& pDarkProfile)
{
	switch (pDarkProfile.Type) {
	case darkProfileConstant :
		DarkProfile = new TDarkProfileConstant(pDarkProfile.ConstantValue);
		break;
	case darkProfileMyModel :
		DarkProfile = new TDarkProfileMyModel();
		break;
	case darkProfileGNFW :
		DarkProfile = new TDarkProfileGNFW(pDarkProfile.alpha,pDarkProfile.beta,pDarkProfile.gamma);
		break;
	case darkProfileCored :
		DarkProfile = new TDarkProfileCored(pDarkProfile.beta,pDarkProfile.gamma);
		break;
	case darkProfileEinasto65 :
		DarkProfile = new TDarkProfileEinasto65(pDarkProfile.alpha);
		break;
	case darkProfileStadel09 :
		DarkProfile = new TDarkProfileStadel09(pDarkProfile.alpha);
		break;
	default :
		throw TDragonExceptions("Unknown DarkMatterProfile in TGalaxy");
	}
	DarkProfile->buildGrid(position);
}

void TGalaxy::buildMagneticField(const TMagneticFieldParams& pMagneticField)
{
	/*switch (pMagneticField.Type) {
	case MagFieldExponential :
		magneticField = new TExponentialMagneticField(pMagneticField.r0,pMagneticField.r0,pMagneticField.rc,pMagneticField.zc);
		break;
	case MagFieldPshirkov11ASS :
		magneticField = new TPshirkov11ASSMagneticField();
		break;
	default :
		throw TDragonExceptions("Unknown MagneticField in TGalaxy");
	}*/
	//magneticField->buildGrid(spatialAxes);
	//magneticField->calculateRandomField();
	//magneticField->calculateRegularField();
}

void TGalaxy::buildPattern(const TPatternParams& pPattern)
{
	//pattern = new spiralPattern();
	//pattern->buildPattern(spatialAxes, pPattern);
	//pattern->calculatePattern(pPattern);
}

//void TGalaxy::applyPattern(ref_ptr<Grid3D>& genericGrid, ref_ptr<TPattern> pattern)
//{
//	double volumeIntegral = 1;//genericGrid->computeVolumeIntegral();

//	ref_ptr<Grid3D> patternGrid = pattern->getPatternFunction();

//	for (size_t i = 0; i < genericGrid->getNx(); i++)
//		for (size_t j = 0; j < genericGrid->getNy(); j++)
//			for (size_t k = 0; k < genericGrid->getNz(); k++)
//				genericGrid->get(i,j,k) = genericGrid->get(i,j,k) * patternGrid->get(i,j,k);

//	double newVolumeIntegral = 1;//genericGrid->computeVolumeIntegral();
//	double scalingFactor = volumeIntegral / newVolumeIntegral;

//	for (size_t i = 0; i < genericGrid->getNx(); i++)
//		for (size_t j = 0; j < genericGrid->getNy(); j++)
//			for (size_t k = 0; k < genericGrid->getNz(); k++)
//				genericGrid->get(i,j,k) *= scalingFactor;
//}

void TGalaxy::buildISRF(const TIsrfParams& pIsrf)
{
	//	if (inputStructure->prop_lep || inputStructure->prop_extracomp || inputStructure->prop_DMel) {
	switch (pIsrf.Type) {
	case IsrfGalpropTable :
		//isrf = new TRadiationFieldGalpropModel(xGalaxy->axis,yGalaxy->axis,zGalaxy->axis,pIsrf.tableFilename);
		break;
	case IsrfDelahaye10 :
		//isrf = new TRadiationFieldIsrfDelahaye10(xGalaxy->axis,yGalaxy->axis,zGalaxy->axis);
		break;
	default:
		throw TDragonExceptions("Unknown ISRF model in TGalaxy");
	}
	return ;
}

void TGalaxy::dumpToFile(const std::string& initFilename)
{
	Xco->dumpToFile("output/"+initFilename+"_Xco.txt");
	HIDensity->dumpToFile("output/"+initFilename+"_HI.txt");
	HIIDensity->dumpToFile("output/"+initFilename+"_HII.txt");
	H2Density->dumpToFile("output/"+initFilename+"_H2.txt");
	SourceProfile->dumpToFile("output/"+initFilename+"_sourceProfile.txt");
	//DarkProfile->dumpToFile("output/dark.txt");
	//magneticField->dumpToFile("output/"+initFilename+"_magneticField.txt");
}

} /* namespace */
