#include "Galaxy/TGalaxyGrid.h"

namespace DRAGON {

TGalaxyGrid::TGalaxyGrid() : grid(NULL) {
	xSize = ySize = zSize = 0;
}

void TGalaxyGrid::setPosition(ref_ptr<TPositionGrid> position) {
	this->position = position;
	xSize = position->getNx();
	ySize = position->getNy();
	zSize = position->getNz();
}

void TGalaxyGrid::buildGrid(ref_ptr<TPositionGrid> position) {
	setPosition(position);
	grid = new ScalarGrid3D(xSize, ySize, zSize);
}

void TGalaxyGrid::calculateGrid(const double& smoothingRadius) {
	for (size_t i = 0; i < position->getNElements(); ++i)
		grid->get(i) = (smoothingRadius < pc) ?
				distribution(position->getPosition(i)) :
				getSmoothedDistribution(position->getPosition(i), smoothingRadius);
}

double TGalaxyGrid::getSmoothedDistribution(const TVector3d& pos, const double& smoothingRadius) {
	size_t nAverageSteps = 10, counter = 0;
	double mean = 0;
	for (double X = pos.x - smoothingRadius; X <= pos.x + smoothingRadius; X += smoothingRadius / (double)nAverageSteps)
		for (double Y = pos.y - smoothingRadius; Y <= pos.y + smoothingRadius; Y += smoothingRadius / (double)nAverageSteps)
			for (double Z = pos.z - smoothingRadius; Z <= pos.z + smoothingRadius; Z += smoothingRadius / (double)nAverageSteps) {
				mean += distribution(TVector3d(X, Y, Z));
				counter++;
			}
	return (mean / (double)counter);
}

double TGalaxyGrid::getTotal() const {
	double total = 0;
	for (size_t i = 0; i < grid->getNElements(); ++i)
		total += grid->get(i);
	return (total);
}

double TGalaxyGrid::getTotal2() const {
	double total2 = 0;
	for (size_t i = 0; i < grid->getNElements(); ++i)
		total2 += grid->get(i) * grid->get(i);
	return (total2);
}

double TGalaxyGrid::getMean() const {
	double mean = 0;
	for (size_t i = 0; i < grid->getNElements(); ++i)
		mean += grid->get(i);
	return (mean / (double)grid->getNElements());
}

double TGalaxyGrid::getVolumeIntegralCyl() {
	TAxis<double> r, z;

	const double dr = 10.0 * pc;
	const size_t nr = ceil((position->getXMax()-position->getXMin()) / dr);
	r.buildLinearAxis(position->getXMin(), position->getXMax(), nr);

	const double dz = 1.0 * pc;
	const size_t nz = ceil((position->getZMax()-position->getZMin()) / dz);
	z.buildLinearAxis(position->getZMin(), position->getZMax(), nz);

	std::vector<double> IntDz(nr, 0);
	for (size_t ix = 0; ix < nr; ++ix)
		for (size_t iz = 0; iz < nz; ++iz) {
			double cj = (iz > 0 and iz < nz - 1) ? dz : dz / 2.0;
			IntDz[ix] += cj * r.getValue(ix) * r.getValue(ix) * distribution(TVector3d(r.getValue(ix), 0, z.getValue(iz)));
		}
	double IntDzDr = 0;
	for (size_t ix = 0; ix < nr; ++ix) {
		double ci = (ix > 0 and ix < nr - 1) ? dr : dr / 2.0;
		IntDzDr += ci * IntDz[ix];
	}

	return (4.0 * M_PI * IntDzDr);
}

double TGalaxyGrid::getVolumeIntegralCar() {
	return (0);
}

void TGalaxyGrid::scaleGrid(const double& norm) {
	const double vol = getVolumeIntegralCyl();
	for (size_t i = 0; i < grid->getNElements(); ++i)
		grid->get(i) *= norm / vol;
}

bool TGalaxyGrid::loadFromFITS(const std::string& filename) {
	return (true);
}

bool TGalaxyGrid::loadFromTXT(const std::string& filename) {
	return (true);
}

void TGalaxyGrid::dumpToFile(const std::string& filename) const {
	std::ofstream fout(filename.c_str());
	fout<<"#x[kpc]    y[kpc]    z[kpc]     f"<<"\n";
	fout<<std::scientific<<std::setprecision(3);
	for (size_t i = 0; i < xSize; ++i)
		for (size_t j = 0; j < ySize; ++j)
			for (size_t k = 0; k < zSize; ++k) {
				fout << position->getPosition(i,j,k) / kpc << "\t" << grid->get(i,j,k) << "\n";
			}
	fout.close();
}

} /* namespace */
