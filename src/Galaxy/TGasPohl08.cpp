#include "Galaxy/TGasPohl08.h"

namespace DRAGON {

TH2Pohl08::TH2Pohl08(const std::string& fitsFilename) {
	this->fitsFilename = fitsFilename;
	if (fileExist(fitsFilename))
		loadMap();
	else
		throw TDragonExceptions("Error reading FITS file: " + fitsFilename + "\n");
}

void TH2Pohl08::loadMap() {
	TReadCfitsioFile cfitsioFile(fitsFilename);

	//size_t nAxes = cfitsioFile.readIntKeyword("NAXIS");
	Nx = cfitsioFile.readIntKeyword("NAXIS1");
	Ny = cfitsioFile.readIntKeyword("NAXIS2");
	Nz = cfitsioFile.readIntKeyword("NAXIS3");

	double x0 = cfitsioFile.readDoubleKeyword("CRVAL1") * pc;
	double dx = cfitsioFile.readDoubleKeyword("CDELT1") * pc;

	double y0 = cfitsioFile.readDoubleKeyword("CRVAL2") * pc;
	double dy = cfitsioFile.readDoubleKeyword("CDELT2") * pc;

	double z0 = cfitsioFile.readDoubleKeyword("CRVAL3") * pc;
	double dz = cfitsioFile.readDoubleKeyword("CDELT3") * pc;

	origin.setXYZ(x0, y0, z0);
	spacing.setXYZ(dx, dy, dz);

	XcoFactor = 2.3e20;

	long nelements = Nx * Ny * Nz;
	std::vector<double> vectorMap = cfitsioFile.readDoubleImage(nelements);

	map = new ScalarGrid3D (Nx, Ny, Nz);

	std::vector<double>::iterator it = vectorMap.begin();
	for (size_t ix = 0; ix < Nx; ++ix)
		for (size_t iy = 0; iy < Ny; ++iy)
			for (size_t iz = 0; iz < Nz; ++iz) {
				map->get(ix, iy, iz) = *it;
				it++;
			}
}


double TH2Pohl08::density(const TVector3d &pos) {
	// position on a unit grid
	TVector3d r = (pos - origin) / spacing;

	// indices of lower and upper neighbors
	int ix, iX, iy, iY, iz, iZ;

	ix = int(floor(r.x));
	iX = ix + 1;
	if (ix < 1 or ix > Nx-2)
		return (0);

	iy = int(floor(r.y));
	iY = iy + 1;
	if (iy < 1 or iy > Ny-2)
		return (0);

	iz = int(floor(r.z));
	iZ = iz + 1;
	if (iz < 1 or iz > Nz-2)
		return (0);

	// linear fraction to lower and upper neighbors
	double fx = r.x - floor(r.x);
	double fX = 1. - fx;
	double fy = r.y - floor(r.y);
	double fY = 1. - fy;
	double fz = r.z - floor(r.z);
	double fZ = 1. - fz;

	// trilinear interpolation (see http://paulbourke.net/miscellaneous/interpolation)
	double b = 0.;
	//V000 (1 - x) (1 - y) (1 - z) +
	b += map->get(ix, iy, iz) * fX * fY * fZ;
	//V100 x (1 - y) (1 - z) +
	b += map->get(iX, iy, iz) * fx * fY * fZ;
	//V010 (1 - x) y (1 - z) +
	b += map->get(ix, iY, iz) * fX * fy * fZ;
	//V001 (1 - x) (1 - y) z +
	b += map->get(ix, iy, iZ) * fX * fY * fz;
	//V101 x (1 - y) z +
	b += map->get(iX, iy, iZ) * fx * fY * fz;
	//V011 (1 - x) y z +
	b += map->get(ix, iY, iZ) * fX * fy * fz;
	//V110 x y (1 - z) +
	b += map->get(iX, iY, iz) * fx * fy * fZ;
	//V111 x y z
	b += map->get(iX, iY, iZ) * fx * fy * fz;

	return (b);
}

} /* namespace */
