#include "Galaxy/Xco.h"

namespace DRAGON {

double TXcoArimoto96::distribution(const TVector3d& pos) {
	const double r = pos.getR();
	return (9e19 / 1e20 * exp(r / 7.1)); // see Eq. 3 in Ferriere+07

}

double TXcoStrong04::distribution(const TVector3d& pos) {
	const double r = pos.getR();
	if (r < 3.5)
		return (0.4);
	else if (r < 5.5)
		return (0.6);
	else if (r < 7.5)
		return (0.8);
	else if (r < 9.5)
		return (1.5);
	else
		return (10.);
}

double TXcoEvoli12::distribution(const TVector3d& pos) {
	return ((pos.getR() < rBorder) ? XcoInner : XcoOuter);
}

double TXcoAckermann10::distribution(const TVector3d& pos) {
	const double r = pos.getR();
	if (r < 2.0)
		return (0.4);
	else if (r < 5.5)
		return (1.5);
	else if (r < 7.5)
		return(1.5);
	else if (r < 10.0)
		return(0.75);
	else if (r < 12.0)
		return(5.);
	else if (r < 17.0)
		return(12.);
	else if (r < 19.0)
		return(60.);
	else if (r < 30.0)
		return(200.);
	else
		return(200.);
}

double TXcoMyModel::distribution(const TVector3d& pos) {
	throw TDragonExceptions("You are trying to use MyModel without implementing it.");
	return (0);
}

} // namespace
