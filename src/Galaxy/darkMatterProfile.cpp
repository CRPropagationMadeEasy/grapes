#include "Galaxy/darkMatterProfile.h"

namespace DRAGON {

double TDarkProfileConstant::distribution(const TVector3d& pos) {
	return (constantValue);
}


double TDarkProfileMyModel::distribution(const TVector3d& pos){
	throw TDragonExceptions("You are trying to use MyModel without implementing it.");
	return (0);
}


TDarkProfileGNFW::TDarkProfileGNFW(double alpha, double beta, double gamma) : TGalaxyGrid() {
	this->alpha = alpha;
	this->beta = beta;
	this->gamma = gamma;
	rhos = rs = 0;
}

void TDarkProfileGNFW::findScaleRadius()
{
	int status;
	int iter = 0, max_iter = 100;

	const gsl_root_fsolver_type *T;
	gsl_root_fsolver *s;

	double r = 0;
	double x_lo = 0.0, x_hi = 5.0;

	gsl_function F;

	//struct quadratic_params params = {1.0, 0.0, -5.0};

	F.function = TDarkProfileGNFW::minimizeFunction;
	F.params = NULL;//&params;

	T = gsl_root_fsolver_brent;
	s = gsl_root_fsolver_alloc (T);
	gsl_root_fsolver_set (s, &F, x_lo, x_hi);

	std::cout << "using " << gsl_root_fsolver_name (s) << "method\n";

	do
	{
		iter++;
		status = gsl_root_fsolver_iterate (s);
		r = gsl_root_fsolver_root (s);
		x_lo = gsl_root_fsolver_x_lower (s);
		x_hi = gsl_root_fsolver_x_upper (s);
		status = gsl_root_test_interval (x_lo, x_hi, 0, 0.001);

		if (status == GSL_SUCCESS)
			std::cout << "Converged:\n";

		std::cout << iter << " " << x_lo << " " << x_hi << " " << r << " " << x_hi - x_lo << "\n";
	}
	while (status == GSL_CONTINUE && iter < max_iter);

	gsl_root_fsolver_free (s);

	return;
}

void TDarkProfileGNFW::computeProfileParams(double mwDarkMass, double mwDarkRmax, double mwDarkLocalDensity) {
	double rSun = 8.5 * kpc;
	rs = 0.1; //computeScaleRadius();
	rhos = mwDarkLocalDensity / profile(rSun / rs);
}

double TDarkProfileGNFW::profile(const double& x)
{
	double profile = 1. / pow(x, gamma) / pow(1. + pow(x, alpha), (beta - gamma) / alpha);
	return (profile);
}

double TDarkProfileGNFW::distribution(const TVector3d& pos) {
	double r = pos.getR();
	return (rhos * profile(r / rs));
}


TDarkProfileCored::TDarkProfileCored(double beta, double gamma) : TGalaxyGrid() {
	this->beta = beta;
	this->gamma = gamma;
	rhos = rs = 0;
}

double TDarkProfileCored::distribution(const TVector3d& pos) {
	double r = pos.getR();
	double density = rhos / pow(1. + (r * r) / (rs * rs), beta) / pow(1. + r / rs, gamma);
	return (density);
}


TDarkProfileEinasto65::TDarkProfileEinasto65(double alpha) : TGalaxyGrid() {
	this->alpha = alpha;
	rhos = rs = 0;
}

double TDarkProfileEinasto65::distribution(const TVector3d& pos) {
	double r = pos.getR();
	double density = rhos * exp(- 2. / alpha * (pow(r / rs, alpha) - 1.));
	return (density);
}


TDarkProfileStadel09::TDarkProfileStadel09(double alpha) : TGalaxyGrid() {
	this->alpha = alpha;
	rhos = rs = 0;
}

double TDarkProfileStadel09::distribution(const TVector3d& pos) {
	double r = pos.getR();
	double density = rhos * exp(- alpha * pow(log(1. + r / rs), 2.));
	return (density);
}

} /* namespace */
