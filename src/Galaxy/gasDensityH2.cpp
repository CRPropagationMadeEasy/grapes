#include "Galaxy/gasDensityH2.h"

namespace DRAGON {

TH2DensityConstant::TH2DensityConstant(const double& constantValue_) : TGalaxyGrid() {
	this->constantValue = constantValue_;
}

double TH2DensityConstant::distribution(const TVector3d& pos) {
	return (constantValue);
}


TH2DensityFerriere07::TH2DensityFerriere07() : TGalaxyGrid() {
	gasModel = new TH2Ferriere07();
}

double TH2DensityFerriere07::distribution(const TVector3d& pos) {
	if (position->getGridType() == g3D) {
		return (gasModel->getDensity3D(pos));
	}
	else {
		return (gasModel->getDensity2D(pos));
	}
}


TH2DensityPohl08::TH2DensityPohl08(const std::string& filename) : TGalaxyGrid() {
	gasModel = new TH2Pohl08(filename);
}

double TH2DensityPohl08::distribution(const TVector3d& pos) {
	if (position->getGridType() == g3D) {
		return (gasModel->getDensity3D(pos));
	}
	else {
		return (gasModel->getDensity2D(pos));
	}
}


double TH2DensityBronfman88::distribution(const TVector3d& pos) {
	double r = pos.getR();

	int i;
	double nH2_ = 0.0, fR, fZ0, fZh;                                            // [B88]/Table 3
	double R[18] = { 0.00, 2.25, 2.75, 3.25, 3.75, 4.25, 4.75, 5.25, 5.75,      // kpc, col.1
			6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75, 10.25 };
	double Y[18] = { 0.00,  1.5,  3.3,  5.8,  5.5,  8.4,  9.0,  9.6,  8.6,      // CO, K km s^-1
			9.1,  7.9,  9.2,  7.7,  5.0,  3.6,  4.8,  1.7,  0.0}; // (col.4)
	double Z0[18] = {0.039,0.039,0.036,0.000,-.008,0.001,-.010,-.001,-.004,     // kpc, col.7
			-.019,-.022,-.014,-.009,-.004,0.013,-.004,-.020,-.020};
	double Zh[18] = {0.077,0.077,0.080,0.061,0.065,0.071,0.072,0.082,0.083,     // kpc, col.10
			0.073,0.063,0.058,0.072,0.080,0.066,0.023,0.147,0.147};

	if (r > R[17])
		return (nH2_);

	for (i=0; i<17; i++)
		if(R[i] <= r && r <= R[i+1])  break;

	fR = Y[i] + (Y[i+1] - Y[i]) / (R[i+1] - R[i]) * (r - R[i]);
	fZ0 = Z0[i] + (Z0[i+1] -Z0[i]) / (R[i+1] - R[i]) * (r - R[i]);
	fZh = Zh[i] + (Zh[i+1] -Zh[i]) / (R[i+1] - R[i]) * (r - R[i]);
	nH2_ = fR * exp( -log(2.)*pow((pos.z - fZ0) / fZh, 2.0)) / kpc;

	return (nH2_ < 0. ? 0. : nH2_);
}


double TH2DensityNakanishi06::distribution(const TVector3d& pos) {
	double r = pos.getR();
	double E1 = 11.2 * exp(-(r * r) / 0.874);
	double E2 = 0.83 * exp(-pow((r - 4.0) / 3.2, 2.0));
	double h = (1.06 / 1000.0 * (10.8 * exp(0.28 * r) + 42.78));

	return (2.0 * 0.94 * (E1 + E2) * exp(-M_LN2 * pow(pos.z / h, 2.0)));
}


double TH2DensityMyModel::distribution(const TVector3d& pos) {
	throw TDragonExceptions("You are trying to use MyModel without implementing it.");
	return (0);
}

} // namespace
