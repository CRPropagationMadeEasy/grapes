#include "Galaxy/gasDensityHI.h"

namespace DRAGON {

double THIDensityConstant::distribution(const TVector3d& pos) {
	return (constantValue);
}


THIDensityFerriere07::THIDensityFerriere07() : TGalaxyGrid()
{
	gasModel = new THIFerriere07();
}

double THIDensityFerriere07::distribution(const TVector3d& pos)
{
	if (position->getGridType() == g3D) {
		return (gasModel->getDensity3D(pos));
	}
	else {
		return (gasModel->getDensity2D(pos));
	}
}


THIDensityNakanishi03::THIDensityNakanishi03() : TGalaxyGrid() {
	h0 = 1.06 * pc;
	n0 = 0.94 * (1./cm3);
}

double THIDensityNakanishi03::distribution(const TVector3d& pos)
{
	double rKpc = pos.getR() / kpc;
	double exp1 = exp(-rKpc / 2.4);
	double exp2 = exp(-pow((rKpc - 9.5) / 4.8, 2));
	double densityOnPlane = n0 * (0.6 * exp1 + 0.24 * exp2);
	double scaleHeight = h0 * (116.3 + 19.3 * rKpc + 4.1 * rKpc * rKpc - 0.05 * rKpc * rKpc * rKpc);

	return (densityOnPlane * exp(-M_LN2 * pow(pos.z / scaleHeight, 2)));
}

THIDensityMoskalenko02::THIDensityMoskalenko02() : TGalaxyGrid()
{
	double RfromTable[30] = {0.0, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5,
			7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11.0, 11.5, 12.0, 12.5, 13.0,
			13.5, 14.0, 14.5, 15.0, 15.5, 16.0}; // col.1, kpc

	double NHIfromTable[30] = {.10, .13, .14, .16, .19, .25, .30, .33, .32, .31,
			.30, .37, .38, .36, .32, .29, .38, .40, .25, .23,
			.32, .36, .32, .25, .16, .10, .09, .08, .06, .00};  // nHI, cm^-3

	for (size_t i = 0; i < 30; i++)
	{
		Rkpc.push_back(RfromTable[i]);
		NHI.push_back(NHIfromTable[i] * 1./cm3);
	}

	nGB = 0.33, nDL = 0.57; // cm^-3, disk density @ 4-8 kpc; [GB76][DL90]

	A1 = 0.395, A2 = 0.107, B = 0.0640; // cm^-3, Z-distribution parameters from [DL90]
	z1 = 0.212/2., z2 = 0.530/2., zh = 0.403; // kpc, Z-distribution parameters from [DL90]
}

double THIDensityMoskalenko02::calcfR(const double& rKpc, const double& zKpc)
{
	double fR;

	double R1 = 0, R2 = Rkpc[29];
	double Y1 = 0, Y2 = NHI[29];

	unsigned int i = 0;

	for (i = 0; i < 29; ++i) if (Rkpc[i] <= rKpc && rKpc <= Rkpc[i+1])  break;

	R1 = (Rkpc[i] + Rkpc[i+1]) / 2.0;
	Y1 = NHI[i];

	if (rKpc < R1)
	{
		if (i > 0)
		{
			R2 = (Rkpc[i-1] + Rkpc[i]) / 2.0;
			Y2 = NHI[i-1];
		}
		else
		{
			R2 = Rkpc[0];
			Y2 = NHI[0];
		}
	}
	else if (i < 28)
	{
		R2 = (Rkpc[i+1] + Rkpc[i+2]) / 2.0;
		Y2 = NHI[i+1];
	}

	fR = Y1 + (Y2 - Y1) / (R2 - R1) * (rKpc - R1); // interpolation in R

	R2 = (Rkpc[28] + Rkpc[29]) / 2.0;

	if (rKpc > R2)
		fR = NHI[28] * exp(-(rKpc - R2) / 3.0); // extrapolation in R

	return (fR);
}

double THIDensityMoskalenko02::calcfZ(const double& rKpc, const double& zKpc)
{
	double fZ;

	double fZ1 = 0, fZ2 = 0;

	if (rKpc < 10.0) // [DL90]
		fZ1 = A1 * exp(-log(2.) * pow(zKpc / z1, 2.0)) + A2 * exp(-log(2.) * pow(zKpc / z2, 2.0)) + B * exp(-fabs(zKpc) / zh);

	if (rKpc > 8.0)
		fZ2 = nDL * exp(-pow(zKpc / (0.0523 * exp(0.11 * rKpc)), 2.0)); // [C86]

	if (rKpc <= 8.0)
		fZ = fZ1;
	else if (rKpc >= 10.0)
		fZ = fZ2;
	else
		fZ = fZ1 + (fZ2 -fZ1) / 2.0 * (rKpc - 8.0); // interp. [DL90] & [C86]

	return (fZ);
}

double THIDensityMoskalenko02::distribution(const TVector3d& pos)
{
	double rKpc = pos.getR() / kpc;
	double zKpc = pos.z / kpc;

	if (rKpc > 16.0)
		return (0.);

	double fR = calcfR(rKpc, zKpc);
	double fZ = calcfZ(rKpc, zKpc);

	return (fZ * fR / nGB);
}


double THIDensityMyModel::distribution(const TVector3d& pos) {
	throw TDragonExceptions("You are trying to use MyModel without implementing it.");
	return (0);
}

} /* namespace */
