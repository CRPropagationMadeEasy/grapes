#include "Galaxy/gasDensityHII.h"

namespace DRAGON {

double THIIDensityConstant::distribution(const TVector3d& pos) {
	return (constantValue);
}

THIIDensityCordes91::THIIDensityCordes91() : TGalaxyGrid() {
	fne1 = 0.025;
	H1 = 1.00;
	A1 = 20.0;
	fne2 = 0.200;
	H2 = 0.15;
	A2 = 2.0;
	R2 = 4.0;
}


double THIIDensityCordes91::distribution(const TVector3d& pos) {
	double r = pos.getR();
	double ne1 = fne1 * exp(-fabs(pos.z)/H1) * exp (-pow(r/A1, 2.0));
	double ne2 = fne2 * exp(-fabs(pos.z)/H2) * exp (-pow((r - R2)/A2, 2.0));
	return (ne1 + ne2);
}


double THIIDensityNE2001::distribution(const TVector3d& pos) {
	return (model.ne_outer(pos.x, pos.y, pos.z) + model.ne_inner(pos.x, pos.y, pos.z) + model.ne_gc(pos.x, pos.y, pos.z));
}


double THIIDensityFerriere07::distribution(const TVector3d& pos) {
	return (0);
}


double THIIDensityMyModel::distribution(const TVector3d& pos) {
	throw TDragonExceptions("You are trying to use MyModel without implementing it.");
	return (0);
}

} // namespace



