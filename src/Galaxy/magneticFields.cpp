#include "Galaxy/magneticFields.h"

namespace DRAGON {

void TMagneticField::buildGrid() {
	//xGalaxy = spatialAxes->getXGalaxy().axis;
	//yGalaxy = spatialAxes->getYGalaxy().axis;
	//zGalaxy = spatialAxes->getZGalaxy().axis;
	xSize = 1.;//xGalaxy.size();
	ySize = 1.;//yGalaxy.size();
	zSize = 1.;//zGalaxy.size();

	regularMagneticField = new VectorGrid(xSize, ySize, zSize);
	randomMagneticField = new ScalarGrid3D(xSize, ySize, zSize);
}

void TMagneticField::calculateRandomField() {
	for (size_t i = 0; i < xSize; ++i)
		for (size_t j = 0; j < ySize; ++j)
			for (size_t k = 0; k < zSize; ++k)
			{
				randomMagneticField->get(i,j,k) = randomMagneticFieldFunction(xGalaxy[i],yGalaxy[j],zGalaxy[k]);
			}
	return ;
}

void TMagneticField::calculateRegularField() {
	for (size_t i = 0; i < xSize; ++i)
		for (size_t j = 0; j < ySize; ++j)
			for (size_t k = 0; k < zSize; ++k)
			{
				regularMagneticField->get(i,j,k) = regularMagneticFieldFunction(xGalaxy[i],yGalaxy[j],zGalaxy[k]);
			}
	return ;
}

void TMagneticField::dumpToFile(const std::string& outputFilename) {
	std::ofstream fout(outputFilename.c_str());
	fout<<std::scientific<<std::setprecision(3);
	for (size_t i = 0; i < xSize; ++i)
		for (size_t j = 0; j < ySize; ++j)
			for (size_t k = 0; k < zSize; ++k)
			{
				TVector3d regularB = regularMagneticField->get(i,j,k);
				fout<<xGalaxy[i]<<"\t"<<yGalaxy[j]<<"\t"<<zGalaxy[k]<<"\t"<<randomMagneticField->get(i,j,k)<<"\t"<<regularB.getX()<<"\t"<<regularB.getY()<<"\t"<<regularB.getZ()<<"\n";
			}
	fout.close();
	return ;
}


TVector3d TExponentialMagneticField::regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {
	TVector3d value;
	const double magneticFieldMagnitude = B0 * exp(-(x_ - r0) / rc) * exp(-fabs(z_) / zc);
	const double theta = atan2(y_, x_);
	value.setX(magneticFieldMagnitude * cos(theta));
	value.setY(magneticFieldMagnitude * sin(theta));
	return (value);
}


TVector3d TSun2007MagneticField::regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {

	TVector3d value;

	const double r = sqrt(x_ * x_ + y_ * y_);
	const double theta = atan2(y_, x_);

	double Br_disk = D1(r,theta,z_) * D2(r,theta) * sin (deg2rad(p));
	double Btheta_disk = - D1(r,theta,z_) * D2(r,theta) * cos (deg2rad(p));
	double Bz_disk = B0z_disk;

	value.setValuesCylindrical(theta, Br_disk, Btheta_disk, Bz_disk);

	double Btheta_halo;
	if (fabs(z_) < z0_halo)
		Btheta_halo = B0_halo * 1./( 1. +  ( (fabs(z_)-z0_halo)/z1_halo_low ) ) * (r/r0_halo) * exp( -(r - r0_halo)/r0_halo );
	else
		Btheta_halo = B0_halo * 1./( 1. +  ( (fabs(z_)-z0_halo)/z1_halo_high ) ) * (r/r0_halo) * exp( -(r - r0_halo)/r0_halo );

	value.addValuesCylindrical(theta, 0., Btheta_halo, 0.);

	return (value);
}

const double TSun2007MagneticField::D1(const double& r_, const double& theta_, const double& z_) {

	double value;

	value = (r_ > rc_disk) ? B0_disk * exp( -(r_-rsun)/r0_disk - fabs(z_)/z0_disk )
			: Bc_disk;

	return (value);
}

const double TSun2007MagneticField::D2(const double& r_, const double& theta_) {

	double value;
	if (r_ > r1)
		value = 1.;
	else if (r_ > r2)
		value = -1.;
	else if (r_ > r3)
		value = 1.;
	else
		value = -1.;
	return (value);
}

double TSun2007MagneticField::randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {

	double value;
	value = Brandom; // do we really want a constant field here?
	return (value);
}


TVector3d TPshirkov11ASSMagneticField::regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {

	TVector3d value;

	const double r = sqrt(x_ * x_ + y_ * y_);
	const double theta = atan2(y_, x_);

	double Br_disk = D1(r,theta,z_) * D2(r,theta) * sin (deg2rad(p));
	double Btheta_disk = D1(r,theta,z_) * D2(r,theta) * cos (deg2rad(p));
	double Bz_disk = B0z_disk;

	value.setValuesCylindrical(theta, Br_disk, Btheta_disk, Bz_disk);

	double Btheta_halo;
	if (fabs(z_) < z0_halo)
		Btheta_halo = B0_halo * 1./( 1. +  ( (fabs(z_)-z0_halo)/z1_halo_low ) ) * (r/r0_halo) * exp( -(r - r0_halo)/r0_halo );
	else
		Btheta_halo = B0_halo * 1./( 1. +  ( (fabs(z_)-z0_halo)/z1_halo_high ) ) * (r/r0_halo) * exp( -(r - r0_halo)/r0_halo );

	value.addValuesCylindrical(theta, 0., Btheta_halo, 0.);

	return (value);
}

const double TPshirkov11ASSMagneticField::D1(const double& r_, const double& theta_, const double& z_) {

	double value;

	double b = 1. / tan (deg2rad(p));
	double phi = b * log(1 + d / rsun) - M_PI / 2.;

	value = (r_ > rc_disk) ? B0_disk * rsun / (r_ * cos(phi)) * exp(-fabs(z_ / z0))
			: B0_disk * rsun / (rc_disk * cos(phi)) * exp(-fabs(z_ / z0));

	return (value);
}

const double TPshirkov11ASSMagneticField::D2(const double& r_, const double& theta_) {

	double value;

	double b = 1. / tan (deg2rad(p));
	double phi = b * log(1 + d / rsun) - M_PI / 2.;

	value = fabs(cos(theta_ - b * log(r_ / rsun) + phi));

	return (value);
}

double TPshirkov11ASSMagneticField::randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {

	double value;
	value = Brandom; // do we really want a constant field here?
	return (value);
}


TVector3d TPshirkov11BSSMagneticField::regularMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {

	TVector3d value;

	const double r = sqrt(x_ * x_ + y_ * y_);
	const double theta = atan2(y_, x_);

	double Br_disk = D1(r,theta,z_) * D2(r,theta) * sin (deg2rad(p));
	double Btheta_disk = D1(r,theta,z_) * D2(r,theta) * cos (deg2rad(p));
	double Bz_disk = B0z_disk;

	value.setValuesCylindrical(theta, Br_disk, Btheta_disk, Bz_disk);

	double Btheta_halo;
	if (fabs(z_) < z0_halo)
		Btheta_halo = B0_halo * 1./( 1. +  ( (fabs(z_)-z0_halo)/z1_halo_low ) ) * (r/r0_halo) * exp( -(r - r0_halo)/r0_halo );
	else
		Btheta_halo = B0_halo * 1./( 1. +  ( (fabs(z_)-z0_halo)/z1_halo_high ) ) * (r/r0_halo) * exp( -(r - r0_halo)/r0_halo );

	value.addValuesCylindrical(theta, 0., Btheta_halo, 0.);

	return (value);
}

const double TPshirkov11BSSMagneticField::D1(const double& r_, const double& theta_, const double& z_) {

	double value;

	double b = 1. / tan (deg2rad(p));
	double phi = b * log(1 + d / rsun) - M_PI / 2.;

	value = (r_ > rc_disk) ? B0_disk * rsun / (r_ * cos(phi)) * exp(-fabs(z_ / z0))
			: B0_disk * rsun / (rc_disk * cos(phi)) * exp(-fabs(z_ / z0));

	return (value);
}

const double TPshirkov11BSSMagneticField::D2(const double& r_, const double& theta_) {

	double value;

	double b = 1. / tan (deg2rad(p));
	double phi = b * log(1 + d / rsun) - M_PI / 2.;

	value = cos(theta_ - b * log(r_ / rsun) + phi);

	return (value);
}

double TPshirkov11BSSMagneticField::randomMagneticFieldFunction(const double& x_, const double& y_, const double& z_) {

	double value;
	value = Brandom; // do we really want a constant field here?
	return (value);
}




//TExponentialMagneticField::TExponentialMagneticField(const double& magneticFieldAtSun,const double& rSun, TGalaxyGrids* galGrids) : TMagneticField(galGrids) /*, TGeometry* geom*/
//{
//	rScale=10.;
//	zScale=2.;
//	B0=magneticFieldAtSun;
//	rObs=rSun;
//
//	std::vector<double> x,y,z;
//
//	z = galGrids->zGalaxy;
//	x = galGrids->xGalaxy;
//
//	int counter = 0;
//
//	if (galGrids->type == g3D)
//	{
//		y = galGrids->yGalaxy;
//
//		for (int i = 0; i < x.size(); ++i)
//			for (int j = 0; j < y.size(); ++j)
//				for (int k = 0; k < z.size(); k++)
//				{
//					std::vector<double> bRegularTemp = getMagneticField(x[i],y[j],z[k]);
//
//					Breg[0][counter] = bRegularTemp[0];
//					Breg[1][counter] = bRegularTemp[1];
//					Breg[2][counter] = bRegularTemp[2];
//
//					double Breg_mod = pow(bRegularTemp[0],2)+pow(bRegularTemp[1],2)+pow(bRegularTemp[2],2);
//
//					Breg_versors[0][counter] = bRegularTemp[0]/sqrt(Breg_mod);
//					Breg_versors[1][counter] = bRegularTemp[1]/sqrt(Breg_mod);
//					Breg_versors[2][counter] = bRegularTemp[2]/sqrt(Breg_mod);
//
//					MagField[counter] = sqrt(Breg_mod) * pow( Coord->in_LB_MagField, Coord->IsInLocalBubble(x[i],y[j],z[k]) );
//
//					double spiral_factor_magfield = max( min( pow(geom->GetPattern(i,j,k), Coord->in_SA_MagField), Coord->in_SA_cut_MagField), 1./Coord->in_SA_cut_MagField );
//					MagField[counter] *= spiral_factor_magfield;
//
//					counter++;
//
//				}
//	}
//}
//}
//else {
//
//	ofstream datafile;
//	datafile.open("bfield_new.dat");
//
//	x = Coord->GetR();
//	for (int i = 0; i < dimx; i++) {
//		for (int k = 0; k < dimz; k++) {
//
//			MagField[counter] = B0*exp(-1.*(x[i]-robs)/r_scale)*exp(-1.*fabs(z[k])/z_scale);
//			datafile << x[i] << " " << z[k] << " " << MagField[counter] << endl;
//			counter++;
//		}
//	}
//}
//}




//TMagneticField::TMagneticField(TGalaxyGrids* galGrids) {

//	if (galGrids->getType() == g3D) // Coord->GetType() == "3D")
//	{
//		xGalaxy = galGrids->getXGalaxy();
//		yGalaxy = galGrids->getYGalaxy();
/*x = Coord->GetX();
       y = Coord->GetY();
       dimx = x.size();
       dimy = y.size();*/
//	}
//	else if (galGrids->getType() == g2D)
//	{
//		xGalaxy = galGrids->getXGalaxy();
//		yGalaxy.Size = 1;
//		/*x = Coord->GetR();
//       dimx = x.size();
//       dimy = 1;*/
//	}
//	else throw TDragonExceptions("Problems with TMagneticField constructor.");
//
//	zGalaxy = galGrids->getZGalaxy();
//	//z = Coord->GetZ();
//	//dimz = z.size();
//
//	magneticField = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//
//	if (galGrids->getType() == g3D)
//	{
//		magneticFieldRegular[0] = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//		magneticFieldRegular[1] = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//		magneticFieldRegular[2] = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//		magneticFieldRegularVersors[0] = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//		magneticFieldRegularVersors[1] = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//		magneticFieldRegularVersors[2] = std::vector<double>(xGalaxy.Size*yGalaxy.Size*zGalaxy.Size,0.0);
//	}
//
//	//double Bdisk,
//	//Bhalo;
//
//	//const double RC = 5.0,
//	//B0 = 2.0,
//	//Z0 = 1.0,
//	//R0 = 10.0;
//
//	//const double B0H = 4., R0H = 8., Z0H = 1.3; // MEAN
//	//const double B0H = 3., R0H = 6., Z0H = 1.;  // MIN
//	//const double B0H = 12., R0H = 15., Z0H = 2.5; // MAX
//
//
//	/*
//     for (int i = 0; i < dimx; i++) {
//     for (int l = 0; l < dimy; l++) {
//     double radius = 0.0;
//     if (y.size()) radius = sqrt(x[i]*x[i]+y[l]*y[l]);
//     else radius = x[i];
//     for (vector<double>::iterator zeta = z.begin(); zeta != z.end(); ++zeta) {
//
//     double Z1H = (fabs(*zeta) < Z0H) ? 0.2 : 0.4;
//
//     Bdisk = B0 * ( (radius < RC) ? exp(-fabs(*zeta)/Z0) : exp(-(radius-robs)/R0-fabs(*zeta)/Z0) );
//     Bhalo = B0H / (1.+pow((fabs(*zeta)-Z0H)/Z1H,2)) * radius/R0H * exp(-1.-radius/R0H);
//     MagField.push_back(Bdisk+Bhalo);
//     //MagField.push_back(Bh*exp(-((radius)-robs)/rB)*exp(-fabs((*zeta))/zr));
//     }
//     }
//     }
//	 */
//}
//
//std::vector<double> TMagneticField::getMagneticFieldRegularVersors(const double& x,const double& y,const double& z)
//{
//	std::vector<double> b = getMagneticField(x,y,z); // why not regular?
//	//cout << "GetBregVersors " << endl;
//	//cout << B[0] << " " << B[1] << " " << B[2] << endl;
//	const double bNorm = std::sqrt( std::pow(b[0],2) + std::pow(b[1],2) + std::pow(b[2],2) );
//	//cout << "Bmod = " << Bmod << endl;
//	b[0] /= bNorm;
//	b[1] /= bNorm;
//	b[2] /= bNorm;
//	return (b);
//}
//
//double TMagneticField::getMagneticEnergyDensity(const int& i)
//{
//	return (std::pow(magneticField[i],2.)/8.0/M_PI*cErgToEv);
//}
//
//double TMagneticField::getMagneticEnergyDensity(const int& ir, const int& iz)
//{
//	return ( getMagneticEnergyDensity(index(ir,iz)) );
//}
//
//double TMagneticField::getMagneticEnergyDensity(const int& ix, const int& iy, const int& iz)
//{
//	return ( getMagneticEnergyDensity(index(ix,iy,iz)) );
//}
//
//TUniformMagneticField::TUniformMagneticField(const double& magneticFieldConstantValue,TGalaxyGrids* galGrid) : TMagneticField(galGrid)
//{
//	int counter = 0;
//
//	for (unsigned int i = 0; i < xGalaxy.Size; i++) {
//		for (unsigned int l = 0; l < yGalaxy.Size; l++) {
//			for (unsigned int k = 0; k < zGalaxy.Size; k++) {
//
//				magneticField[counter] = magneticFieldConstantValue;
//
//				/*if(Coord->GetType() == "3D")
//           {
//           double spiral_factor_magfield = max( min( pow(geom->GetPattern(i,l,k), Coord->in_SA_MagField), Coord->in_SA_cut_MagField), 1./Coord->in_SA_cut_MagField );
//           MagField[counter] *= spiral_factor_magfield;
//
//           MagField[counter] *= pow( Coord->in_LB_MagField, Coord->IsInLocalBubble(x[i],y[l],z[k]) );
//           }*/
//
//				counter++;
//			}
//		}
//	}
//}
//
//
//TPshirkov11MagneticField::TPshirkov11MagneticField(TGalaxyGrids* galGrid) : TMagneticField(galGrid),
//		B0(1.), RC(1.), Z0(1.), R0(1.), B0H(1.), R0H(1.), Z0H(1.), B0turb(1.), rscaleTurb(1.), zscaleTurb(1.), robs(1.)
//{
//	std::vector<double> x, y, z;
//
//	z = galGrid->getZGalaxy().grid;
//
//	if (galGrid->getType() == g3D)
//	{
//		x = galGrid->getXGalaxy().grid;
//		y = galGrid->getYGalaxy().grid;
//	}
//	else if (galGrid->getType() == g2D)
//	{
//		x = galGrid->getXGalaxy().grid;
//	}
//
//	//double Bdisk, Bhalo;
//
//	//const double RC = 5.0,
//	//B0 = 2.0,
//	//Z0 = 1.0,
//	//R0 = 10.0;
//	//const double B0H = 4., R0H = 8., Z0H = 1.3; // MEAN
//	//const double B0H = 3., R0H = 6., Z0H = 1.;  // MIN
//	//const double B0H = 12., R0H = 15., Z0H = 2.5; // MAX
//
//	int counter = 0;
//
//	for (int i = 0; i < x.size(); ++i)
//		for (int l = 0; l < y.size(); ++l)
//			for (int k = 0; k < z.size(); k++)
//			{
//				std::vector<double> B = (galGrid->getType() == g3D)
//        										  ? getMagneticField(x[i],y[l],z[k])
//        												  : getMagneticField(x[i],0,z[k]);
//
//				if (galGrid->getType() == g3D)
//				{
//					magneticFieldRegular[0][counter] = B[0];
//					magneticFieldRegular[1][counter] = B[1];
//					magneticFieldRegular[2][counter] = B[2];
//					const double BregNorm = std::sqrt(B[0]*B[0]+B[1]*B[1]+B[2]*B[2]);
//					magneticFieldRegularVersors[0][counter] = magneticFieldRegular[0][counter]/BregNorm;
//					magneticFieldRegularVersors[1][counter] = magneticFieldRegular[1][counter]/BregNorm;
//					magneticFieldRegularVersors[2][counter] = magneticFieldRegular[2][counter]/BregNorm;
//					//MagField.push_back(Bh*exp(-((radius)-robs)/rB)*exp(-fabs((*zeta))/zr));
//					magneticField[counter] = std::sqrt(BregNorm*BregNorm+B[3]*B[3]);
//				}
//				else
//				{
//					magneticField[counter] = std::sqrt(B[0]*B[0]+B[1]*B[1]+B[2]*B[2]+B[3]*B[3]);
//				}
//
//				/*if(Coord->GetType() == "3D")
//           {
//           double spiral_factor_magfield = max( min( pow(geom->GetPattern(i,l,k), Coord->in_SA_MagField), Coord->in_SA_cut_MagField), 1./Coord->in_SA_cut_MagField );
//           MagField[counter] *= spiral_factor_magfield;
//
//           MagField[counter] *= pow( Coord->in_LB_MagField, Coord->IsInLocalBubble(x[i],y[l],z[k]) );
//           }*/
//
//				counter++;
//			}
//}
//
//std::vector<double> TPshirkov11MagneticField::getMagneticField(const double& x, const double& y, const double& z)
//{
//	const double Z1H = (fabs(z) < Z0H) ? 0.2 : 0.4;
//	const double radius = std::sqrt(x*x+y*y);
//	const double Bdisk = B0 * ( (radius < RC) ? std::exp(-std::fabs(z)/Z0) : std::exp(-(radius-robs)/R0-std::fabs(z)/Z0) );
//	const double Bhalo = B0H / (1.+std::pow((std::fabs(z)-Z0H)/Z1H,2)) * radius/R0H * std::exp(1.-radius/R0H);
//	const double BregNorm = Bdisk+Bhalo;
//	const double Brand = B0turb*std::exp(-(radius-robs)/rscaleTurb)*std::exp(-fabs((z)/zscaleTurb));
//	const double phi = std::atan2(y,x)+M_PI/2.0;
//
//	std::vector<double> b;
//	b.push_back(BregNorm*std::cos(phi));
//	b.push_back(BregNorm*std::sin(phi));
//	b.push_back(0);
//	b.push_back(Brand);
//
//	return (b);
//}
//
//TJanssonFarrar12MagneticField::TJanssonFarrar12MagneticField(const double& betaFarrar, TGalaxyGrids* galGrids) : TMagneticField(galGrids),
//		bring(0.1), hdisk(0.40), wdisk(0.27), Bn(1.4), Bs(-1.1), rn(9.22), rs(16.7), wh(0.2), z0(5.3), BX(4.6), Theta0X(49.0*M_PI/180.0), rcX(4.8), rX(2.9), p(11.5*M_PI/180.0)
//{
//	bj = std::vector<double>(8,0);
//	bj[0] = 0.1;
//	bj[1] = 3.0;
//	bj[2] = -0.9;
//	bj[3] = -0.8;
//	bj[4] = -2.0;
//	bj[5] = -4.2;
//	bj[6] = 0.0;
//	bj[7] = 0.0;
//
//	fj = std::vector<double>(8,0);
//	fj[0] = 0.130;
//	fj[1] = 0.165;
//	fj[2] = 0.094;
//	fj[3] = 0.122;
//	fj[4] = 0.13;
//	fj[5] = 0.118;
//	fj[6] = 0.084;
//	fj[7] = 0.156;
//
//	for (int i = 0; i < 7; i++) bj[7] -= (fj[i]*bj[i]/fj[7]);
//
//	rj = std::vector<double>(8,0);
//	rj[0] = 5.1;
//	rj[1] = 6.3;
//	rj[2] = 7.1;
//	rj[3] = 8.3;
//	rj[4] = 9.8;
//	rj[5] = 11.4;
//	rj[6] = 12.7;
//	rj[7] = 15.5;
//
//	std::vector<double> x, y, z;
//
//	x = galGrids->getXGalaxy().grid;
//	z = galGrids->getZGalaxy().grid;
//
//	if (galGrids->getType() == g3D)
//	{
//		y = galGrids->getYGalaxy().grid;
//		//dimx = x.size();
//		//dimy = y.size();
//	}
//
//	int counter = 0;
//
//	if (galGrids->getType() == g3D)
//	{
//		//x = Coord->GetX();
//		//y = Coord->GetY();
//
//		for (int i = 0; i < x.size(); ++i)
//			for (int j = 0; j < y.size(); ++j)
//				for (int k = 0; k < z.size(); ++k)
//				{
//					std::vector<double> Breg1 = getMagneticField(x[i],y[j],z[k]);
//
//					magneticFieldRegular[0][counter] = Breg1[0];
//					magneticFieldRegular[1][counter] = Breg1[1];
//					magneticFieldRegular[2][counter] = Breg1[2];
//
//					const double Breg_mod = Breg1[0]*Breg1[0]+Breg1[1]*Breg1[1]+Breg1[2]*Breg1[2];
//
//					magneticFieldRegularVersors[0][counter] = Breg1[0]/sqrt(Breg_mod);
//					magneticFieldRegularVersors[1][counter] = Breg1[1]/sqrt(Breg_mod);
//					magneticFieldRegularVersors[2][counter] = Breg1[2]/sqrt(Breg_mod);
//
//					magneticField[counter] = std::sqrt((1.0+betaFarrar)*Breg_mod);
//
//					/*if(Coord->GetType() == "3D")
//             {
//             double spiral_factor_magfield = max( min( pow(geom->GetPattern(i,j,k), Coord->in_SA_MagField), Coord->in_SA_cut_MagField), 1./Coord->in_SA_cut_MagField );
//             MagField[counter] *= spiral_factor_magfield;
//
//             MagField[counter] *= pow( Coord->in_LB_MagField, Coord->IsInLocalBubble(x[i],y[j],z[k]) );
//             }*/
//
//					counter++;
//				}
//	}
//	else if (galGrids->getType() == g2D)
//	{
//		for (int i = 0; i < x.size(); ++i)
//			for (int k = 0; k < z.size(); ++k)
//			{
//				std::vector<double> bAveraged = getMagneticFieldAveraged(x[i],z[k]);
//				const double Bav2 = bAveraged[0]*bAveraged[0]+bAveraged[1]*bAveraged[1]+bAveraged[2]*bAveraged[2];
//
//				magneticField[counter] = std::sqrt((1.0+betaFarrar)*Bav2);
//				counter++;
//			}
//	}
//}
//
//std::vector<double> TJanssonFarrar12MagneticField::getMagneticField(const double& x, const double& y, const double& z)
//{
//	std::vector<double> bOut(3,0);
//
//	const double r = sqrt(x*x+y*y);
//	//if (r > 20 || sqrt(r*r+z*z) < 1) return bOut;
//
//	double Bdisk=0;
//	double Bhalo=0;
//	double Bxx=0;
//
//	double rp=0;
//	double ThetaX=0;
//	double phi = std::atan2(y,x);
//	double sinphi = std::sin(phi);
//	double cosphi = std::cos(phi);
//	double Ldisk = 1.0 / (1.0 + std::exp(-2.0*(fabs(z)-hdisk)/wdisk) );
//
//	//if (r > 3.0) {
//	if (r < 5.0)
//	{
//		bOut[0] = -bring*sinphi*(1.0-Ldisk);
//		bOut[1] = bring*cosphi*(1.0-Ldisk);
//	}
//	else
//	{
//		const double tanfactor = 1.0/tan(M_PI/2.-p);
//		double rxneg = r*exp(-(phi-M_PI)*tanfactor);
//		if (rxneg > rj[7]) rxneg = r*exp(-(phi+M_PI)*tanfactor);
//		if (rxneg > rj[7]) rxneg = r*exp(-(phi+3.0*M_PI)*tanfactor);
//		for (int loopcounter = 7; loopcounter>=0; loopcounter--) { if (rxneg < rj[loopcounter]) Bdisk = bj[loopcounter]; }
//
//		Bdisk *= (5.0/r);
//		bOut[0] = Bdisk*sin(p-phi)*( 1.0 - Ldisk );
//		bOut[1] = Bdisk*cos(p-phi)*( 1.0 - Ldisk );
//	}
//	//}
//
//	double Lhalo = 0;
//
//	if (z>=0)
//	{
//		Bhalo = Bn;
//		Lhalo = 1.0 / (1.0 + std::exp(-2.0*(r-rn)/wh) );
//	}
//	else
//	{
//		Bhalo = Bs;
//		Lhalo = 1.0 / (1.0 + std::exp(-2.0*(r-rs)/wh) );
//	}
//
//	Bhalo *= (std::exp(-fabs(z)/z0)*Ldisk*(1.0-Lhalo));
//	bOut[0] -= (Bhalo*sinphi);
//	bOut[1] += (Bhalo*cosphi);
//
//	const double ztheta0x = fabs(z)/std::tan(Theta0X);
//	const double rpcX = rcX+ztheta0x;
//
//	if (r<rpcX)
//	{ // interior region, with varying elevation angle
//		rp   = r*rcX/rpcX;
//		ThetaX = (z!=0) ? std::atan(fabs(z)/(r-rp)) : M_PI/2.;
//		Bxx = BX*std::exp(-rp/rX)*std::pow(rcX/rpcX,2);
//	}
//	else
//	{ // exterior region with constant elevation angle
//		rp = r-ztheta0x;
//		ThetaX = Theta0X;
//		Bxx = BX*std::exp(-rp/rX)*(rp/r);
//	}
//
//	if (z<0)
//	{
//		bOut[0] -= (Bxx*cosphi*cos(ThetaX));
//		bOut[1] -= (Bxx*sinphi*cos(ThetaX));
//	}
//	else
//	{
//		bOut[0] += (Bxx*cosphi*cos(ThetaX));
//		bOut[1] += (Bxx*sinphi*cos(ThetaX));
//	}
//
//	bOut[2] += (Bxx*sin(ThetaX));
//
//	bOut[0] *= 1.e-6;
//	bOut[1] *= 1.e-6;
//	bOut[2] *= 1.e-6;
//
//	return (bOut);
//}
//
//std::vector<double> TJanssonFarrar12MagneticField::getMagneticFieldInCylindricalCoords(const double& r, const double& phi, const double& z)
//{
//	return (TJanssonFarrar12MagneticField::getMagneticField(r*std::cos(phi),r*std::sin(phi),z));
//}
//
//std::vector<double> TJanssonFarrar12MagneticField::getMagneticFieldAveraged(const double& r, const double& z)
//{
//	std::vector<double> bAveraged(3,0);
//	if (sqrt(r*r+z*z) < 1 || r > 20) return (bAveraged);
//
//	const int nAveraged = 100;
//
//	for (unsigned int i = 0; i < nAveraged; ++i)
//	{
//		const double phi = double(i)/double(nAveraged-1)*2.0*M_PI;
//		std::vector<double> bRegular = getMagneticFieldInCylindricalCoords(r,phi,z);
//		for (unsigned int j = 0; j < 3; ++j) bAveraged[j] += bRegular[j]/double(nAveraged);
//	}
//
//	return (bAveraged);
//}
//
///*ToyModelField::ToyModelField(double Bx_, double By_, double Bz_, double B0turb_, TGrid* Coord, TGeometry* geom) :
//				TBField(Coord),
//				Bx(Bx_),
//				By(By_),
//				Bz(Bz_),
//				B0turb(B0turb_)
//{
//
//	vector<double> x;
//	vector<double> y;
//
//	if (Coord->GetType() == "3D") {
//		x = Coord->GetX();
//		y = Coord->GetY();
//	}
//	else {
//		x = Coord->GetR();
//	}
//
//	vector<double> z = Coord->GetZ();
//	//cout << " dimx = " << dimx;
//	//cout << " dimy = " << dimy;
//
//	int counter = 0;
//	for (int i = 0; i < dimx; i++) {
//		for (int l = 0; l < dimy; l++) {
//			for (int k = 0; k < dimz; k++) {
//				vector<double> B = (Coord->GetType() == "3D") ? GetField(x[i],y[l],z[k]) : GetField(x[i],0,z[k]);
//
//				Breg[0][counter] = B[0];
//				Breg[1][counter] = B[1];
//				Breg[2][counter] = B[2];
//				double Breg_mod = sqrt( pow(B[0],2) + pow(B[1],2) +pow(B[2],2) );
//				Breg_versors[0][counter] = Breg[0][counter]/Breg_mod;
//				Breg_versors[1][counter] = Breg[1][counter]/Breg_mod;
//				Breg_versors[2][counter] = Breg[2][counter]/Breg_mod;
//
//				//MagField.push_back(Bh*exp(-((radius)-robs)/rB)*exp(-fabs((*zeta))/zr));
//				MagField[counter] = sqrt(pow(Breg_mod,2)+pow(B[3],2)) * pow( Coord->in_LB_MagField, Coord->IsInLocalBubble(x[i],y[l],z[k]) );
//
//				double spiral_factor_magfield = max( min( pow(geom->GetPattern(i,l,k), Coord->in_SA_MagField), Coord->in_SA_cut_MagField), 1./Coord->in_SA_cut_MagField );
//				MagField[counter] *= spiral_factor_magfield;
//
//				counter++;
//			}
//		}
//	}
//}*/
//
///*vector<double> ToyModelField::GetField(double x, double y, double z) {
//
//	vector<double> Bmixed;
//	//Bmixed.push_back(B0reg);
//	Bmixed.push_back(Bx);//
//	Bmixed.push_back(By);
//	Bmixed.push_back(Bz);
//	Bmixed.push_back(B0turb);
//
//	return Bmixed;
//}*/
//
//TExponentialMagneticField::TExponentialMagneticField(const double& magneticFieldAtSun,const double& rSun, TGalaxyGrids* galGrids) : TMagneticField(galGrids) /*, TGeometry* geom*/
//{
//	rScale=10.;
//	zScale=2.;
//	B0=magneticFieldAtSun;
//	rObs=rSun;
//
//	std::vector<double> x,y,z;
//
//	z = galGrids->zGalaxy;
//	x = galGrids->xGalaxy;
//
//	int counter = 0;
//
//	if (galGrids->type == g3D)
//	{
//		y = galGrids->yGalaxy;
//
//		for (int i = 0; i < x.size(); ++i)
//			for (int j = 0; j < y.size(); ++j)
//				for (int k = 0; k < z.size(); k++)
//				{
//					std::vector<double> bRegularTemp = getMagneticField(x[i],y[j],z[k]);
//
//					Breg[0][counter] = bRegularTemp[0];
//					Breg[1][counter] = bRegularTemp[1];
//					Breg[2][counter] = bRegularTemp[2];
//
//					double Breg_mod = pow(bRegularTemp[0],2)+pow(bRegularTemp[1],2)+pow(bRegularTemp[2],2);
//
//					Breg_versors[0][counter] = bRegularTemp[0]/sqrt(Breg_mod);
//					Breg_versors[1][counter] = bRegularTemp[1]/sqrt(Breg_mod);
//					Breg_versors[2][counter] = bRegularTemp[2]/sqrt(Breg_mod);
//
//					MagField[counter] = sqrt(Breg_mod) * pow( Coord->in_LB_MagField, Coord->IsInLocalBubble(x[i],y[j],z[k]) );
//
//					double spiral_factor_magfield = max( min( pow(geom->GetPattern(i,j,k), Coord->in_SA_MagField), Coord->in_SA_cut_MagField), 1./Coord->in_SA_cut_MagField );
//					MagField[counter] *= spiral_factor_magfield;
//
//					counter++;
//
//				}
//	}
//}
//}
//else {
//
//	ofstream datafile;
//	datafile.open("bfield_new.dat");
//
//	x = Coord->GetR();
//	for (int i = 0; i < dimx; i++) {
//		for (int k = 0; k < dimz; k++) {
//
//			MagField[counter] = B0*exp(-1.*(x[i]-robs)/r_scale)*exp(-1.*fabs(z[k])/z_scale);
//			datafile << x[i] << " " << z[k] << " " << MagField[counter] << endl;
//			counter++;
//		}
//	}
//}
//}
//
//std::vector<double> TExponentialMagneticField::getMagneticField(const double& x, const double& y, const double& z)
//{
//	std::vector<double> Breg1(3,0);
//
//	double r = sqrt(x*x+y*y);
//	if (fabs(r) < 1e-3) r = 1e-3;
//
//	double theta = atan(fabs(z)/r);
//	double phi = atan2(y,x)+M_PI/2.0;
//
//	double B_simple = B0*exp(-1.*(r-robs)/r_scale)*exp(-1.*fabs(z)/z_scale);
//
//	//     cout << "[MW-DEBUG BFIELD] " << x << " " << y << " " << z << " | " << B0 << " " << r_scale << " " << z_scale << " " << robs << " " << phi << " " << theta << " | " << B_simple << endl;
//
//	Breg1[0] += B_simple*cos(phi)*cos(theta);
//	Breg1[1] += B_simple*sin(phi)*cos(theta);
//	Breg1[2] += B_simple*sin(theta);
//
//	return Breg1;
//
//}

} /* namespace */
