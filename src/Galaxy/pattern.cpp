#include "Galaxy/pattern.h"

namespace DRAGON {

void TPattern::buildPattern(const TPatternParams& params) {

	//xGalaxy = spatialAxes->getXGalaxy().axis;
	//yGalaxy = spatialAxes->getYGalaxy().axis;
	//zGalaxy = spatialAxes->getZGalaxy().axis;
	xSize = 1;//xGalaxy.size();
	ySize = 1;//yGalaxy.size();
	zSize = 1;//zGalaxy.size();

	patternFunction = new ScalarGrid3D(xSize, ySize, zSize);
}

void TPattern::calculatePattern(const TPatternParams& params) {

	for (size_t i = 0; i < xSize; ++i)
		for (size_t j = 0; j < ySize; ++j)
			for (size_t k = 0; k < zSize; ++k)
			{
				patternFunction->get(i,j,k) = computePatternFunction(xGalaxy[i],yGalaxy[j],zGalaxy[k],params);
			}
	return ;
}


void TPattern::dumpToFile(const std::string& outputFilename) {

	std::ofstream fout(outputFilename.c_str());
	fout<<std::scientific<<std::setprecision(3);
	for (size_t i = 0; i < xSize; ++i)
		for (size_t j = 0; j < ySize; ++j)
			for (size_t k = 0; k < zSize; ++k)
			{
				double value = patternFunction->get(i,j,k);
				fout<<xGalaxy[i]<<"\t"<<yGalaxy[j]<<"\t"<<zGalaxy[k]<<"\t"<<patternFunction->get(i,j,k)<<"\n";
			}
	fout.close();
	return ;
}


double spiralPattern::computePatternFunction(const double& x_, const double& y_, const double& z_, const TPatternParams& p) {

	double value = 0.;

	double radius = sqrt(x_*x_ + y_*y_);
	double theta = atan2(y_,x_);

	double norm_gaussian = 1./(p.armWidth*sqrt(2.*M_PI));

	for (unsigned int i_arm=0; i_arm < p.numArms; i_arm++) {

		double arm_theta = (p.arms_K[i_arm] * log(radius/p.arms_r0[i_arm]) + p.arms_theta0[i_arm]);
		int n1 = floor(fabs(arm_theta - theta)/(2.*M_PI));
		int n2 =  ceil(fabs(arm_theta - theta)/(2.*M_PI));
		double lower_r = p.arms_r0[i_arm] * exp((theta - p.arms_theta0[i_arm] + n1*2.*M_PI)/p.arms_K[i_arm]);
		double upper_r = p.arms_r0[i_arm] * exp((theta - p.arms_theta0[i_arm] + n2*2.*M_PI)/p.arms_K[i_arm]);
		double arm_distance = std::min(  fabs(radius-lower_r) , fabs(upper_r-radius) );
		value += norm_gaussian * exp( -pow(arm_distance,2.)/(2.*p.armWidth*p.armWidth) );
	}

	return (value);
}

} /* namespace */
