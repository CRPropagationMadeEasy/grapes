#include "Galaxy/sourceProfile.h"

namespace DRAGON{

double TSourceProfileConstant::distribution(const TVector3d& pos) {
	return (constantValue);
}

TSourceProfileGaussianModel::TSourceProfileGaussianModel(GaussianParams params) : TGalaxyGrid(), pGaussian(params) {
	GaussianCentre.setXYZ(pGaussian.xC, pGaussian.yC, pGaussian.zC);
}

double TSourceProfileGaussianModel::distribution(const TVector3d& pos) {
	TVector3d distance = pos - GaussianCentre;
	double X = distance.getR2();
	return (exp(-X / 2.0 / pGaussian.sigma2));
}


double TSourceProfileYusifov04::distribution(const TVector3d& pos){
	double r = pos.getR();
	double expR = exp(-pYusifov04.b * (r - cXSun) / (pYusifov04.R1 + cXSun));
	double radialProfile = pow( (r + pYusifov04.R1)/(cXSun + pYusifov04.R1), pYusifov04.a) * expR;
	double zProfile = exp(-fabs(pos.z) / pYusifov04.z0);
	return (radialProfile * zProfile);
}


double TSourceProfileFerriere01::distribution(const TVector3d& pos){
	double r =  pos.getR();
	double profileSnI = 7.3 * exp(-(r - cXSun) / 4.5 - fabs(pos.z) / 0.325);
	double profileSnII = (r > 3.7) ? 50.0 * exp(-(r * r - cXSun * cXSun) / 46.24) : 177.5 * exp(-pow((r - 3.7) / 2.1, 2));
	profileSnII *= (0.79 * exp(-pow(pos.z / 0.212, 2)) + 0.21 * exp(-pow(pos.z / 0.636, 2)));
	return (profileSnI + profileSnII);
}


double TSourceProfileMyModel::distribution(const TVector3d& pos){
	throw TDragonExceptions("You are trying to use MyModel without implementing it.");
	return (0);
}

} // namespace
