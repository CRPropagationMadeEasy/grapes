#include "ParticleChain/particleChain.h"

namespace DRAGON {

TParticleChain::TParticleChain(ref_ptr<TInput> in) {

	readParticleListFromFile(/*passargli il pezzo di input*/);

	sortParticleList();

	updateAttribute(/*passargli il pezzo di input*/);
}

bool TParticleChain::doPropagate(int particleId) {
	return (true);
}

void TParticleChain::addParticle(const std::string& line) {

	std::istringstream iss(line);

	double A, Z, tHalf, ab_1stPop, rigCutoff_1stPop, ab_2ndPop, rigCutoff_2ndPop;
	std::vector<double> slopes_1stPop(2), slopes_2ndPop(2), rigBreaks_1stPop(1), rigBreaks_2ndPop(1);
	std::string decayMode, name;

	iss >> A >> Z >> tHalf >> decayMode >> ab_1stPop >> slopes_1stPop[0] >> rigBreaks_1stPop[0] >> slopes_1stPop[1] >> rigCutoff_1stPop >> ab_2ndPop >> slopes_2ndPop[0] >> rigBreaks_2ndPop[0] >> slopes_2ndPop[1] >> rigCutoff_2ndPop >> name;

	int particleId = 1000*Z+A;

	if (doPropagate(particleId)) {

		TSpectrumAttributes firstPop(ab_1stPop, rigCutoff_1stPop, slopes_1stPop, rigBreaks_1stPop);
		TSpectrumAttributes secondPop(ab_2ndPop, rigCutoff_2ndPop, slopes_2ndPop, rigBreaks_2ndPop);
		TDmSpectrumAttributes DarkMatterSpectrum(0, 0, 0, "");

		TParticleAttributes p(Z, A, tHalf / Myr, strToDecayMode(decayMode), name, firstPop, secondPop, DarkMatterSpectrum);

		particleChain.insert( std::pair<int, TParticleAttributes>(particleId,p) );
		//particleChain[particleId] = p; //This does not work on some compilers...

		Logger::cout(DEBUG)<<"Adding to particle list: id = "<<particleId<<" with lifetime in Myr = "<<tHalf / Myr<<" and decay mode = "<<decayMode<<"\n";
	}

	//if (Z > Zmax || Z < Zmin) continue;
	//	if (tau > cMinNucleiLifetime || decayMode == "STABLE")
	//{
	//addParticle(id,tau,tauNaked,Utilities::strToDecMode(decayMode));
	//}
	//else nucleiShortLived.push_back(id); // Short-lived nuclei are not propagated but are treated properly in the spallation network
	//}

	return ;
}

void TParticleChain::readParticleListFromFile() {

	std::string line;
	std::ifstream infile(isotopeListFilename.c_str(), std::ios::in);

	const int max_num_of_char_in_a_line = 512,
			num_of_header_lines = 2;

	for(int i = 0; i < num_of_header_lines; ++i)
		infile.ignore(max_num_of_char_in_a_line, '\n');

	while (std::getline(infile, line))
	{
		addParticle(line);
	}

	infile.close();

	return ;
}

void TParticleChain::updateAttribute(/* passare il pezzo di input */) {

	bool modifyAllNuclei = false; // to be read from Input
	TParticleAttributes pAllNucleiFromInput ; //to be read from Input

	if (modifyAllNuclei == true) {

		for (std::map<int, TParticleAttributes>::iterator it=particleChain.begin(); it!=particleChain.end(); ++it) {

			TParticleAttributes CurrentParticle = it->second;
			TParticleAttributes NewAttributes;
			if (CurrentParticle.A > 0) { // it is a nucleus!
				NewAttributes = pAllNucleiFromInput;
				it->second = NewAttributes;
			}
		}
	}

}

void TParticleChain::sortParticleList() {

	//std::sort(particleIds.begin(), particleIds.end(), Utilities::idComparator); // Sort nuclei according to ordering defined by function comparator.
	//std::sort(nucleiShortLived.begin(), nucleiShortLived.end(), Utilities::idComparator);

	return ;
}

} /* namespace */
