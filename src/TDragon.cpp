#include "../include/TDragon.h"

namespace DRAGON {

TDragon::TDragon() {
	if (!dirExist("output"))
		throw TDragonExceptions("Output directory not found.\n");
	if (!dirExist("data"))
		throw TDragonExceptions("Data directory not found.\n");
	if (!fileExist("data/particleList.txt"))
		throw TDragonExceptions("particleList.txt not found\n");
}

void TDragon::loadInputFile(const std::string &filename) {
	xml_filename = filename;

	root_filename = base_name<std::string>(filename);
	root_filename = remove_extension<std::string>(root_filename);

	xml_document doc;
	xml_parse_result result = doc.load_file(xml_filename.c_str());
	if (!result) {
		throw TDragonExceptions("Error reading XML card: " + str(result.description())+"\n");
	}
	xml_node root = doc.child("DRAGON");
	if (!root) {
		throw TDragonExceptions("Error reading XML card: Root element DRAGON not found");
	}
	in = new DRAGON::TInput(root);
}

void TDragon::buildPositionGrid(const TAxisParams& pAxis) {
	Logger::cout(DEBUG) << "Building position... " << "\n";
	position = new TPositionGrid(pAxis);
	Logger::cout(DEBUG) << "... first position in Galaxy : " << position->getFirstPosition() / kpc << " kpc\n";
	Logger::cout(DEBUG) << "... last position in Galaxy : " << position->getLastPosition() /kpc << " kpc\n";
	Logger::cout(DEBUG) << "... done!" << "\n";
}

void TDragon::buildEnergyAxes(const TAxisParams& pAxis) {
	Logger::cout(DEBUG) << "Building energy axis... " << "\n";
	energyAxes = new TEnergyAxes(pAxis);
	Logger::cout(DEBUG) << "... kEnergy : " << energyAxes->getMin() / GeV << " ... " << energyAxes->getMax() / GeV << " GeV\n";
	Logger::cout(DEBUG) << "... done!" << "\n";
}

void TDragon::buildGalaxy(const TGalaxyParams& pGalaxy) {
	Logger::cout(INFO)<<"Init Galaxy construction...\n";
	galaxy = new TGalaxy(pGalaxy, position);
	if (pGalaxy.doDumpToFile) {
		galaxy->dumpToFile(root_filename);
		Logger::cout(DEBUG)<<"... Galaxy wrote to file"<<"\n";
	}
	Logger::cout(DEBUG) << "... done!" << "\n";
}

void TDragon::run() {
	buildPositionGrid(in->AxisNode);
	buildEnergyAxes(in->AxisNode);
	buildGalaxy(in->GalaxyNode);
	//DRAGON::ref_ptr<DRAGON::TParticleChain> pChain = new DRAGON::TParticleChain(in);
	//DRAGON::TParticleList * pList = new DRAGON::TParticleList(in);
	//DRAGON::TGalaxy * galaxy = new DRAGON::TGalaxy(in,pList);
}

} /* namespace */
