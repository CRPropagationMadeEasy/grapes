#include "TEnergyAxes.h"

namespace DRAGON {

TEnergyAxes::TEnergyAxes(const TAxisParams& pAxis)
{
	kineticEnergy.buildGeometricalAxis(pAxis.EkMin,pAxis.EkMax,pAxis.EkSize);
}

double TEnergyAxes::getMin() {
	return (kineticEnergy.getMin());
}

double TEnergyAxes::getMax() {
	return (kineticEnergy.getMax());
}

std::vector<double>& TEnergyAxes::getKEnergyAxis() {
	return (kineticEnergy.getAxis());
}

} /* namespace */
