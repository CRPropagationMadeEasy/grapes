#include "TPositionGrid.h"

namespace DRAGON {

TPositionGrid::TPositionGrid(const TAxisParams& pAxis_) : pAxis(pAxis_) {
	TAxis<double> xGalaxy, yGalaxy, zGalaxy;

	if (pAxis.doEquidistantInX) {
		xGalaxy.buildLinearAxis(pAxis.xMin, pAxis.xMax, pAxis.xSize);
	}
	else {
		xGalaxy.buildAxis(pAxis.xValuesFilename,false);
	}

	if (pAxis.doEquidistantInY) {
		yGalaxy.buildLinearAxis(pAxis.yMin, pAxis.yMax, pAxis.ySize);
	}
	else {
		yGalaxy.buildAxis(pAxis.yValuesFilename,false);
	}

	if (pAxis.doEquidistantInZ) {
		zGalaxy.buildLinearAxis(pAxis.zMin, pAxis.zMax, pAxis.zSize);
	}
	else {
		zGalaxy.buildAxis(pAxis.zValuesFilename,true);
	}

	Nx = xGalaxy.getSize();
	Ny = yGalaxy.getSize();
	Nz = zGalaxy.getSize();

	grid = new VectorGrid(Nx, Ny, Nz);
	for (size_t ix = 0; ix < Nx; ++ix)
		for (size_t iy = 0; iy < Ny; ++iy)
			for (size_t iz = 0; iz < Nz; ++iz) {
				TVector3d &p = grid->get(ix, iy, iz);
				p.x = xGalaxy.getValue(ix);
				p.y = yGalaxy.getValue(iy);
				p.z = zGalaxy.getValue(iz);
			}
}

void TPositionGrid::setGrid(ref_ptr<VectorGrid> grid) {
	this->grid = grid;
}

size_t TPositionGrid::getNx() const {
	return (Nx);
}

size_t TPositionGrid::getNy() const {
	return (Ny);
}

size_t TPositionGrid::getNz() const {
	return (Nz);
}

size_t TPositionGrid::getNElements() const {
	return (Nx * Ny * Nz);
}

double TPositionGrid::getXMin() const {
	return (pAxis.xMin);
}

double TPositionGrid::getYMin() const {
	return (pAxis.yMin);
}

double TPositionGrid::getZMin() const {
	return (pAxis.zMin);
}

double TPositionGrid::getXMax() const {
	return (pAxis.xMax);
}

double TPositionGrid::getYMax() const {
	return (pAxis.yMax);
}

double TPositionGrid::getZMax() const {
	return (pAxis.zMax);
}

gridTypes TPositionGrid::getGridType() const {
	return (pAxis.Type);
}

ref_ptr<VectorGrid> TPositionGrid::getGrid() {
	return (grid);
}

TVector3d TPositionGrid::getPosition(const size_t& ix, const size_t& iy, const size_t& iz) const {
	return (grid->get(ix, iy, iz));
}

TVector3d TPositionGrid::getPosition(const size_t& i) const {
	return (grid->get(i));
}

TVector3d TPositionGrid::getFirstPosition() const {
	return (grid->get(0));
}

TVector3d TPositionGrid::getLastPosition() const {
	return (grid->get(Nx * Ny * Nz - 1));
}

} /* namespace */
