#include "cfitsioWrapper.h"

namespace DRAGON {

int TReadCfitsioFile::readIntKeyword(const std::string& keywordName) {

	int status = 0;
	int value = -1;
	char comment[100];

	if (fits_read_key(fptr, TINT, keywordName.c_str(), &value, comment, &status))
		fits_report_error(stderr, status);

	return (value);
}

double TReadCfitsioFile::readDoubleKeyword(const std::string& keywordName) {

	int status = 0;
	float value = -1;
	char comment[100];

	if (fits_read_key(fptr, TFLOAT, keywordName.c_str(), &value, comment, &status))
		fits_report_error(stderr, status);

	return ((double)value);
}

std::vector<double> TReadCfitsioFile::readDoubleImage(long nelements) {

	int status = 0;
	long felement = 1;
	float nulval = 0;
	int anynul;

	float * image = new float[nelements]();

	if (fits_read_img(fptr, TFLOAT, felement, nelements, &nulval, image, &anynul, &status))
		fits_report_error(stderr, status);

	std::vector<double> out;
	for (int i = 0; i < nelements; ++i)
		out.push_back( (double)image[i] );

	delete image;

	return (out);
}

}


