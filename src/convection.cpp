
TConvectionVelocity::TConvectionVelocity(TGrid* coord, TGeometry* geom, Input* in, TSource* SourceTerm) {

  nrn_sn = SourceTerm->GetSource(in->xobs,in->yobs,in->zobs);

  if (in->feedback >1) cout << "Called ConvectionVelocity"  << endl;
  dvdz = in->dvdz;
  conv_index_radial = in->conv_index_radial;
  set_profile_conv = in->set_profile_conv;
  conv_threshold = in->conv_threshold;
  vector<double> zgrid = coord->GetZ();
  dimz = zgrid.size();
  double rem_vel[dimz];
  double velocity=0;

  char buff[1000];
  sprintf(buff,"ASCII_spectra/%s/convection.dat",in->run_id.c_str());
  ofstream datafile;
  if(in->write_flag) datafile.open(buff);

  if (coord->GetType() == "2D") {
    vector<double> rgrid = coord->GetR();
    dimr = rgrid.size();

    for (int ir=0; ir<dimr; ir++) {
      double radius = rgrid[ir];

      for (int iz = 0; iz<dimz; iz++) {
	double zeta = zgrid[iz];

	if(fabs(zeta)<=in->z_k) velocity = ((((in->v0)-(in->vb))*pow(zeta,2.0)/pow(in->z_k,2.0))+(in->vb))* GetProfile(radius,0,zeta,SourceTerm);
	if(fabs(zeta)>in->z_k) velocity = ((in->v0) + dvdz*(fabs(zeta)-in->z_k)) * GetProfile(radius,0,zeta,SourceTerm);

	if((in->vb)>(in->v0)) cerr << "WARNING: vb > v0!" << endl;

	//smoothen drop to avoid non-numerical values in fluxes
	if(set_profile_conv == Radial && conv_index_radial>0.){
	  if(ir==dimr-1) velocity=0.; //last bin  = zero
	  if(ir==dimr-3) rem_vel[iz]=velocity;
	  if(ir==dimr-2)velocity=0.5*rem_vel[iz]; //second to last bin is set to 0.5 * third to last bin
	}

	if(in->write_flag) datafile << radius << " " << zeta << " " << velocity/km/Myr*kpc << endl;

	vc.push_back(velocity);
      }
    }

    //MW130705: CN coefficients now here for 2D, too
    for (unsigned int i = 0; i < dimr; ++i) {
      for (unsigned int k = 0; k < dimz; ++k) {

	double vCk = 0.0; // vC(i)
	double vCk1 = 0.0; // vC(i+1)
	double vC1k = 0.0; // vC(i-1)

	if ( coord->GetZ().at(k) > 0 ) {
	  vCk1 = 0.0;
	  vCk  = vc[conv_index(i,k)]/coord->GetDeltaZ_down(k);
	  vC1k = vc[conv_index(i,k-1)]/coord->GetDeltaZ_down(k);
	}
	else if ( coord->GetZ().at(k) < 0) {
	  vCk  = vc[conv_index(i,k)]/coord->GetDeltaZ_up(k);
	  vC1k = 0.0;
	  vCk1 = vc[conv_index(i,k+1)]/coord->GetDeltaZ_up(k);
	}
	else {
	  vCk  = vc[conv_index(i,k)]*2/(coord->GetDeltaZ_up(k) + coord->GetDeltaZ_down(k));
	  vC1k = -0.5*vc[conv_index(i,k-1)]/coord->GetDeltaZ_down(k);
	  vCk1 = -0.5*vc[conv_index(i,k+1)]/coord->GetDeltaZ_up(k);
	}

	CNconv_alpha1_z.push_back( vC1k );
	CNconv_alpha2_z.push_back( vCk );
	CNconv_alpha3_z.push_back( vCk1 );

#ifdef DEBUGMODE
	cout<<"[MW-DEBUG-CONV]"<<i<<" "<<k<<" "<<conv_index(i,k)<<" | ";
	cout<<vc[conv_index(i,k)]<<" | "<<vC1k<<" "<<vCk<<" "<<vCk1<<" | ";
	cout<<coord->GetDeltaZ_up(k)<<" "<<coord->GetDeltaZ_down(k)<<" "<<coord->GetDeltaZ(k)<<endl;
#endif
      }
    }
  } // end 2D
  else {
    vector<double> xgrid = coord->GetX();
    dimx = xgrid.size();
    vector<double> ygrid = coord->GetY();
    dimy = ygrid.size();

    for (int ix=0; ix<dimx; ix++) {
      double x = xgrid[ix];
      for (int iy=0; iy<dimy; iy++) {
	double y = ygrid[iy];
	for (int iz = 0; iz<dimz; iz++) {
	  double z = zgrid[iz];

	  if(fabs(z)<=in->z_k) velocity = ((((in->v0)-(in->vb))*pow(z,2.0)/pow(in->z_k,2.0))+(in->vb))* GetProfile(x,y,z,SourceTerm);
	  if(fabs(z)>in->z_k) velocity = ((in->v0) + dvdz*(fabs(z)-in->z_k)) * GetProfile(x,y,z,SourceTerm);

	  if((in->vb)>(in->v0)) cerr << "WARNING: vb > v0!" << endl;

	  velocity*= max( min( pow(geom->GetPattern(ix,iy,iz), in->SA_convec), in->SA_cut_convec), 1./in->SA_cut_convec );
	  velocity *= pow( in->LB_convec, coord->IsInLocalBubble(xgrid[ix],ygrid[iy],zgrid[iz]) );

	  vc.push_back(velocity);
	}
      }
    }

    if(set_profile_conv == Radial && conv_index_radial>0.){
      for (int ix=0; ix<dimx; ix++) for (int iy=0; iy<dimy; iy++) for (int iz = 0; iz<dimz; iz++){
	    //smoothen drop to avoid non-numerical values in fluxes, SK 06/13
	    if(ix==dimx-2 && iy==dimy-2)  vc[conv_index(ix,iy,iz)]=0.5*vc[conv_index(ix-1,iy,iz)];//second to last bin is set to 0.5 * third to last bin
	    if(iy==dimy-2 && ix==dimx-2)  vc[conv_index(ix,iy,iz)]=0.5*vc[conv_index(ix,iy-1,iz)];//second to last bin is set to 0.5 * third to last bin
	    if(ix==1 && iy==1)  vc[conv_index(ix,iy,iz)]=0.5*vc[conv_index(ix+1,iy,iz)];//second to last bin is set to 0.5 * third to last bin
	    if(iy==1 && ix==1)  vc[conv_index(ix,iy,iz)]=0.5*vc[conv_index(ix,iy+1,iz)];//second to last bin is set to 0.5 * third to last bin
	    if(ix==dimx-1 && iy==dimy-1) vc[conv_index(ix,iy,iz)]=0.; //border bins==0
	    if(ix==0 && iy==0) vc[conv_index(ix,iy,iz)]=0.; //border bins==0
	    if(ix==dimx-1 && iy==0) vc[conv_index(ix,iy,iz)]=0.; //border bins==0
	    if(ix==0 && iy==dimy-1) vc[conv_index(ix,iy,iz)]=0.; //border bins==0
	  }
    }

    //MW130624: CN coefficients are now here, the Evolutor just calls these vectors
    for (unsigned int i = 0; i < dimx; ++i) {
      for (unsigned int j = 0; j < dimy; ++j) {
	for (unsigned int k = 0; k < dimz; ++k) {

	  double vCk = 0.0; // vC(i)
	  double vCk1 = 0.0; // vC(i+1)
	  double vC1k = 0.0; // vC(i-1)

	  if ( coord->GetZ().at(k) > 0 ) {
	    vCk1 = 0.0;
	    vCk  = vc[conv_index(i,j,k)]/coord->GetDeltaZ_down(k);
	    vC1k = vc[conv_index(i,j,k-1)]/coord->GetDeltaZ_down(k);
	  }
	  else if ( coord->GetZ().at(k) < 0) {
	    vCk  = vc[conv_index(i,j,k)]/coord->GetDeltaZ_up(k);
	    vC1k = 0.0;
	    vCk1 = vc[conv_index(i,j,k+1)]/coord->GetDeltaZ_up(k);
	  }
	  else {
	    vCk  = vc[conv_index(i,j,k)]*2/(coord->GetDeltaZ_up(k) + coord->GetDeltaZ_down(k));
	    vC1k = -0.5*vc[conv_index(i,j,k-1)]/coord->GetDeltaZ_down(k);
	    vCk1 = -0.5*vc[conv_index(i,j,k+1)]/coord->GetDeltaZ_up(k);
	  }

	  CNconv_alpha1_z.push_back( vC1k );
	  CNconv_alpha2_z.push_back( vCk );
	  CNconv_alpha3_z.push_back( vCk1 );

	}
      }
    }
  }

  if(in->write_flag) datafile.close();

}

double TConvectionVelocity::GetProfile(double x, double y, double zeta, TSource* SourceTerm) {

  double radial = 1;
  double result = 1.;

  switch(set_profile_conv) {
  case Constant :
    return 1.0;
    break;

  case Radial : //MW130621: Qtau is doing the same as Radial from Convection_new, just not evaluating it again, but instead taking the value of the SourceTerm.

    //MW130711: z-dependency should not be accounted for!
    zeta = 0;

    //MW130711: for comparison with DRAGON-KIT, don't normalize.
    radial = SourceTerm->GetSource(x,y,zeta);
    if (radial < conv_threshold) radial=conv_threshold;
    return pow(radial, conv_index_radial);

  case Qtau :

    radial = SourceTerm->GetSource(x,y,zeta);
    radial /= nrn_sn;

    if (radial < conv_threshold) radial=conv_threshold;

    return pow(radial, conv_index_radial);
    break;

  default :
    return -1;
  }

}
