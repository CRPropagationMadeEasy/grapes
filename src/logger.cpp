#include "logger.h"

#include <ctime>

namespace DRAGON {
  
  const char* const Logger::LogFileName = "log.out";
  
  std::ofstream Logger::LogStream;
  
  LogLevel Logger::LogLevelCout = NONE;
  
  LogLevel Logger::LogLevelScreen = INFO;
  
  LogLevel Logger::LogLevelFile = DEBUG;
  
  Logger Logger::Instance;
  
  Logger& Logger::cout(LogLevel i)
  {
    LogLevelCout = i;
    
    time_t t = time(0);
    
    struct tm * now = localtime(&t);
    
    std::ostringstream timeStream;
    timeStream << (now->tm_year+1900) << "-" << (now->tm_mon) << "-" << (now->tm_mday) << " " << (now->tm_hour) << ":" << (now->tm_min) << ":" << (now->tm_sec);
    
    switch (i) {
      case INFO:
        if (LogLevelFile >= INFO  ) LogStream << "[INFO  " << timeStream.str() << "] ";
        break;
      case DEBUG:
        if (LogLevelFile >= DEBUG ) LogStream << "[DEBUG " << timeStream.str() << "] ";
        break;
      case ERROR:
        if (LogLevelFile >= ERROR ) LogStream << "[ERROR " << timeStream.str() << "] ";
        break;
      default:
        break;
    }
    return (Instance);
  }
  
  Logger::~Logger()
  {
    LogStream.close();
  }
  
  Logger::Logger()
  {
    LogStream.open(LogFileName);
    if (!LogStream.good()) {
      std::cerr << "Unable to initialize the Logger!" << std::endl;
    }
  }
  
  void Logger::log(const std::string& inMessage) //, const string& inLogLevel) {
  {
    if (LogLevelCout <= LogLevelFile) LogStream << inMessage << std::endl;
    if (LogLevelCout <= LogLevelScreen) std::cout << inMessage << std::endl;
  }
  
  void Logger::log(const std::vector<std::string>& inMessages)
  {
    for (size_t i = 0; i < inMessages.size(); i++) {
      log(inMessages[i]);
    }
  }
  
} // namespace
