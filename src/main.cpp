#include <iostream>

#include "TDragon.h"
#include "exceptions.h"
#include "logger.h"

int main(int argc, char **argv) {

	if (argc < 2) {
		std::cout << "Please provide an input file: ./dragon <XML File>" << std::endl;
		return (1);
	}

	DRAGON::Logger::cout(DRAGON::INFO).log("Version : DRAGON 2.0");
	try {
		DRAGON::TDragon exe;
		exe.loadInputFile(argv[1]);
		//exe.loadInputFile("examples/dragonModel2d.xml");
		exe.run();
	}
	catch (DRAGON::TDragonExceptions &e) {
		DRAGON::Logger::cout(DRAGON::ERROR)<<e.what()<<"\n";
		return (1);
	}

	return (0);
}
