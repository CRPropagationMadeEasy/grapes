
TReaccelerationCoefficient::TReaccelerationCoefficient(vector<double> pp, TDiffusionCoefficient* dperp, TGeometry* geom, Input* in) {

  double a;
  double Dpp_constant[pp.size()];

  for (unsigned int i = 0; i < pp.size(); ++i){
    if(pp[i] < in->rho_b)
      a = (in->DiffT == Anisotropic) ? in->DeltaPar : dperp->GetDelta(); //MW130711: integrate Anisotropic Diffusion
    else
      a = (in->DiffT == Anisotropic) ? in->DeltaPar : dperp->GetDelta_h();

    Dpp_constant[i]= 1.0/(a*(4.-a)*(4.-a*a));       // Ptuskin-2003
    if(in->diff_reacc == 1) Dpp_constant[i] *= 4.0/3.0; // Seo & Ptuskin
  }

  vector<double> DiffSpectrum = dperp->GetSpectrum();
  for (unsigned int i = 0; i < pp.size(); ++i) sp.push_back(Dpp_constant[i]*in->vAlfven*in->vAlfven*pp[i]*pp[i]/DiffSpectrum[i]);

#ifdef DEBUGMODE
  for (unsigned int i = 0; i < pp.size(); ++i){
    cout<<"[MW-DEBUG REACC G] "<<" "<<i<<" "<<Dpp_constant[i]<<" "<<in->vAlfven<<" "<<pp[i]<<" ";
    cout<<DiffSpectrum[i]<<" | " <<in->DiffT<<" "<<in->DeltaPar<<" "<<" "<<dperp->GetDelta()<<" "<<dperp->GetDelta_h()<<" "<<endl;
  }
#endif

  dimr = dperp->GetDimR();
  dimx = dperp->GetDimX();
  dimy = dperp->GetDimY();
  dimz = dperp->GetDimZ();

  vector<double> DiffProfile = (in->DiffT == Anisotropic) ? dperp->GetDPar() : dperp->GetDiffusionCoefficient();

  if(dperp->GetCoord()->GetType() == "3D")
    {
      int index = 0;
      for (vector<double>::iterator i = DiffProfile.begin(); i != DiffProfile.end(); ++i)
        {
	  int ix = dperp->GetCoord()->GetXFromIndexD_3D(index);
	  int iy = dperp->GetCoord()->GetYFromIndexD_3D(index);
	  int iz = dperp->GetCoord()->GetZFromIndexD_3D(index);

	  double xx = dperp->GetCoord()->GetX()[ix];
	  double yy = dperp->GetCoord()->GetY()[iy];
	  double zz = dperp->GetCoord()->GetZ()[iz];

	  double reacc_spatial = 1.0/(*i);

	  double spiral_factor_dperp = max( min( pow(geom->GetPattern(ix,iy,iz), in->SA_diff), in->SA_cut_diff), 1./in->SA_cut_diff );
	  double spiral_factor_dpp = max( min( spiral_factor_dperp * pow(geom->GetPattern(ix,iy,iz), 2*in->SA_vA), in->SA_cut_vA), 1./in->SA_cut_vA );

	  reacc_spatial *= spiral_factor_dpp; //mw 130422
	  if (dperp->GetCoord()->IsInLocalBubble(xx,yy,zz)) reacc_spatial *= pow(in->LB_vA, 2*dperp->GetCoord()->IsInLocalBubble(xx,yy,zz)) * pow(in->LB_diff, dperp->GetCoord()->IsInLocalBubble(xx,yy,zz));
	  dpp.push_back(reacc_spatial);

	  index++;
        }
    }
  else
    {
      for (vector<double>::iterator i = DiffProfile.begin(); i != DiffProfile.end(); ++i)
        {
	  double reacc_spatial = 1.0/(*i);
	  dpp.push_back(reacc_spatial);
        }
    }
}

