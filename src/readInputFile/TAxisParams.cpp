#include "readInputFile/TAxisParams.h"

namespace DRAGON {

void TAxisParams::setParams(xml_node &node) {
	if (!node.empty()) {
		readSpatialDimensions(node);
		readXParams(node);
		if (Type == g3D)
			readYParams(node);
		readZParams(node);
		readEnergyParams(node);
	}
	else {
		Logger::cout(DEBUG)<<"... using standard values for Spatial and Energy Axes"<<"\n";
	}
}

void TAxisParams::readSpatialDimensions(xml_node &node) {
	size_t nSpatialDimensions = childIntValue(node, "nSpatialDimensions");
	if (nSpatialDimensions == 2)
		Type = g2D;
	else if (nSpatialDimensions == 3)
		Type = g3D;
	else
		throw TDragonExceptions("Wrong grid type in XML.");
}

void TAxisParams::readXParams(xml_node &node) {
	if (childExist(node,"xValuesFilename")) {
		doEquidistantInX = false;
		xValuesFilename = childStringValue(node, "xValuesFilename");
	}
	else {
		xSize = childIntValue(node, "nPointsInR");
		xMax = childDoubleValue(node, "GalaxyRadius", kpc);
		xMin = (Type == g3D) ? -xMax : 0.0;
	}
}

void TAxisParams::readYParams(xml_node &node) {
	if (childExist(node,"yValuesFilename")) {
		doEquidistantInY = false;
		yValuesFilename = childStringValue(node, "yValuesFilename");
	}
	else {
		ySize = childIntValue(node, "nPointsInY");
		yMax = childDoubleValue(node, "GalaxyRadius", kpc);
		yMin = -yMax;
	}
}

void TAxisParams::readZParams(xml_node &node) {
	if (childExist(node,"zValuesFilename")) {
		doEquidistantInZ = false;
		zValuesFilename = childStringValue(node, "zValuesFilename");
	}
	else {
		zSize = childIntValue(node, "nPointsInZ");
		zMax = childDoubleValue(node, "GalaxyHaloHeight", kpc);
		zMin = -zMax;
	}
}

void TAxisParams::readEnergyParams(xml_node &node) {
	EkSize = childIntValue(node, "nPointsInEnergy");
	EkMin = childDoubleValue(node, "minKineticEnergy", GeV);
	EkMax = childDoubleValue(node, "maxKineticEnergy", GeV);
}


void TAxisParams::print() {
	Logger::cout(DEBUG)<<"Axis Params\n";
	if (doEquidistantInX) {
		Logger::cout(DEBUG)<<"... nPointsInX   "<<xSize<<"\n";
		Logger::cout(DEBUG)<<"... minimum X    "<<xMin / kpc<<" kpc\n";
		Logger::cout(DEBUG)<<"... maximum X    "<<xMax / kpc<<" kpc\n";
	}
	else {
		Logger::cout(DEBUG)<<"... x binning from "<<xValuesFilename<<"\n";
	}
	if (doEquidistantInY) {
		Logger::cout(DEBUG)<<"... nPointsInY   "<<ySize<<"\n";
		Logger::cout(DEBUG)<<"... minimum Y    "<<yMin / kpc<<" kpc\n";
		Logger::cout(DEBUG)<<"... maximum Y    "<<yMax / kpc<<" kpc\n";
	}
	else {
		Logger::cout(DEBUG)<<"... y binning from "<<yValuesFilename<<"\n";
	}
	if (doEquidistantInZ) {
		Logger::cout(DEBUG)<<"... nPointsInZ   "<<zSize<<"\n";
		Logger::cout(DEBUG)<<"... minimum Z    "<<zMin / kpc<<" kpc\n";
		Logger::cout(DEBUG)<<"... maximum Z    "<<zMax / kpc<<" kpc\n";
	}
	else {
		Logger::cout(DEBUG)<<"... z binning from "<<zValuesFilename<<"\n";
	}
	Logger::cout(DEBUG)<<"... nPointsInEnergy  "<<EkSize<<"\n";
	Logger::cout(DEBUG)<<"... minKineticEnergy "<<EkMin / GeV<<" GeV\n";
	Logger::cout(DEBUG)<<"... maxKineticEnergy "<<EkMax / GeV<<" GeV\n";
}

} /* namespace */



