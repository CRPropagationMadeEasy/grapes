#include "readInputFile/TDarkProfileParams.h"

namespace DRAGON {

void TDarkProfileParams::setParams(xml_node &node) {
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		Type = darkProfileConstant;
		ConstantValue = childDoubleAttribute(node, "ConstantValue", 1);
	}
	else if (stringType == "MyModel")
	{
		Type = darkProfileMyModel;
	}
	else if (stringType == "GNFW")
	{
		Type = darkProfileGNFW;
		alpha = childDoubleAttribute(node, "alpha", 1);
		beta = childDoubleAttribute(node, "beta", 1);
		gamma = childDoubleAttribute(node, "gamma", 1);
	}
	else if (stringType == "Cored")
	{
		Type = darkProfileCored;
		beta = childDoubleAttribute(node, "beta", 1);
		gamma = childDoubleAttribute(node, "gamma", 1);
	}
	else if (stringType == "Einasto65")
	{
		Type = darkProfileEinasto65;
		alpha = childDoubleAttribute(node, "alpha", 1);
	}
	else if (stringType == "Stadel09")
	{
		Type = darkProfileStadel09;
		alpha = childDoubleAttribute(node, "alpha", 1);
	}
	else
	{
		throw TDragonExceptions("Unknown SourceProfile type in XML.");
	}
}

void TDarkProfileParams::print() {
	Logger::cout(DEBUG)<<"... DarkProfile Type  "<<stringType<<"\n";
	if (Type == darkProfileConstant)
		Logger::cout(DEBUG)<<"... constant value "<<ConstantValue<<"\n";
	if (Type == darkProfileGNFW or Type == darkProfileEinasto65 or Type == darkProfileStadel09)
		Logger::cout(DEBUG)<<"... alpha "<<alpha<<"\n";
	if (Type == darkProfileGNFW or Type == darkProfileCored)
		Logger::cout(DEBUG)<<"... beta "<<beta<<"\n";
	if (Type == darkProfileGNFW)
		Logger::cout(DEBUG)<<"... gamma "<<gamma<<"\n";


	return ;
}

} /* namespace */
