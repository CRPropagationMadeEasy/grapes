#include "readInputFile/TGalaxyParams.h"

namespace DRAGON {

void TGalaxyParams::setParams(xml_node &node) {
	if (!node.empty()) {
		xml_node child;
		smoothGasRadius = childDoubleValue(node, "SmoothGasRadius", kpc, false);
		smoothGasRadius = (smoothGasRadius < pc) ? 0 : smoothGasRadius;

		child = childNode(node,"HIModel");
		pHI.setParams(child, smoothGasRadius);
		child = childNode(node,"HIIModel");
		pHII.setParams(child, smoothGasRadius);
		child = childNode(node,"H2Model");
		pH2.setParams(child, smoothGasRadius);
		child = childNode(node,"XcoModel");
		pXco.setParams(child);
		child = childNode(node,"IsrfModel");
		pIsrf.setParams(child);
		child = childNode(node,"SourceProfile");
		pSourceProfile.setParams(child);
		child = childNode(node,"DarkMatterProfile");
		pDarkProfile.setParams(child);
		child = childNode(node,"MagneticField");
		pMagneticField.setParams(child);
		child = childNode(node,"Pattern");
		pPattern.setParams(child);
		doDumpToFile = childExist(node,"DumpGalaxyToFile");
	}
	else {
		Logger::cout(DEBUG)<<"... using standard values for Galaxy"<<"\n";
	}
}

void TGalaxyParams::print() {
	Logger::cout(DEBUG)<<"Galaxy Params\n";
	pHI.print();
	pHII.print();
	pH2.print();
	pXco.print();
	pIsrf.print();
	pSourceProfile.print();
	pDarkProfile.print();
	pMagneticField.print();
	pPattern.print();
	Logger::cout(DEBUG)<<"... SmoothGasRadius "<<smoothGasRadius / kpc<<"\n";
	Logger::cout(DEBUG)<<"... DumpToFile "<<doDumpToFile<<"\n";
}

} /* namespace */
