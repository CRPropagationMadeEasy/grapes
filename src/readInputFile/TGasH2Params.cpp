#include "readInputFile/TGasH2Params.h"

namespace DRAGON {

void TGasH2Params::setParams(xml_node &node, const double& smoothGasRadius_) {
	smoothGasRadius = smoothGasRadius_;
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		type = H2Constant;
		constantValue = childDoubleAttribute(node, "ConstantValue", 1. / cm3);
	}
	else if (stringType == "MyModel")
	{
		type = H2MyModel;
	}
	else if (stringType == "Ferriere2007")
	{
		type = H2Ferriere07;
	}
	else if (stringType == "Bronfman1988")
	{
		type = H2Bronfman88;
	}
	else if (stringType == "Nakanishi2006")
	{
		type = H2Nakanishi06;
	}
	else if (stringType == "Pohl2008")
	{
		type = H2Pohl08;
	}
	else
	{
		throw TDragonExceptions("Unknown H2 gas type in XML file.");
	}
	return ;
}


void TGasH2Params::print() {
	Logger::cout(DEBUG)<<"... H2 Type   "<<stringType<<"\n";
	if (type == H2Constant)
		Logger::cout(DEBUG)<<"... constant value "<<constantValue * cm3<<" cm3\n";
	return ;
}

} /* namespace */
