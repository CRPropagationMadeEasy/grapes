#include "readInputFile/TGasHIIParams.h"

namespace DRAGON {

void TGasHIIParams::setParams(xml_node &node, const double& smoothGasRadius_) {
	smoothGasRadius = smoothGasRadius_;
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		type = HIIConstant;
		constantValue = childDoubleAttribute(node, "ConstantValue", 1. / cm3);
	}
	else if (stringType == "MyModel")
	{
		type = HIIMyModel;
	}
	else if (stringType == "HIICordes91")
	{
		type = HIICordes91;
	}
	else if (stringType == "HIINE2001")
	{
		type = HIINE2001;
	}
	else
	{
		throw TDragonExceptions("Unknown HII type in XML file.");
	}
}

void TGasHIIParams::print() {
	Logger::cout(DEBUG)<<"... HII Type  "<<stringType<<"\n";
	if (type == HIIConstant)
		Logger::cout(DEBUG)<<"... constant value "<<constantValue<<" cm-3\n";
}

} /* namespace */
