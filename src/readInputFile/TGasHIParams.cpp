#include "readInputFile/TGasHIParams.h"

namespace DRAGON {

void TGasHIParams::setParams(xml_node &node, const double& smoothGasRadius_) {
	smoothGasRadius = smoothGasRadius_;
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		type = HIConstant;
		constantValue = childDoubleAttribute(node, "ConstantValue", 1. / cm3);
	}
	else if (stringType == "MyModel")
	{
		type = HIMyModel;
	}
	else if (stringType == "Moskalenko2002")
	{
		type = HIMoskalenko02;
	}
	else if (stringType == "Nakanishi2003")
	{
		type = HINakanishi03;
	}
	else if (stringType == "Ferriere2007")
	{
		type = HIFerriere07;
	}
	else
	{
		throw TDragonExceptions("Unknown HI type in XML file.");
	}
	return ;
}

void TGasHIParams::print() {
	Logger::cout(DEBUG)<<"... HI Type   "<<stringType<<"\n";
	if (type == HIConstant)
		Logger::cout(DEBUG)<<"... constant value "<<constantValue<<" cm-3\n";
	return ;
}

} /* namespace */
