#include "readInputFile/TInput.h"

namespace DRAGON {

TInput::TInput(xml_node root)
{
	xml_node node;

	//inputFilename = inputFilename_;
	//setSourceParamFilename();
	//setOutputFilenameInit();
	//readInputFile();
	//printParams();

	node = childNode(root,"OutputMode",false);
	OutputNode.setParams(node);

	node = childNode(root,"ObserverPosition",false);
	ObserverNode.setParams(node);

	node = childNode(root,"Axes",false);
	AxisNode.setParams(node);

	node = childNode(root,"Galaxy",false);
	GalaxyNode.setParams(node);

	printParams();
}

void TInput::printParams()
{
	Logger::cout(DEBUG)<<"Input Filenames\n";
	//Logger::cout(DEBUG)<<"... "<<inputFilename<<" and "<</*sourceParamsFilename<<*/"\n";
	OutputNode.print();
	ObserverNode.print();
	AxisNode.print();
	GalaxyNode.print();
	//dxx.print();
	//dpp.print();
	//pSource.print();
	//pNorms.print();
	return;
}

/*void TSpatialDiffusionParams::print(){
	Logger::cout(DEBUG)<<"Spatial Diffusion Params\n";
	return;
}*/

/*void TMomentumDiffusionParams::print(){
	Logger::cout(DEBUG)<<"Momentum Diffusion Params\n";
	return;
}*/

/*void TNormalizations::print(){
	Logger::cout(DEBUG)<<"Normalizations Params\n";
	Logger::cout(DEBUG)<<"... ProtonNormalizationAtEnergyInGeV         "<<ProtonNormEnergy<<"\n";
	Logger::cout(DEBUG)<<"... ElectronNormalizationAtEnergyInGeV       "<<ElectronNormEnergy<<"\n";
	Logger::cout(DEBUG)<<"... ExtraComponentNormalizationAtEnergyInGeV "<<ExtraComponentNormEnergy<<"\n";
	Logger::cout(DEBUG)<<"... ProtonNormalizationValue                 "<<ProtonNormValue<<"\n";
	Logger::cout(DEBUG)<<"... ElectronNormalizationValue               "<<ElectronNormValue<<"\n";
	Logger::cout(DEBUG)<<"... ExtraComponentNormalizationValue         "<<ExtraComponentNormValue<<"\n";
	return;
}*/

/*void TInput::setOutputFilenameInit()
{
	std::string Filename = base_name(inputFilename);
	outputFilenameInit = std::string(Filename, 0, Filename.size()-4);
	return ;
}*/

/*void TInput::setSourceParamFilename()
{
	sourceParamsFilename = std::string(inputFilename, 0, inputFilename.size()-4);
	sourceParamsFilename += ".source";

	if ( !fileExist(sourceParamsFilename) )
	{
		if ( fileExist(sourceParamsDefaultFilename) )
			sourceParamsFilename = sourceParamsDefaultFilename;
		else
			throw std::runtime_error("Problems with source file: " + str(sourceParamsFilename));
	}
	return ;
}*/

} /* namespace */
