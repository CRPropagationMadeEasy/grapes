#include "readInputFile/TIsrfParams.h"

namespace DRAGON {

void TIsrfParams::setParams(xml_node &node) {
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		Type = IsrfConstant;
		ConstantValue = childDoubleAttribute(node, "ConstantValue", 1.0);
	}
	else if (stringType == "MyModel")
	{
		Type =  IsrfMyModel;
	}
	else if (stringType == "GalpropTable")
	{
		Type = IsrfGalpropTable;
		tableFilename = childStringAttribute(node, "TableFilename");
	}
	else if (stringType == "Delahaye2010")
	{
		Type = IsrfDelahaye10;
		setDefaultTemperatures();
		setIRTemperatureFromXML(node);
		setOpticalTemperatureFromXML(node);
		setUVITemperatureFromXML(node);
		setUVIITemperatureFromXML(node);
		setUVIIITemperatureFromXML(node);
	}
	else
	{
		throw TDragonExceptions("Unknown ISRF type in XML.");
	}
	return ;
}

void TIsrfParams::setDefaultTemperatures() {
	CMB.setTemperature(2.7);
	CMB.setNormalizationToCmb(1.0);
	IR.setTemperature(33.07);
	IR.setNormalizationToCmb(4.5e-5);
	Optical.setTemperature(313.32);
	Optical.setNormalizationToCmb(1.2e-9);
	UVI.setTemperature(3249.3);
	UVI.setNormalizationToCmb(7.03e-13);
	UVII.setTemperature(6150.4);
	UVII.setNormalizationToCmb(3.39e-14);
	UVIII.setTemperature(23209.0);
	UVIII.setNormalizationToCmb(8.67e-17);
	return ;
}

void TIsrfParams::setIRTemperatureFromXML(xml_node &node) {
	if (childExist(node,"IR_Temperature"))
		IR.setTemperature(childDoubleValue(node, "IR_Temperature", 1));
	if (childExist(node,"IR_Normalization"))
		IR.setNormalizationToCmb(childDoubleValue(node, "IR_Normalization", 1));
	return ;
}

void TIsrfParams::setOpticalTemperatureFromXML(xml_node &node) {
	if (childExist(node,"Optical_Temperature"))
		Optical.setTemperature(childDoubleValue(node, "Optical_Temperature", 1));
	if (childExist(node,"Optical_Normalization"))
		Optical.setNormalizationToCmb(childDoubleValue(node, "Optical_Normalization", 1));
	return ;
}

void TIsrfParams::setUVITemperatureFromXML(xml_node &node) {
	if (childExist(node,"UVI_Temperature"))
		UVI.setTemperature(childDoubleValue(node, "UVI_Temperature", 1));
	if (childExist(node,"UVI_Normalization"))
		UVI.setNormalizationToCmb(childDoubleValue(node, "UVI_Normalization", 1));
	return ;
}

void TIsrfParams::setUVIITemperatureFromXML(xml_node &node) {
	if (childExist(node,"UVII_Temperature"))
		UVII.setTemperature(childDoubleValue(node, "UVII_Temperature", 1));
	if (childExist(node,"UVII_Normalization"))
		UVII.setNormalizationToCmb(childDoubleValue(node, "UVII_Normalization", 1));
	return ;
}

void TIsrfParams::setUVIIITemperatureFromXML(xml_node &node) {
	if (childExist(node,"UVIII_Temperature"))
		UVIII.setTemperature(childDoubleValue(node, "UVIII_Temperature", 1));
	if (childExist(node,"UVI_Normalization"))
		UVIII.setNormalizationToCmb(childDoubleValue(node, "UVIII_Normalization", 1));
	return ;
}

void TIsrfParams::print() {
	Logger::cout(DEBUG)<<"... ISRF Type "<<stringType<<"\n";
	if (Type == IsrfConstant)
		Logger::cout(DEBUG)<<"... ConstantValue "<<ConstantValue<<"\n";
	if (Type == IsrfGalpropTable)
		Logger::cout(DEBUG)<<"... tableFilename "<<tableFilename<<"\n";
	if (Type == IsrfDelahaye10) {
		Logger::cout(DEBUG)<<"... CMB     "<<CMB.getTemperature()<<"\t"<<CMB.getNormalizationToCmb()<<"\n";
		Logger::cout(DEBUG)<<"... IR      "<<IR.getTemperature()<<"\t"<<IR.getNormalizationToCmb()<<"\n";
		Logger::cout(DEBUG)<<"... Optical "<<Optical.getTemperature()<<"\t"<<Optical.getNormalizationToCmb()<<"\n";
		Logger::cout(DEBUG)<<"... UVI     "<<UVI.getTemperature()<<"\t"<<UVI.getNormalizationToCmb()<<"\n";
		Logger::cout(DEBUG)<<"... UVII    "<<UVII.getTemperature()<<"\t"<<UVII.getNormalizationToCmb()<<"\n";
		Logger::cout(DEBUG)<<"... UVIII   "<<UVIII.getTemperature()<<"\t"<<UVIII.getNormalizationToCmb()<<"\n";
	}

	return ;
}

} /*namespace */

