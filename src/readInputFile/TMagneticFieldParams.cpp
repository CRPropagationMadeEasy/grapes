#include "readInputFile/TMagneticFieldParams.h"

namespace DRAGON {

void TMagneticFieldParams::setParams(xml_node &node)
{
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		Type = MagFieldConstant;
		ConstantValue = childDoubleAttribute(node, "ConstantValue", muG);
	}
	else if (stringType == "MyModel")
	{
		Type = MagFieldMyModel;
	}
	else if (stringType == "Exponential")
	{
		Type = MagFieldExponential;
		if (childAttributeExist(node, "B0"))
			B0 = childDoubleAttribute(node, "B0", muG);
		if (childAttributeExist(node,"r0"))
			r0 = childDoubleAttribute(node, "r0", kpc);
		if (childAttributeExist(node,"rc"))
			rc = childDoubleAttribute(node, "rc", kpc);
		if (childAttributeExist(node,"zc"))
			zc = childDoubleAttribute(node, "zc", kpc);
	}
	else if (stringType == "Sun2007")
	{
		Type = MagFieldSun07;
	}
	else if (stringType == "Pshirkov2011ASS")
	{
		Type = MagFieldPshirkov11ASS;
	}
	else if (stringType == "Pshirkov2011BSS")
	{
		Type = MagFieldPshirkov11BSS;
	}
	else if (stringType == "Farrar2012")
	{
		Type = MagFieldFarrar12;
	}
	else
	{
		throw TDragonExceptions("Unknown MagneticField type in XML file.");
	}
	return ;
}


void TMagneticFieldParams::print()
{
	Logger::cout(DEBUG)<<"... MagneticField Type "<<stringType<<"\n";
	if (Type == MagFieldConstant) {
		Logger::cout(DEBUG)<<"... ConstantValue "<<ConstantValue<<"\n";
	}
	if (Type == MagFieldExponential) {
		Logger::cout(DEBUG)<<"... B0 "<<B0 / muG<<" muG\n";
		Logger::cout(DEBUG)<<"... r0 "<<r0 / kpc<<" kpc\n";
		Logger::cout(DEBUG)<<"... rc "<<rc / kpc<<" kpc\n";
		Logger::cout(DEBUG)<<"... zc "<<zc / kpc<<" kpc\n";
	}
	return ;
}

} /* namespace */
