#include "readInputFile/TObserverParams.h"

namespace DRAGON {

void TObserverParams::setParams(xml_node &node) {
	if (!node.empty()) {
		x = childDoubleValue(node, "x", kpc);
		y = childDoubleValue(node, "y", kpc);
		z = childDoubleValue(node, "z", kpc);
	}
	else {
		Logger::cout(DEBUG)<<"... using standard values for ObserverPosition"<<"\n";
	}
}

void TObserverParams::print() {
	Logger::cout(DEBUG)<<"Observer Params\n";
	Logger::cout(DEBUG)<<"... x "<<x / kpc<<" kpc\n";
	Logger::cout(DEBUG)<<"... y "<<y / kpc<<" kpc\n";
	Logger::cout(DEBUG)<<"... z "<<z / kpc<<" kpc\n";
}

} /* namespace */






