#include "readInputFile/TOutputModeParams.h"

namespace DRAGON {

void TOutputModeParams::setParams(xml_node &node) {
	if (!node.empty()) {
		doStoreSpectraAtObs = childExist(node,"doStoreSpectraAtObs");
		doStoreSpectraAllGalaxy = childExist(node,"doStoreSpectraAllGalaxy");
		doOutputInASCII = childExist(node,"doOutputInASCII");
		doOutputInFITS = childExist(node,"doOutputInFITS");
		//LogLevel logLevelScreen = childExist(node,"
		//LogLevel logLevelFile= childExist(node,"
	}
	else {
		Logger::cout(DEBUG)<<"... no output mode chosen"<<"\n";
	}
}

void TOutputModeParams::print() {
	Logger::cout(DEBUG)<<"OutputMode Params\n";
	Logger::cout(DEBUG)<<"... doStoreSpectraAtObs     "<<doStoreSpectraAtObs<<"\n";
	Logger::cout(DEBUG)<<"... doStoreSpectraAllGalaxy "<<doStoreSpectraAllGalaxy<<"\n";
	Logger::cout(DEBUG)<<"... doOutputInASCII         "<<doOutputInASCII<<"\n";
	Logger::cout(DEBUG)<<"... doOutputInFITS          "<<doOutputInFITS<<"\n";
	Logger::cout(DEBUG)<<"... logLevelScreen          "<<logLevelScreen<<"\n";
	Logger::cout(DEBUG)<<"... logLevelFile            "<<logLevelFile<<"\n";
}

} /* namespace */
