#include <sstream>
#include "readInputFile/TPatternParams.h"

namespace DRAGON {

void TPatternParams::setParams(xml_node &node) {
	stringType = childStringAttribute(node, "type");
	if (stringType == "SpiralArms" || stringType == "spiralarms" || stringType == "Spiralarms") {
		Type = PatternConstant;
		constantValue = 1.;
	} else if (stringType == "SpiralArms" || stringType == "spiralarms" || stringType == "Spiralarms") {
		Type = PatternSpiralArms;
		setDefaultArms();
		setArmsFromXML(node);
	} else
		throw TDragonExceptions("Unknown pattern type in XML.");
	return ;
}

void TPatternParams::setDefaultArms() {

	numArms = 4.;
	armWidth = 0.5; //kpc

	arms_K.push_back(4.25);
	arms_K.push_back(4.25);
	arms_K.push_back(4.89);
	arms_K.push_back(4.89);

	arms_r0.push_back(3.48);
	arms_r0.push_back(3.48);
	arms_r0.push_back(4.90);
	arms_r0.push_back(4.90);

	arms_theta0.push_back(0.00);
	arms_theta0.push_back(3.14);
	arms_theta0.push_back(2.52);
	arms_theta0.push_back(-0.62);
	return ;
}

void TPatternParams::setArmsFromXML(xml_node &node) {

	if (childAttributeExist(node,"numArms")) {

		numArms = childDoubleAttribute(node, "numArms", 1);

		if (childAttributeExist(node,"armWidth"))
			armWidth = childDoubleAttribute(node, "armWidth", kpc);

		for (unsigned int i_arm = 0; i_arm < numArms; i_arm++) {

			Logger::cout(DEBUG)<<"Reading arm number "<<i_arm+1<<"\n";
			std::ostringstream K_string, r0_string, theta0_string;
			K_string  << "K_"   << i_arm+1;
			r0_string << "r0_"  << i_arm+1;
			theta0_string << "theta0_" << i_arm+1;
			arms_K.push_back( childDoubleAttribute(node, K_string.str().c_str(), 1));
			arms_r0.push_back( childDoubleAttribute(node, r0_string.str().c_str(), 1));
			arms_theta0.push_back( childDoubleAttribute(node, theta0_string.str().c_str(), 1));
			Logger::cout(DEBUG)<<"Arm parameters (K, r0, theta0) = "<<arms_K.back()<<"\t"<<arms_r0.back()<<"\t"<<arms_theta0.back()<<"\n";
		}
	}

	return ;
}


void TPatternParams::print() {
	Logger::cout(DEBUG)<<"... Pattern Type "<<stringType<<"\n";
}

} /*namespace */
