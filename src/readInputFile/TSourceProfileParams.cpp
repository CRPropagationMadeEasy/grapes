#include "readInputFile/TSourceProfileParams.h"

namespace DRAGON {

void TSourceProfileParams::setParams(xml_node &node) {
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		Type = sourceProfileConstant;
		ConstantValue = childDoubleAttribute(node, "ConstantValue", 1);
	}
	else if (stringType == "MyModel")
	{
		Type = sourceProfileMyModel;
	}
	else if (stringType == "GaussianModel")
	{
		Type = sourceProfileGaussianModel;
		setGaussianProfileParamsFromXML(node);
	}
	else if (stringType == "Lorimer2006")
	{
		Type = sourceProfileLorimer06;
		setProfileParamsForLorimer06();
	}
	else if (stringType == "Yusifov2004")
	{
		Type = sourceProfileYusifov04;
		setProfileParamsForYusifov04();
		setProfileParamsFromXML(node);
	}
	else if (stringType == "Case1998")
	{
		Type = sourceProfileCase98;
		setProfileParamsForCase98();
	}
	else if (stringType == "Ferriere2001")
	{
		Type = sourceProfileFerriere01;
	}
	else
	{
		throw TDragonExceptions("Unknown SourceProfile type in XML.");
	}
	sourceRate = childDoubleAttribute(node, "SourceRate", 1);
	return ;
}

void TSourceProfileParams::setProfileParamsForYusifov04()
{
	pYusifov04.a = 1.64;
	pYusifov04.b = 4.01;
	pYusifov04.R1 = 0.55;
	pYusifov04.z0 = 0.1;
	return ;
}

void TSourceProfileParams::setProfileParamsForCase98()
{
	pYusifov04.a = 1.69;
	pYusifov04.b = 3.33;
	pYusifov04.R1 = 0.0;
	pYusifov04.z0 = 0.2;
	return ;
}

void TSourceProfileParams::setProfileParamsForLorimer06()
{
	pYusifov04.a = 1.9;
	pYusifov04.b = 5.0;
	pYusifov04.R1 = 0.0;
	pYusifov04.z0 = 0.2;
	return ;
}

void TSourceProfileParams::setProfileParamsFromXML(xml_node &node)
{
	if (childAttributeExist(node,"a"))
		pYusifov04.a = childDoubleAttribute(node, "a", 1);
	if (childAttributeExist(node,"b"))
		pYusifov04.b = childDoubleAttribute(node, "b", 1);
	if (childAttributeExist(node,"R1"))
		pYusifov04.R1 = childDoubleAttribute(node, "R1", kpc);
	if (childAttributeExist(node,"z0"))
		pYusifov04.z0 = childDoubleAttribute(node, "z0", kpc);
	return ;
}

void TSourceProfileParams::setGaussianProfileParamsFromXML(xml_node &node)
{
	pGaussian.xC = childDoubleAttribute(node, "xC", kpc);
	pGaussian.yC = childDoubleAttribute(node, "yC", kpc);
	pGaussian.zC = childDoubleAttribute(node, "zC", kpc);
	pGaussian.sigma2 = childDoubleAttribute(node, "sigma2", 1);
	return ;
}

void TSourceProfileParams::print() {
	Logger::cout(DEBUG)<<"... SourceProfile Type "<<stringType<<"\n";
	if (Type == sourceProfileConstant) {
		Logger::cout(DEBUG)<<"... ConstantValue "<<ConstantValue<<"\n";
	}
	if (Type == sourceProfileLorimer06 || Type == sourceProfileYusifov04 || Type == sourceProfileCase98) {
		Logger::cout(DEBUG)<<"... a "<<pYusifov04.a<<"\n";
		Logger::cout(DEBUG)<<"... b "<<pYusifov04.b<<"\n";
		Logger::cout(DEBUG)<<"... R1 "<<pYusifov04.R1<<"\n";
		Logger::cout(DEBUG)<<"... z0 "<<pYusifov04.z0<<"\n";
	}
	if (Type == sourceProfileGaussianModel) {
		Logger::cout(DEBUG)<<"... xC "<<pGaussian.xC<<"\n";
		Logger::cout(DEBUG)<<"... yC "<<pGaussian.yC<<"\n";
		Logger::cout(DEBUG)<<"... zC "<<pGaussian.zC<<"\n";
		Logger::cout(DEBUG)<<"... sigma2 "<<pGaussian.sigma2<<"\n";
	}
	Logger::cout(DEBUG)<<"... SourceRate "<<sourceRate<<"\n";
	return ;
}

} /* namespace */
