#include "readInputFile/TXcoParams.h"

namespace DRAGON {

void TXcoParams::setParams(xml_node &node) {
	stringType = childStringAttribute(node, "type");
	if (stringType == "Constant")
	{
		Type = XcoConstant;
		ConstantValue = childDoubleAttribute(node, "ConstantValue", 1);
	}
	else if (stringType == "MyModel")
	{
		Type = XcoMyModel;
	}
	else if (stringType == "Strong04")
	{
		Type = XcoStrong04;
	}
	else if (stringType == "Ackermann10")
	{
		Type = XcoAckermann10;
	}
	else if (stringType == "Evoli12")
	{
		Type = XcoEvoli12;
		InnerValue = childDoubleAttribute(node, "InnerValue", 1);
		OuterValue = childDoubleAttribute(node, "OuterValue", 1);
		Border = childDoubleAttribute(node, "Border", kpc);
	}
	else
	{
		throw TDragonExceptions("Unknown XCO type in XML file.");
	}
	return ;
}

void TXcoParams::print() {
	Logger::cout(DEBUG)<<"... XCO Type  "<<stringType<<"\n";
	if (Type == XcoConstant)
		Logger::cout(DEBUG)<<"... constant value "<<ConstantValue<<"\n";
	if (Type == XcoEvoli12) {
		Logger::cout(DEBUG)<<"... InnerValue value "<<InnerValue<<"\n";
		Logger::cout(DEBUG)<<"... OuterValue value "<<OuterValue<<"\n";
		Logger::cout(DEBUG)<<"... Border value "<<Border / kpc<<" kpc\n";
	}
	return ;
}

} /* namespace */
