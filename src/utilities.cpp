#include "utilities.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

namespace DRAGON {

bool fileExist(const std::string& filename) {
	std::fstream fin;
	fin.open(filename.data(), std::ifstream::in);
	if( fin.is_open() )
	{
		fin.close();
		return (true);
	}
	else return (false);
}

bool dirExist(const std::string& dirname) {
	struct stat buf;
	int result = stat(dirname.c_str(), &buf);
	if (result == 0 && S_ISDIR(buf.st_mode))
		return (true);
	else
		return (false);
}

std::string base_name(std::string const & path)
{
	return (path.substr(path.find_last_of("/\\") + 1));
}

void idToAZ(const int& uid, int& A, int& Z)
{
	if (uid == -999)
	{
		A = 1;
		Z = -1;
		return ;
	}
	if (uid == -998)
	{
		A = 2;
		Z = -1;
		return ;
	}

	A = int(uid%1000);
	Z = int(uid/1000);

	return ;
}

bool idComparator(const int& i, const int& j)
{
	int Ai = -1000;
	int Zi = -1000;
	idToAZ(i, Ai, Zi);

	int Aj = -1000;
	int Zj = -1000;
	idToAZ(j, Aj, Zj);

	if ((Ai == Aj) && ((Zi == 4 && Zj == 5) || (Zi == 6 && Zj == 7) || (Zi == 14 && Zj == 16))) return (true);
	if (Zi == Zj && (Ai > Aj)) return (true);
	if (Zi > Zj) return (true);
	return (false);
}

decayTypes strToDecayMode(const std::string& str)
{
	if (str == "EC") return (EC);
	if (str == "B-") return (BM);
	if (str == "BB") return (BB);
	if (str == "IT") return (IT);
	if (str == "ECB-") return (ECBM);
	if (str == "ECB+") return (ECBP);
	return (STABLE);
}

} /* namespace */
